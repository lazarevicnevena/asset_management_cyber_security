# Asset Management Cyber Security System


<p> Asset Management Cyber Security System (AMCSS) represents a system which is built to provide a simple yet efficient platform to organize, store, secure and manage all assets of an organization. An asset can be defined as anything of value to an organization that is related to information services. It can take form of a physical device, virtual machine, data or information and software system within the structure of an organization.
<br><br>
The system represents a classic web application which consists of two seperate applications: the client (frontend application) and the server (backend application). Additionally, a database management system for data persistence is also used.


## Domain Model

<p>
    The core component of the system is certainly an asset. To satisfy the fact that an asset can be anything an organization owns, it is important to distinguish different categories. Accordingly, an asset is either classified as hardware (work stations, laptops, network devices, etc), software (applications, operating systems, etc), virtual (virtual machines) or data (files, information, etc). Beside the name and description which describe an asset closer, the state and uuid are also mandatory properties. The state is used for keeping track of an asset’s lifecycle and uuid for uniquely identifying the asset. 
    <br/><br/>
    In order to effectively use, manage and secure all of the assets, it is mandatory to know their locations and functions at any time. Each hardware asset must have a location where it is physically located. If in some kind of relation with hardware assets, non-hardware assets can easily get location information over them. An example for that is when an application is installed on a laptop. The application which is classified as a software asset has relation with a laptop which is classified as a hardware asset, which eventually has a location assigned to it.
    Furthermore, each employee is assigned to one or more assets they are responsible for. In other words, each asset must have an owner who can be contacted when system detects any threat on their granted assets. 
    Assets can not only be tracked individually, but also as a group of assets. A group of assets is called an asset network and it can contain assets of same class, purpose, model and etc. 
    <br/><br/>
    An asset is considered sensitive if it has at least one security goal related to it. Security goal represents a goal which has to be fulfilled during the asset’s lifecycle in order to keep the asset secure and protected. Common security goals are confidentiality, integrity and availability (CIA triad). Each security goal can have multiple values related to it. For each security goal value, multiple security measures can be defined. Security measures represent security controls which need to be applied in order to protect the asset throughout its lifecycle.
    <br/><br/>
    Class diagram of described domain model of the asset management system is shown in the picture below:

</p>
<div align="center">

![Domain model](/uploads/5b2279884dbbe547a126293153b1e08e/class_diagram.png "Class diagram")

</div>

## Demo

Watch the next video in order to see how the application is intended to be used.

<div align="center">

[![Demo of AMCSS](http://img.youtube.com/vi/1MUO7AUtPcQ/0.jpg)](http://www.youtube.com/watch?v=1MUO7AUtPcQ "Click to see the demo video on YouTube")

</div>

## Installation

### 1. Simple start

#### Requirements:

1. Java 8 (also needs to be set as an environment variable)
1. Maven installed
1. MySQL installed

#### Download:

Open the [download link](https://gitlab.com/lazarevicnevena/asset_management_cyber_security/tree/master/distribution) and download the .zip file.

#### Remarks:

Database configuration is defined in the *src/main/resources/application.properties* file with three properties:
1. spring.datasource.url (URL database is running on and consists of: {IP address:port}/{database name})
1. spring.datasource.username (Username required to access the database)
1. spring.datasource.password (Password required to access the database)

The database is by default running on same machine (localhost) on port 3306. Name of the database is asset-management.
In order to access a remote database or access database with different credentials change above properties.

#### Run:

Double click on the *application.bat* file. When done, open browser on localhost:8080 and start using the system.

### 2. Manual installation

#### Requirements:

1. All requirements above listed 
1. Node.js 8+

#### Build and Run:

In order to build the system follow four simple steps:

1. Clone or download the project;
1. Open project with Java IDE (IntelliJ, Eclipse, NetBeans or any other);
1. Configure database connection (open the *src/main/resources/application.properties* file and adjust the url and credentials of your datasource)
1. Build backend application (Maven automatically downloads all required dependencies);
1. Build frontend application (position to root folder of frontend application: *src/main/resources/webapp/front* and run `npm install` to install required libraries).

In order to run the system you need to:

1. Run backend application (run application from your IDE)
1. Run frontend application (when positioned in the root folder either run command `npm start` or `yarn start`)

Once both applications are running, open browser on localhost:4200 and start using the system.

## Additional Information

For more detailed information about the system visit the repository's [wiki page](https://gitlab.com/lazarevicnevena/asset_management_cyber_security/wikis/Home).

## Issues

If you have any questions, concerns or bug reports, please file an issue in the repository's [Issue Tracker](https://gitlab.com/lazarevicnevena/asset_management_cyber_security/issues).