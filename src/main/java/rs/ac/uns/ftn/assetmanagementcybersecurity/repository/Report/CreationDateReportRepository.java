package rs.ac.uns.ftn.assetmanagementcybersecurity.repository.Report;

import org.springframework.data.jpa.repository.JpaRepository;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.Report.CreationDateReport;

import java.util.List;


public interface CreationDateReportRepository extends JpaRepository<CreationDateReport, Long> {
    List<CreationDateReport> findAllByOrderByIdDesc();
}
