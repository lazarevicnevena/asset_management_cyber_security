package rs.ac.uns.ftn.assetmanagementcybersecurity.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.SecurityGoal;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.SecurityMeasure;
import rs.ac.uns.ftn.assetmanagementcybersecurity.repository.SecurityGoalRepository;
import rs.ac.uns.ftn.assetmanagementcybersecurity.repository.SecurityMeasureRepository;

import java.util.List;
import java.util.Optional;

@Service
public class SecurityMeasureService {

    @Autowired
    private SecurityMeasureRepository securityMeasureRepository;

    public Optional<SecurityMeasure> getById(Long id) {
        return securityMeasureRepository.findById(id);
    }

    public List<SecurityMeasure> getAll(){
        return securityMeasureRepository.findAllByOrderByIdDesc();
    }

    public List<SecurityMeasure> getAllPageable(Pageable pageable){
        return securityMeasureRepository.findAll(pageable).getContent();
    }

    public Optional<SecurityMeasure> getByName(String name){
        return securityMeasureRepository.getByName(name);
    }

    public SecurityMeasure save(SecurityMeasure securityMeasure){
        return securityMeasureRepository.save(securityMeasure);
    }

    public List<SecurityMeasure> getAllPageableAndName(Pageable pageable, String name){
        return securityMeasureRepository.findAllByName(pageable, name).getContent();
    }

    public void delete(SecurityMeasure securityMeasure){
        securityMeasureRepository.delete(securityMeasure);
    }

}
