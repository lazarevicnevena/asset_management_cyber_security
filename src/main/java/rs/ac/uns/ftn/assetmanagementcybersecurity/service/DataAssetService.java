package rs.ac.uns.ftn.assetmanagementcybersecurity.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.DataAsset;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.VirtualAsset;
import rs.ac.uns.ftn.assetmanagementcybersecurity.repository.DataAssetRepository;
import rs.ac.uns.ftn.assetmanagementcybersecurity.repository.VirtualAssetRepository;

import java.util.List;
import java.util.Optional;

@Service
public class DataAssetService {

    @Autowired
    private DataAssetRepository dataAssetRepository;

    public Optional<DataAsset> getByUuid(String uuid) {
        return dataAssetRepository.getByUuid(uuid);
    }

    public List<DataAsset> getAll(){
        return dataAssetRepository.findAllByOrderByIdDesc();
    }

    public List<DataAsset> getAllPageable(Pageable pageable){
        return dataAssetRepository.findAll(pageable).getContent();
    }

    public DataAsset save(DataAsset asset){
        return dataAssetRepository.save(asset);
    }

    public void delete(DataAsset asset){
        dataAssetRepository.delete(asset);
    }
}
