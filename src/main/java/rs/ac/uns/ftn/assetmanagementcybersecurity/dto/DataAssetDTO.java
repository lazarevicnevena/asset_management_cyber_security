package rs.ac.uns.ftn.assetmanagementcybersecurity.dto;

import lombok.*;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class DataAssetDTO {

    private String name;
    private String description;
    private LocalDateTime created;
    private AssetType type;
    private String state;
    private String uuid;
    private OwnerDTO owner;
    private List<SecurityGoalValueDTO> securityGoalValues = new ArrayList<>();

    public DataAssetDTO(DataAsset dataAsset){
        this.name = dataAsset.getName();
        this.description = dataAsset.getDescription();
        this.created = dataAsset.getCreated();
        this.type = dataAsset.getType();
        this.state = dataAsset.getState();
        this.uuid = dataAsset.getUuid();
        this.owner = new OwnerDTO(dataAsset.getOwner());
        for (SecurityGoalValue value: dataAsset.getSecurityGoalValues()){
            this.securityGoalValues.add(new SecurityGoalValueDTO(value));
        }
    }
}
