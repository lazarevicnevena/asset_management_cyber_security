package rs.ac.uns.ftn.assetmanagementcybersecurity.dto;

import lombok.*;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.AssetType;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.HardwareAsset;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.SecurityGoalValue;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.VirtualAsset;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class VirtualAssetDTO {

    private String name;
    private String description;
    private String macAddress;
    private String ipAddress;
    private LocalDateTime created;
    private LocalDateTime lastUpdated;
    private AssetType type;
    private String state;
    private String uuid;
    private OwnerDTO owner;
    private String operatingSystem;
    private HardwareAssetDTO hardwareAsset;
    private List<SecurityGoalValueDTO> securityGoalValues = new ArrayList<>();

    public VirtualAssetDTO(VirtualAsset virtualAsset){
        this.name = virtualAsset.getName();
        this.description = virtualAsset.getDescription();
        this.macAddress = virtualAsset.getMacAddress();
        this.ipAddress = virtualAsset.getIpAddress();
        this.created = virtualAsset.getCreated();
        this.lastUpdated = virtualAsset.getLastUpdated();
        this.type = virtualAsset.getType();
        this.state = virtualAsset.getState();
        this.uuid = virtualAsset.getUuid();
        this.owner = new OwnerDTO(virtualAsset.getOwner());
        this.operatingSystem = virtualAsset.getOperatingSystem();
        this.hardwareAsset = new HardwareAssetDTO(virtualAsset.getHardwareAsset());
        for (SecurityGoalValue value: virtualAsset.getSecurityGoalValues()){
            this.securityGoalValues.add(new SecurityGoalValueDTO(value));
        }
    }

}
