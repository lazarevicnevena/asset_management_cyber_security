package rs.ac.uns.ftn.assetmanagementcybersecurity.dto;

import lombok.*;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.Owner;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class OwnerDTO {

    private String firstName;
    private String middleName;
    private String lastName;
    private String phoneNumber;
    private String employeeId;

    public OwnerDTO(Owner owner){
        this.firstName = owner.getFirstName();
        this.middleName = owner.getMiddleName();
        this.lastName = owner.getLastName();
        this.phoneNumber = owner.getPhoneNumber();
        this.employeeId = owner.getEmployeeId();
    }
}
