package rs.ac.uns.ftn.assetmanagementcybersecurity.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.VirtualAsset;

import java.util.List;
import java.util.Optional;

public interface VirtualAssetRepository extends JpaRepository<VirtualAsset, Long> {
    Optional<VirtualAsset> getByUuid(String uuid);
    List<VirtualAsset> findAllByOrderByIdDesc();
    Page<VirtualAsset> findAll(Pageable pageable);
}
