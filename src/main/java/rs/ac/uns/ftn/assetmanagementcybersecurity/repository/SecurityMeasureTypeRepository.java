package rs.ac.uns.ftn.assetmanagementcybersecurity.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.SecurityMeasureType;

import java.util.List;
import java.util.Optional;

public interface SecurityMeasureTypeRepository extends JpaRepository<SecurityMeasureType, Long> {
    Optional<SecurityMeasureType> getByName(String name);
    List<SecurityMeasureType> findAllByOrderByIdDesc();
    Page<SecurityMeasureType> findAll(Pageable pageable);
    Page<SecurityMeasureType> findAllByName(Pageable pageable, String name);
}
