package rs.ac.uns.ftn.assetmanagementcybersecurity.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.AssetNetwork;

import java.util.List;
import java.util.Optional;

public interface AssetNetworkRepository extends JpaRepository<AssetNetwork, Long> {
    Optional<AssetNetwork> getByName(String name);
    List<AssetNetwork> findAllByOrderByIdDesc();
    Page<AssetNetwork> findAll(Pageable pageable);
}
