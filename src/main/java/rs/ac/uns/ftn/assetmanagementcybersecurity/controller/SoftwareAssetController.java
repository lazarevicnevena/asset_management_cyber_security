package rs.ac.uns.ftn.assetmanagementcybersecurity.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rs.ac.uns.ftn.assetmanagementcybersecurity.dto.HardwareAssetDTO;
import rs.ac.uns.ftn.assetmanagementcybersecurity.dto.SecurityGoalValueDTO;
import rs.ac.uns.ftn.assetmanagementcybersecurity.dto.SecurityMeasureDTO;
import rs.ac.uns.ftn.assetmanagementcybersecurity.dto.SoftwareAssetDTO;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.*;
import rs.ac.uns.ftn.assetmanagementcybersecurity.service.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping(value = "/software-assets")
public class SoftwareAssetController {

    @Autowired
    private SoftwareAssetService softwareAssetService;

    @Autowired
    private HardwareAssetService hardwareAssetService;

    @Autowired
    private SecurityGoalValueService securityGoalValueService;

    @Autowired
    private SecurityGoalService securityGoalService;

    @Autowired
    private SecurityMeasureService securityMeasureService;

    @Autowired
    private LocationService locationService;

    @Autowired
    private OwnerService ownerService;

    @GetMapping(
            value = "/{uuid}",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<SoftwareAssetDTO> getOne(@PathVariable String uuid) {
        Optional<SoftwareAsset> optional = softwareAssetService.getByUuid(uuid);

        if (!optional.isPresent()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(new SoftwareAssetDTO(optional.get()), HttpStatus.OK);
    }

    @GetMapping(
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<List<SoftwareAssetDTO>> getAll() {

        List<SoftwareAsset> list = softwareAssetService.getAll();

        if (list.isEmpty()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        List<SoftwareAssetDTO> dtos = new ArrayList<>();
        for (SoftwareAsset softwareAsset : list){
            dtos.add(new SoftwareAssetDTO(softwareAsset));
        }

        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping(
            value = "/{uuid}/hardware-assets",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<List<HardwareAssetDTO>> getSoftwareAssets(@PathVariable String uuid) {
        Optional<SoftwareAsset> optional = softwareAssetService.getByUuid(uuid);

        if (!optional.isPresent()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        List<HardwareAsset> list = optional.get().getHardwareAssets();

        if (list.isEmpty()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        List<HardwareAssetDTO> dtos = new ArrayList<>();
        for (HardwareAsset hardwareAsset : list){
            dtos.add(new HardwareAssetDTO(hardwareAsset));
        }

        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping(
            value = "/{uuid}/non-hardware-assets",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<List<HardwareAssetDTO>> getNonSoftwareAssets(@PathVariable String uuid) {
        Optional<SoftwareAsset> optional = softwareAssetService.getByUuid(uuid);

        if (!optional.isPresent()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        List<HardwareAsset> all = hardwareAssetService.getAll();
        List<HardwareAsset> list = optional.get().getHardwareAssets();

        if (!list.isEmpty()){
            all.removeAll(list);
        }

        if (all.isEmpty()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        List<HardwareAssetDTO> dtos = new ArrayList<>();
        for (HardwareAsset hardwareAsset : all){
            dtos.add(new HardwareAssetDTO(hardwareAsset));
        }

        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }


    @SuppressWarnings("Duplicates")
    @PostMapping(
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<String> create(@RequestBody SoftwareAssetDTO dto){

        Optional<SoftwareAsset> softwareAsset = softwareAssetService.getByUuid(dto.getUuid());

        if (softwareAsset.isPresent()){
            return new ResponseEntity<>("Asset not saved!", HttpStatus.BAD_REQUEST);
        }

        Optional<Owner> owner = ownerService.getByEmployeeId(dto.getOwner().getEmployeeId());

        if (!owner.isPresent()){
            return new  ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        SoftwareAsset toDb = new SoftwareAsset(
                dto.getName(),
                dto.getDescription(),
                LocalDateTime.now(),
                dto.getType(),
                "created",
                UUID.randomUUID().toString().replaceAll("-", ""),
                owner.get());

        if (dto.getHardwareAssets() != null){
            for (HardwareAssetDTO hardwareAssetDTO : dto.getHardwareAssets()) {
                Optional<HardwareAsset> hardwareAsset = hardwareAssetService.getByUuid(hardwareAssetDTO.getUuid());
                if (hardwareAsset.isPresent()){
                    toDb.getHardwareAssets().add(hardwareAsset.get());
                }
            }
        }

        if (dto.getSecurityGoalValues() != null){
            for (SecurityGoalValueDTO goalValueDTO : dto.getSecurityGoalValues()) {
                Optional<SecurityGoalValue> securityGoalValue = securityGoalValueService.getById(goalValueDTO.getId());
                if (securityGoalValue.isPresent()){
                    toDb.getSecurityGoalValues().add(securityGoalValue.get());
                }
            }
        }

        softwareAssetService.save(toDb);

        return new ResponseEntity<>("Asset saved!", HttpStatus.OK);
    }


    @SuppressWarnings("Duplicates")
    @PutMapping(
            value = "/{uuid}",
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<String> update(@RequestBody SoftwareAssetDTO dto,
                                         @PathVariable String uuid){

        Optional<SoftwareAsset> optional = softwareAssetService.getByUuid(uuid);

        if (!optional.isPresent()){
            return new ResponseEntity<>("Asset not updated!", HttpStatus.BAD_REQUEST);
        }

        SoftwareAsset fromDb = optional.get();

        fromDb.setDescription(dto.getDescription());
        fromDb.setState(dto.getState());

        Optional<Owner> owner = ownerService.getByEmployeeId(dto.getOwner().getEmployeeId());
        if (owner.isPresent()){
            fromDb.setOwner(owner.get());
        }

        fromDb.getSecurityGoalValues().clear();
        if (dto.getSecurityGoalValues() != null){
            for (SecurityGoalValueDTO goalValueDTO : dto.getSecurityGoalValues()) {
                Optional<SecurityGoalValue> securityGoalValue = securityGoalValueService.getById(goalValueDTO.getId());
                if (securityGoalValue.isPresent()){
                    fromDb.getSecurityGoalValues().add(securityGoalValue.get());
                }
            }
        }

        fromDb.getHardwareAssets().clear();
        if (dto.getHardwareAssets() != null){
            for (HardwareAssetDTO hardwareAssetDTO : dto.getHardwareAssets()) {
                Optional<HardwareAsset> hardwareAsset = hardwareAssetService.getByUuid(hardwareAssetDTO.getUuid());
                if (hardwareAsset.isPresent()){
                    fromDb.getHardwareAssets().add(hardwareAsset.get());
                }
            }
        }

        softwareAssetService.save(fromDb);

        return new ResponseEntity<>("Asset updated!", HttpStatus.OK);
    }

    @DeleteMapping(
            value = "/{uuid}"
    )
    public ResponseEntity<String> delete(@PathVariable String uuid) {
        Optional<SoftwareAsset> optional = softwareAssetService.getByUuid(uuid);

        if (!optional.isPresent()){
            return new ResponseEntity<>("Asset not deleted!", HttpStatus.BAD_REQUEST);
        }

        softwareAssetService.delete(optional.get());

        return new ResponseEntity<>("Asset deleted!", HttpStatus.OK);
    }

    @SuppressWarnings("Duplicates")
    @PostMapping(
            value = "/{uuid}/security-goal-values/from-dialog",
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<String> addSecurityGoalValues(@PathVariable String uuid,
                                                        @RequestBody SecurityGoalValueDTO securityGoalValueDTO) {
        Optional<SoftwareAsset> optional = softwareAssetService.getByUuid(uuid);

        if (!optional.isPresent()){
            return new ResponseEntity<>("Software asset not found", HttpStatus.BAD_REQUEST);
        }

        Optional<SecurityGoal> securityGoal = securityGoalService.getById(securityGoalValueDTO.getSecurityGoal().getId());

        if (!securityGoal.isPresent()){
            return new ResponseEntity<>("Security goal not found", HttpStatus.BAD_REQUEST);
        }

        SoftwareAsset softwareAsset = optional.get();

        SecurityGoalValue toDb = new SecurityGoalValue();
        toDb.setName(securityGoalValueDTO.getName());
        toDb.setDescription(securityGoalValueDTO.getDescription());
        toDb.setSecurityGoal(securityGoal.get());

        if (securityGoalValueDTO.getSecurityMeasures() != null ){
            for (SecurityMeasureDTO measureDTO: securityGoalValueDTO.getSecurityMeasures()) {
                Optional<SecurityMeasure> securityMeasure = securityMeasureService.getById(measureDTO.getId());
                if (securityMeasure.isPresent()){
                    toDb.getSecurityMeasures().add(securityMeasure.get());
                }
            }
        }

        SecurityGoalValue value = securityGoalValueService.save(toDb);

        softwareAsset.getSecurityGoalValues().add(value);
        softwareAssetService.save(softwareAsset);

        return new ResponseEntity<>("Software asset updated!", HttpStatus.OK);
    }

    @SuppressWarnings("Duplicates")
    @PostMapping(
            value = "/{uuid}/hardware-assets/from-dialog",
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<String> addHardwareAsset(@PathVariable String uuid,
                                                   @RequestBody HardwareAssetDTO hardwareAssetDTO) {
        Optional<SoftwareAsset> optional = softwareAssetService.getByUuid(uuid);

        if (!optional.isPresent()){
            return new ResponseEntity<>("Software asset not found", HttpStatus.BAD_REQUEST);
        }

        SoftwareAsset softwareAsset = optional.get();

        Optional<Location> locationOptional = locationService.getById(hardwareAssetDTO.getLocation().getId());
        if (!locationOptional.isPresent()){
            return new ResponseEntity<>("Location not found", HttpStatus.BAD_REQUEST);
        }

        Optional<Owner> owner = ownerService.getByEmployeeId(hardwareAssetDTO.getOwner().getEmployeeId());

        if (!owner.isPresent()){
            return new ResponseEntity<>("Owner not found", HttpStatus.BAD_REQUEST);
        }

        HardwareAsset toDb = new HardwareAsset(
                hardwareAssetDTO.getName(),
                hardwareAssetDTO.getDescription(),
                hardwareAssetDTO.getHostName(),
                hardwareAssetDTO.getMacAddress(),
                hardwareAssetDTO.getIpAddress(),
                LocalDateTime.now(),
                null,
                hardwareAssetDTO.getType(),
                "created",
                UUID.randomUUID().toString().replaceAll("-", ""),
                owner.get(),
                hardwareAssetDTO.getSerialNumber(),
                hardwareAssetDTO.getModel(),
                hardwareAssetDTO.getOperatingSystem(),
                locationOptional.get());

        if (hardwareAssetDTO.getSecurityGoalValues() != null){
            for (SecurityGoalValueDTO goalValueDTO : hardwareAssetDTO.getSecurityGoalValues()) {
                Optional<SecurityGoalValue> securityGoalValue = securityGoalValueService.getById(goalValueDTO.getId());
                if (securityGoalValue.isPresent()){
                    toDb.getSecurityGoalValues().add(securityGoalValue.get());
                }
            }
        }

        HardwareAsset ha = hardwareAssetService.save(toDb);


        HardwareAsset asset = hardwareAssetService.save(ha);

        softwareAsset.getHardwareAssets().add(asset);
        softwareAssetService.save(softwareAsset);

        return new ResponseEntity<>("Software asset updated!", HttpStatus.OK);
    }
}
