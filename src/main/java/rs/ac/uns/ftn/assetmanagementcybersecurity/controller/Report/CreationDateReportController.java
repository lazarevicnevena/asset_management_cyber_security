package rs.ac.uns.ftn.assetmanagementcybersecurity.controller.Report;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rs.ac.uns.ftn.assetmanagementcybersecurity.dto.AssetDTO;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.Asset;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.Report.CreationDateReport;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.Report.CustomReport;
import rs.ac.uns.ftn.assetmanagementcybersecurity.service.AssetService;
import rs.ac.uns.ftn.assetmanagementcybersecurity.service.Report.CreationDateReportService;
import rs.ac.uns.ftn.assetmanagementcybersecurity.service.Report.CustomReportService;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/creation-date-reports")
public class CreationDateReportController {

    @Autowired
    private AssetService assetService;

    @Autowired
    private CreationDateReportService creationDateReportService;

    @SuppressWarnings("Duplicates")
    @GetMapping(
            value = "/{id}/filter",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<List<AssetDTO>> creationDateReport(@PathVariable Long id) {

        Optional<CreationDateReport> optional = creationDateReportService.getById(id);

        List<Asset> list = assetService.getAll();

        if (list.isEmpty() || !optional.isPresent()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        List<Asset> finalList = creationDateReportService.filter(list, optional.get());

        if (finalList.isEmpty()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        List<AssetDTO> dtos = assetService.convertAssetsToDTO(finalList);

        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping(
            value = "/{id}",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<CreationDateReport> getOne(@PathVariable Long id) {
        Optional<CreationDateReport> optional = creationDateReportService.getById(id);

        if (!optional.isPresent()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(optional.get(), HttpStatus.OK);
    }

    @GetMapping(
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<List<CreationDateReport>> getAll() {
        List<CreationDateReport> list = creationDateReportService.getAll();

        if (list.isEmpty()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @PostMapping(
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<String> create(@RequestBody CreationDateReport dto){

        if (creationDateReportService.getById(dto.getId()).isPresent()){
            return new ResponseEntity<>("Creation date report not created!", HttpStatus.BAD_REQUEST);
        }

        CreationDateReport toDb = new CreationDateReport();
        toDb.setName(dto.getName());
        toDb.setSubtype(dto.getSubtype());
        toDb.setType(dto.getType());
        toDb.setCreatedFrom(dto.getCreatedFrom());
        toDb.setCreatedTo(dto.getCreatedTo());
        creationDateReportService.save(toDb);

        return new ResponseEntity<>("Creation date report created!", HttpStatus.OK);
    }

    @PutMapping(
            value = "/{id}",
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<String> update(@RequestBody CreationDateReport dto,
                                         @PathVariable Long id){

        Optional<CreationDateReport> optional = creationDateReportService.getById(id);

        if (!optional.isPresent()){
            return new ResponseEntity<>("Creation date report not updated!", HttpStatus.BAD_REQUEST);
        }

        CreationDateReport fromDb = optional.get();

        fromDb.setName(dto.getName());
        fromDb.setSubtype(dto.getSubtype());
        fromDb.setType(dto.getType());
        fromDb.setCreatedFrom(dto.getCreatedFrom());
        fromDb.setCreatedTo(dto.getCreatedTo());
        creationDateReportService.save(fromDb);

        return new ResponseEntity<>("Creation date report updated!", HttpStatus.OK);
    }

    @DeleteMapping(
            value = "/{id}")
    public ResponseEntity<String> delete(@PathVariable Long id) {
        Optional<CreationDateReport> optional = creationDateReportService.getById(id);

        if (!optional.isPresent()){
            return new ResponseEntity<>("Creation date report not deleted!", HttpStatus.BAD_REQUEST);
        }

        creationDateReportService.delete(optional.get());

        return new ResponseEntity<>("Creation date report deleted!", HttpStatus.OK);
    }

}
