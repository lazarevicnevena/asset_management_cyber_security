package rs.ac.uns.ftn.assetmanagementcybersecurity.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.SecurityMeasure;

import java.util.List;
import java.util.Optional;

public interface SecurityMeasureRepository extends JpaRepository<SecurityMeasure, Long> {
    Optional<SecurityMeasure> getByName(String name);
    List<SecurityMeasure> findAllByOrderByIdDesc();
    Page<SecurityMeasure> findAll(Pageable pageable);
    Page<SecurityMeasure> findAllByName(Pageable pageable, String name);
}
