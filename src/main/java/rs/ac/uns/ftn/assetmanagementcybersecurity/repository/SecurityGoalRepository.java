package rs.ac.uns.ftn.assetmanagementcybersecurity.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.SecurityGoal;

import java.util.List;
import java.util.Optional;

public interface SecurityGoalRepository extends JpaRepository<SecurityGoal, Long> {
    Optional<SecurityGoal> getByName(String name);
    List<SecurityGoal> findAllByOrderByIdDesc();
    Page<SecurityGoal> findAll(Pageable pageable);
    Page<SecurityGoal> findAllByName(Pageable pageable, String name);
}
