package rs.ac.uns.ftn.assetmanagementcybersecurity.dto;

import lombok.*;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.Location;

import javax.persistence.Entity;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class LocationDTO {

    private Long id;
    private String building;
    private String room;
    private Integer rack;

    public LocationDTO(Location location){
        this.id = location.getId();
        this.building = location.getBuilding();
        this.room = location.getRoom();
        this.rack = location.getRack();
    }
}
