package rs.ac.uns.ftn.assetmanagementcybersecurity.dto;

import lombok.*;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.AssetType;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.HardwareAsset;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.SecurityGoalValue;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class HardwareAssetDTO {

    private String name;
    private String description;
    private String hostName;
    private String macAddress;
    private String ipAddress;
    private LocalDateTime created;
    private LocalDateTime lastUpdated;
    private AssetType type;
    private String state;
    private String uuid;
    private String serialNumber;
    private String model;
    private OwnerDTO owner;
    private String operatingSystem;
    private LocationDTO location;
    private List<SecurityGoalValueDTO> securityGoalValues = new ArrayList<>();

    public HardwareAssetDTO(HardwareAsset hardwareAsset){
        this.name = hardwareAsset.getName();
        this.description = hardwareAsset.getDescription();
        this.hostName = hardwareAsset.getHostName();
        this.macAddress = hardwareAsset.getMacAddress();
        this.ipAddress = hardwareAsset.getIpAddress();
        this.created = hardwareAsset.getCreated();
        this.lastUpdated = hardwareAsset.getLastUpdated();
        this.type = hardwareAsset.getType();
        this.state = hardwareAsset.getState();
        this.uuid = hardwareAsset.getUuid();
        this.serialNumber = hardwareAsset.getSerialNumber();
        this.model = hardwareAsset.getModel();
        this.owner = new OwnerDTO(hardwareAsset.getOwner());
        this.operatingSystem = hardwareAsset.getOperatingSystem();
        this.location = new LocationDTO(hardwareAsset.getLocation());
        for (SecurityGoalValue value: hardwareAsset.getSecurityGoalValues()){
            this.securityGoalValues.add(new SecurityGoalValueDTO(value));
        }
    }

}
