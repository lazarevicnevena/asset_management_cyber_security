package rs.ac.uns.ftn.assetmanagementcybersecurity.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.assetmanagementcybersecurity.dto.AssetDTO;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.*;
import rs.ac.uns.ftn.assetmanagementcybersecurity.repository.AssetRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class AssetService {

    @Autowired
    private AssetRepository assetRepository;

    public Optional<Asset> getByUuid(String uuid) {
        return assetRepository.getByUuid(uuid);
    }

    public List<Asset> getAll(){
        return assetRepository.findAllByOrderByIdDesc();
    }

    public List<Asset> getAllPageable(Pageable pageable){
        Page<Asset> page = assetRepository.findAll(pageable);
        return page.getContent();
    }

    public Asset save(Asset asset){
        return assetRepository.save(asset);
    }

    public void delete(Asset asset){
        assetRepository.delete(asset);
    }

    public List<AssetDTO> convertAssetsToDTO(List<Asset> list){
        List<AssetDTO> dtos = new ArrayList<>();
        for (Asset asset: list){
            if (asset instanceof HardwareAsset){
                dtos.add(new AssetDTO((HardwareAsset) asset));
            }else if (asset instanceof SoftwareAsset) {
                dtos.add(new AssetDTO((SoftwareAsset) asset));
            } else if (asset instanceof VirtualAsset){
                dtos.add(new AssetDTO((VirtualAsset) asset));
            } else if (asset instanceof DataAsset) {
                dtos.add(new AssetDTO((DataAsset) asset));
            }
        }
        return dtos;
    }
}
