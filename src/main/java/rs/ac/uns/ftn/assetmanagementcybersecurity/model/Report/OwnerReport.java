package rs.ac.uns.ftn.assetmanagementcybersecurity.model.Report;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
public class OwnerReport {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String firstName;
    private String lastName;
    private String middleName;
    private String employeeNr;
    private String phoneNumber;

    public OwnerReport(String firstName, String lastName,
                       String middleName, String employeeNr,
                       String phoneNumber){
        this.firstName = firstName;
        this.lastName = lastName;
        this.middleName = middleName;
        this.employeeNr = employeeNr;
        this.phoneNumber = phoneNumber;
    }
}
