package rs.ac.uns.ftn.assetmanagementcybersecurity.model;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
public class DataAsset extends Asset {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    public DataAsset(String name,
                     String description,
                     LocalDateTime created,
                     AssetType type,
                     String state,
                     String uuid,
                     Owner owner){
        this.setName(name);
        this.setDescription(description);
        this.setCreated(created);
        this.setType(type);
        this.setState(state);
        this.setUuid(uuid);
        this.setOwner(owner);
    }
}
