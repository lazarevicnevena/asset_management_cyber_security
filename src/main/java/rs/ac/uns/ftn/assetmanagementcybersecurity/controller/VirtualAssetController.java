package rs.ac.uns.ftn.assetmanagementcybersecurity.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rs.ac.uns.ftn.assetmanagementcybersecurity.dto.*;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.*;
import rs.ac.uns.ftn.assetmanagementcybersecurity.service.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping(value = "/virtual-assets")
public class VirtualAssetController {

    @Autowired
    private VirtualAssetService virtualAssetService;

    @Autowired
    private HardwareAssetService hardwareAssetService;

    @Autowired
    private OwnerService ownerService;

    @Autowired
    private SecurityGoalValueService securityGoalValueService;

    @Autowired
    private SecurityGoalService securityGoalService;

    @Autowired
    private SecurityMeasureService securityMeasureService;

    @GetMapping(
            value = "/{uuid}",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<VirtualAssetDTO> getOne(@PathVariable String uuid) {
        Optional<VirtualAsset> optional = virtualAssetService.getByUuid(uuid);

        if (!optional.isPresent()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(new VirtualAssetDTO(optional.get()), HttpStatus.OK);
    }


    @GetMapping(
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<List<VirtualAssetDTO>> getAll() {

        List<VirtualAsset> list = virtualAssetService.getAll();

        if (list.isEmpty()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        List<VirtualAssetDTO> dtos = new ArrayList<>();
        for (VirtualAsset virtualAsset : list){
            dtos.add(new VirtualAssetDTO(virtualAsset));
        }

        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @SuppressWarnings("Duplicates")
    @PostMapping(
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<String> create(@RequestBody VirtualAssetDTO dto){

        Optional<VirtualAsset> virtualAsset = virtualAssetService.getByUuid(dto.getUuid());

        if (virtualAsset.isPresent()){
            return new ResponseEntity<>("Asset not saved!", HttpStatus.BAD_REQUEST);
        }

        Optional<Owner> owner = ownerService.getByEmployeeId(dto.getOwner().getEmployeeId());

        if (!owner.isPresent()){
            return new  ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        Optional<HardwareAsset> hardwareAsset = hardwareAssetService.getByUuid(dto.getHardwareAsset().getUuid());

        if (!hardwareAsset.isPresent()){
            return new  ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        VirtualAsset toDb = new VirtualAsset(
                dto.getName(),
                dto.getDescription(),
                dto.getMacAddress(),
                dto.getIpAddress(),
                LocalDateTime.now(),
                null,
                dto.getType(),
                "created",
                UUID.randomUUID().toString().replaceAll("-", ""),
                owner.get(),
                dto.getOperatingSystem(),
                hardwareAsset.get());

        if (dto.getSecurityGoalValues() != null){
            for (SecurityGoalValueDTO goalValueDTO : dto.getSecurityGoalValues()) {
                Optional<SecurityGoalValue> securityGoalValue = securityGoalValueService.getById(goalValueDTO.getId());
                if (securityGoalValue.isPresent()){
                    toDb.getSecurityGoalValues().add(securityGoalValue.get());
                }
            }
        }

        virtualAssetService.save(toDb);

        return new ResponseEntity<>("Asset saved!", HttpStatus.OK);
    }


    @SuppressWarnings("Duplicates")
    @PutMapping(
            value = "/{uuid}",
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<String> update(@RequestBody VirtualAssetDTO dto,
                                         @PathVariable String uuid){

        Optional<VirtualAsset> optional = virtualAssetService.getByUuid(uuid);

        if (!optional.isPresent()){
            return new ResponseEntity<>("Asset not updated!", HttpStatus.BAD_REQUEST);
        }

        VirtualAsset fromDb = optional.get();

        fromDb.setDescription(dto.getDescription());
        fromDb.setMacAddress(dto.getMacAddress());
        fromDb.setIpAddress(dto.getIpAddress());
        fromDb.setOperatingSystem(dto.getOperatingSystem());
        fromDb.setState(dto.getState());

        Optional<Owner> owner = ownerService.getByEmployeeId(dto.getOwner().getEmployeeId());
        if (owner.isPresent()){
            fromDb.setOwner(owner.get());
        }

        fromDb.getSecurityGoalValues().clear();
        if (dto.getSecurityGoalValues() != null){
            for (SecurityGoalValueDTO goalValueDTO : dto.getSecurityGoalValues()) {
                Optional<SecurityGoalValue> securityGoalValue = securityGoalValueService.getById(goalValueDTO.getId());
                if (securityGoalValue.isPresent()){
                    fromDb.getSecurityGoalValues().add(securityGoalValue.get());
                }
            }
        }

        virtualAssetService.save(fromDb);

        return new ResponseEntity<>("Asset updated!", HttpStatus.OK);
    }


    @DeleteMapping(
            value = "/{uuid}"
    )
    public ResponseEntity<String> delete(@PathVariable String uuid) {
        Optional<VirtualAsset> optional = virtualAssetService.getByUuid(uuid);

        if (!optional.isPresent()){
            return new ResponseEntity<>("Asset not deleted!", HttpStatus.BAD_REQUEST);
        }

        virtualAssetService.delete(optional.get());

        return new ResponseEntity<>("Asset deleted!", HttpStatus.OK);
    }

    @SuppressWarnings("Duplicates")
    @PostMapping(
            value = "/{uuid}/security-goal-values/from-dialog",
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<String> addSecurityGoalValues(@PathVariable String uuid,
                                                        @RequestBody SecurityGoalValueDTO securityGoalValueDTO) {
        Optional<VirtualAsset> optional = virtualAssetService.getByUuid(uuid);

        if (!optional.isPresent()){
            return new ResponseEntity<>("Virtual asset not found", HttpStatus.BAD_REQUEST);
        }

        Optional<SecurityGoal> securityGoal = securityGoalService.getById(securityGoalValueDTO.getSecurityGoal().getId());

        if (!securityGoal.isPresent()){
            return new ResponseEntity<>("Security goal not found", HttpStatus.BAD_REQUEST);
        }

        VirtualAsset virtualAsset = optional.get();

        SecurityGoalValue toDb = new SecurityGoalValue();
        toDb.setName(securityGoalValueDTO.getName());
        toDb.setDescription(securityGoalValueDTO.getDescription());
        toDb.setSecurityGoal(securityGoal.get());

        if (securityGoalValueDTO.getSecurityMeasures() != null){
            for (SecurityMeasureDTO measureDTO: securityGoalValueDTO.getSecurityMeasures()) {
                Optional<SecurityMeasure> securityMeasure = securityMeasureService.getById(measureDTO.getId());
                if (securityMeasure.isPresent()){
                    toDb.getSecurityMeasures().add(securityMeasure.get());
                }
            }
        }

        SecurityGoalValue value = securityGoalValueService.save(toDb);

        virtualAsset.getSecurityGoalValues().add(value);
        virtualAssetService.save(virtualAsset);

        return new ResponseEntity<>("Virtual asset updated!", HttpStatus.OK);
    }
}
