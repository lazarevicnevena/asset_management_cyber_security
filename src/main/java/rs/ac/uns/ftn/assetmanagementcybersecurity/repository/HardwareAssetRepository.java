package rs.ac.uns.ftn.assetmanagementcybersecurity.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.HardwareAsset;

import java.util.List;
import java.util.Optional;

public interface HardwareAssetRepository extends JpaRepository<HardwareAsset, Long> {
    Optional<HardwareAsset> getByUuid(String uuid);
    List<HardwareAsset> findAllByOrderByIdDesc();
    Page<HardwareAsset> findAll(Pageable pageable);
}
