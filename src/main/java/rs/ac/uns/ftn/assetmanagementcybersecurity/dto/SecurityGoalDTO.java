package rs.ac.uns.ftn.assetmanagementcybersecurity.dto;

import lombok.*;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.SecurityGoal;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.SecurityGoalValue;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class SecurityGoalDTO {
    private Long id;

    private String name;

    public SecurityGoalDTO(SecurityGoal securityGoal){
        this.id = securityGoal.getId();
        this.name = securityGoal.getName();

    }

}
