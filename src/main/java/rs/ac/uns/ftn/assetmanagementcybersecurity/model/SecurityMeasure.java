package rs.ac.uns.ftn.assetmanagementcybersecurity.model;

import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
public class SecurityMeasure {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;
    private String description;

    @OneToOne(fetch = FetchType.EAGER)
    private SecurityMeasureType type;

    public SecurityMeasure(String name,
                           String description,
                           SecurityMeasureType securityMeasureType){
        this.name = name;
        this.description = description;
        this.type = securityMeasureType;
    }

}