package rs.ac.uns.ftn.assetmanagementcybersecurity.repository.Report;

import org.springframework.data.jpa.repository.JpaRepository;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.Report.NetworkReport;

import java.util.List;


public interface NetworkReportRepository extends JpaRepository<NetworkReport, Long> {
    List<NetworkReport> findAllByOrderByIdDesc();
}
