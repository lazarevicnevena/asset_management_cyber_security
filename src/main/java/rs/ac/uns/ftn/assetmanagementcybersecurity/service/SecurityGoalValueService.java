package rs.ac.uns.ftn.assetmanagementcybersecurity.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.SecurityGoal;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.SecurityGoalValue;
import rs.ac.uns.ftn.assetmanagementcybersecurity.repository.SecurityGoalRepository;
import rs.ac.uns.ftn.assetmanagementcybersecurity.repository.SecurityGoalValueRepository;

import java.util.List;
import java.util.Optional;

@Service
public class SecurityGoalValueService {

    @Autowired
    private SecurityGoalValueRepository securityGoalValueRepository;

    public Optional<SecurityGoalValue> getById(Long id) {
        return securityGoalValueRepository.findById(id);
    }

    public List<SecurityGoalValue> getAll(){
        return securityGoalValueRepository.findAllByOrderByIdDesc();
    }

    public List<SecurityGoalValue> getAllPageable(Pageable pageable){
        return securityGoalValueRepository.findAll(pageable).getContent();
    }

    public List<SecurityGoalValue> getByName(String name){
        return securityGoalValueRepository.getByName(name);
    }

    public SecurityGoalValue save(SecurityGoalValue securityGoalValue){
        return securityGoalValueRepository.save(securityGoalValue);
    }

    public void delete(SecurityGoalValue securityGoalValue){
        securityGoalValueRepository.delete(securityGoalValue);
    }

}
