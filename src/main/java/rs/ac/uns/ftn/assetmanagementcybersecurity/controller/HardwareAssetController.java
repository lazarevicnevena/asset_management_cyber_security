package rs.ac.uns.ftn.assetmanagementcybersecurity.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rs.ac.uns.ftn.assetmanagementcybersecurity.dto.HardwareAssetDTO;
import rs.ac.uns.ftn.assetmanagementcybersecurity.dto.SecurityGoalValueDTO;
import rs.ac.uns.ftn.assetmanagementcybersecurity.dto.SecurityMeasureDTO;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.*;
import rs.ac.uns.ftn.assetmanagementcybersecurity.service.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping(value = "/hardware-assets")
public class HardwareAssetController {

    @Autowired
    private HardwareAssetService hardwareAssetService;

    @Autowired
    private LocationService locationService;

    @Autowired
    private OwnerService ownerService;

    @Autowired
    private SecurityGoalValueService securityGoalValueService;

    @Autowired
    private SecurityGoalService securityGoalService;

    @Autowired
    private SecurityMeasureService securityMeasureService;

    @GetMapping(
            value = "/{uuid}",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<HardwareAssetDTO> getOne(@PathVariable String uuid) {
        Optional<HardwareAsset> optional = hardwareAssetService.getByUuid(uuid);

        if (!optional.isPresent()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(new HardwareAssetDTO(optional.get()), HttpStatus.OK);
    }

    @GetMapping(
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<List<HardwareAssetDTO>> getAll() {

        List<HardwareAsset> list = hardwareAssetService.getAll();

        if (list.isEmpty()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        List<HardwareAssetDTO> dtos = new ArrayList<>();
        for (HardwareAsset hardwareAsset : list){
            dtos.add(new HardwareAssetDTO(hardwareAsset));
        }

        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @SuppressWarnings("Duplicates")
    @PostMapping(
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<String> create(@RequestBody HardwareAssetDTO dto){

        if (hardwareAssetService.getByUuid(dto.getUuid()).isPresent()){
            return new ResponseEntity<>("Asset not saved!", HttpStatus.BAD_REQUEST);
        }

        Optional<Location> locationOptional = locationService.getById(dto.getLocation().getId());
        if (!locationOptional.isPresent()){
            return new ResponseEntity<>("Location not found", HttpStatus.BAD_REQUEST);
        }

        Optional<Owner> owner = ownerService.getByEmployeeId(dto.getOwner().getEmployeeId());

        if (!owner.isPresent()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        HardwareAsset toDb = new HardwareAsset(
                dto.getName(),
                dto.getDescription(),
                dto.getHostName(),
                dto.getMacAddress(),
                dto.getIpAddress(),
                LocalDateTime.now(),
                null,
                dto.getType(),
                "created",
                UUID.randomUUID().toString().replaceAll("-", ""),
                owner.get(),
                dto.getSerialNumber(),
                dto.getModel(),
                dto.getOperatingSystem(),
                locationOptional.get());

        if (dto.getSecurityGoalValues() != null ){
            for (SecurityGoalValueDTO goalValueDTO : dto.getSecurityGoalValues()) {
                Optional<SecurityGoalValue> securityGoalValue = securityGoalValueService.getById(goalValueDTO.getId());
                if (securityGoalValue.isPresent()){
                    toDb.getSecurityGoalValues().add(securityGoalValue.get());
                }
            }
        }

        hardwareAssetService.save(toDb);

        return new ResponseEntity<>("Asset saved!", HttpStatus.OK);
    }


    @SuppressWarnings("Duplicates")
    @PutMapping(
            value = "/{uuid}",
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<String> update(@RequestBody HardwareAssetDTO dto,
                                         @PathVariable String uuid){

        Optional<HardwareAsset> optional = hardwareAssetService.getByUuid(uuid);

        if (!optional.isPresent()){
            return new ResponseEntity<>("Asset not updated!", HttpStatus.BAD_REQUEST);
        }

        HardwareAsset fromDb = optional.get();

        fromDb.setDescription(dto.getDescription());
        fromDb.setHostName(dto.getHostName());
        fromDb.setIpAddress(dto.getIpAddress());
        fromDb.setMacAddress(dto.getMacAddress());
        fromDb.setSerialNumber(dto.getSerialNumber());
        fromDb.setState(dto.getState());

        Optional<Owner> owner = ownerService.getByEmployeeId(dto.getOwner().getEmployeeId());
        if (owner.isPresent()){
            fromDb.setOwner(owner.get());
        }
        Optional<Location> location = locationService.getByBuildingAndRoomAndRack(
                dto.getLocation().getBuilding(),
                dto.getLocation().getRoom(),
                dto.getLocation().getRack());

        if (location.isPresent()){
            fromDb.setLocation(location.get());
        }

        fromDb.getSecurityGoalValues().clear();
        if (dto.getSecurityGoalValues() != null){
            for (SecurityGoalValueDTO goalValueDTO : dto.getSecurityGoalValues()) {
                Optional<SecurityGoalValue> securityGoalValue = securityGoalValueService.getById(goalValueDTO.getId());
                if (securityGoalValue.isPresent()){
                    fromDb.getSecurityGoalValues().add(securityGoalValue.get());
                }
            }
        }

        hardwareAssetService.save(fromDb);

        return new ResponseEntity<>("Asset updated!", HttpStatus.OK);
    }


    @DeleteMapping(
            value = "/{uuid}"
    )
    public ResponseEntity<String> delete(@PathVariable String uuid) {
        Optional<HardwareAsset> optional = hardwareAssetService.getByUuid(uuid);

        if (!optional.isPresent()){
            return new ResponseEntity<>("Asset not deleted!", HttpStatus.BAD_REQUEST);
        }

        hardwareAssetService.delete(optional.get());

        return new ResponseEntity<>("Asset deleted!", HttpStatus.OK);
    }

    @SuppressWarnings("Duplicates")
    @PostMapping(
            value = "/{uuid}/security-goal-values/from-dialog",
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<String> addSecurityGoalValues(@PathVariable String uuid,
                                                        @RequestBody SecurityGoalValueDTO securityGoalValueDTO) {
        Optional<HardwareAsset> optional = hardwareAssetService.getByUuid(uuid);

        if (!optional.isPresent()){
            return new ResponseEntity<>("Hardware asset not found", HttpStatus.BAD_REQUEST);
        }

        Optional<SecurityGoal> securityGoal = securityGoalService.getById(securityGoalValueDTO.getSecurityGoal().getId());

        if (!securityGoal.isPresent()){
            return new ResponseEntity<>("Security goal not found", HttpStatus.BAD_REQUEST);
        }

        HardwareAsset hardwareAsset = optional.get();

        SecurityGoalValue toDb = new SecurityGoalValue();
        toDb.setName(securityGoalValueDTO.getName());
        toDb.setDescription(securityGoalValueDTO.getDescription());
        toDb.setSecurityGoal(securityGoal.get());

        if (securityGoalValueDTO.getSecurityMeasures() != null){
            for (SecurityMeasureDTO measureDTO: securityGoalValueDTO.getSecurityMeasures()) {
                Optional<SecurityMeasure> securityMeasure = securityMeasureService.getById(measureDTO.getId());
                if (securityMeasure.isPresent()){
                    toDb.getSecurityMeasures().add(securityMeasure.get());
                }
            }
        }

        SecurityGoalValue value = securityGoalValueService.save(toDb);

        hardwareAsset.getSecurityGoalValues().add(value);
        hardwareAssetService.save(hardwareAsset);

        return new ResponseEntity<>("Hardware asset updated!", HttpStatus.OK);
    }
}
