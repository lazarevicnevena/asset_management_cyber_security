package rs.ac.uns.ftn.assetmanagementcybersecurity.service.Report;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.*;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.Report.CreationDateReport;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.Report.CustomReport;
import rs.ac.uns.ftn.assetmanagementcybersecurity.repository.Report.CreationDateReportRepository;
import rs.ac.uns.ftn.assetmanagementcybersecurity.repository.Report.CustomReportRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CreationDateReportService {

    @Autowired
    private CreationDateReportRepository creationDateReportRepository;

    public Optional<CreationDateReport> getById(Long id) {
        return creationDateReportRepository.findById(id);
    }

    public List<CreationDateReport> getAll(){
        return creationDateReportRepository.findAllByOrderByIdDesc();
    }

    public CreationDateReport save(CreationDateReport creationDateReport){
        return creationDateReportRepository.save(creationDateReport);
    }

    public void delete(CreationDateReport creationDateReport){
        creationDateReportRepository.delete(creationDateReport);
    }


    public List<Asset> filter(List<Asset> list, CreationDateReport report)
    {
        List<Asset> assets = new ArrayList<>();

        for (Asset asset: list){
            if (report.getName() != null && !report.getName().equals("") && !asset.getName().toLowerCase().contains(report.getName().toLowerCase())) {
                continue;
            }
            if (report.getSubtype() != null && !report.getSubtype().equals("ANY")) {
                if ((report.getSubtype().equals("HARDWARE") && !(asset instanceof HardwareAsset)) ||
                        (report.getSubtype().equals("SOFTWARE") && !(asset instanceof SoftwareAsset)) ||
                        (report.getSubtype().equals("VIRTUAL") && !(asset instanceof VirtualAsset)) ||
                        (report.getSubtype().equals("DATA") && !(asset instanceof DataAsset))){
                    continue;
                }
            }
            if (report.getType() != null && !report.getType().equals("ANY") && !asset.getType().equals(AssetType.valueOf(report.getType()))) {
                continue;
            }
            if (!(asset.getCreated().isAfter(report.getCreatedFrom())) && asset.getCreated().isBefore(report.getCreatedTo())){
                continue;
            }

            assets.add(asset);
        }

        return  assets;
    }

}
