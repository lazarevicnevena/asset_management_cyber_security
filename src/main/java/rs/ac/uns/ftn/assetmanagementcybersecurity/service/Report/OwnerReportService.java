package rs.ac.uns.ftn.assetmanagementcybersecurity.service.Report;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.Asset;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.Report.OwnerReport;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.Report.SecurityReport;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.SecurityGoalValue;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.SecurityMeasure;
import rs.ac.uns.ftn.assetmanagementcybersecurity.repository.Report.OwnerReportRepository;
import rs.ac.uns.ftn.assetmanagementcybersecurity.repository.Report.SecurityReportRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class OwnerReportService {

    @Autowired
    private OwnerReportRepository ownerReportRepository;

    public Optional<OwnerReport> getById(Long id) {
        return ownerReportRepository.findById(id);
    }

    public List<OwnerReport> getAll(){
        return ownerReportRepository.findAllByOrderByIdDesc();
    }

    public OwnerReport save(OwnerReport report){
        return ownerReportRepository.save(report);
    }

    public void delete(OwnerReport report){
        ownerReportRepository.delete(report);
    }


    public List<Asset> filter(List<Asset> list, OwnerReport report)
    {
        List<Asset> assets = new ArrayList<>();

        for (Asset asset: list){
            if (report.getFirstName() != null && !report.getFirstName().equals("") &&
                    !asset.getOwner().getFirstName().toLowerCase().contains(report.getFirstName().toLowerCase())) {
                continue;
            }
            if (report.getLastName() != null && !report.getLastName().equals("") &&
                    !asset.getOwner().getLastName().toLowerCase().contains(report.getLastName().toLowerCase())) {
                continue;
            }
            if (report.getMiddleName() != null && !report.getMiddleName().equals("") &&
                    !asset.getOwner().getMiddleName().toLowerCase().contains(report.getMiddleName().toLowerCase())) {
                continue;
            }
            if (report.getEmployeeNr() != null && !report.getEmployeeNr().equals("") &&
                    !asset.getOwner().getEmployeeId().toLowerCase().contains(report.getEmployeeNr().toLowerCase())) {
                continue;
            }
            if (report.getPhoneNumber() != null && !report.getPhoneNumber().equals("") &&
                    !asset.getOwner().getPhoneNumber().toLowerCase().contains(report.getPhoneNumber().toLowerCase())) {
                continue;
            }
            assets.add(asset);
        }

        return  assets;
    }

}
