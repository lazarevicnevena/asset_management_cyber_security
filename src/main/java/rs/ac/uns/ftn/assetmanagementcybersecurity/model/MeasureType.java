package rs.ac.uns.ftn.assetmanagementcybersecurity.model;

public enum MeasureType {
    ANTIVIRUS,
    FIREWALL,
    UPDATING,
    BACKUP,
    CRYPTOGRAPHY,
    ACCESS_CONTROL,
    LOGIN_CONTROL
}
