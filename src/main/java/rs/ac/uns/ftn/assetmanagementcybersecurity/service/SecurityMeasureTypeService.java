package rs.ac.uns.ftn.assetmanagementcybersecurity.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.SecurityGoal;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.SecurityMeasure;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.SecurityMeasureType;
import rs.ac.uns.ftn.assetmanagementcybersecurity.repository.SecurityGoalRepository;
import rs.ac.uns.ftn.assetmanagementcybersecurity.repository.SecurityMeasureTypeRepository;

import java.util.List;
import java.util.Optional;

@Service
public class SecurityMeasureTypeService {

    @Autowired
    private SecurityMeasureTypeRepository securityMeasureTypeRepository;

    public Optional<SecurityMeasureType> getById(Long id) {
        return securityMeasureTypeRepository.findById(id);
    }

    public List<SecurityMeasureType> getAll(){
        return securityMeasureTypeRepository.findAllByOrderByIdDesc();
    }

    public List<SecurityMeasureType> getAllPageable(Pageable pageable){
        return securityMeasureTypeRepository.findAll(pageable).getContent();
    }

    public List<SecurityMeasureType> getAllPageableAndName(Pageable pageable, String name){
        return securityMeasureTypeRepository.findAllByName(pageable, name).getContent();
    }

    public Optional<SecurityMeasureType> getByName(String name){
        return securityMeasureTypeRepository.getByName(name);
    }

    public SecurityMeasureType save(SecurityMeasureType securityMeasureType){
        return securityMeasureTypeRepository.save(securityMeasureType);
    }

    public void delete(SecurityMeasureType securityMeasureType){
        securityMeasureTypeRepository.delete(securityMeasureType);
    }

}
