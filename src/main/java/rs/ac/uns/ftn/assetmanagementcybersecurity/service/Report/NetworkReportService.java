package rs.ac.uns.ftn.assetmanagementcybersecurity.service.Report;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.Asset;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.AssetNetwork;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.Report.NetworkReport;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.Report.OwnerReport;
import rs.ac.uns.ftn.assetmanagementcybersecurity.repository.Report.NetworkReportRepository;
import rs.ac.uns.ftn.assetmanagementcybersecurity.repository.Report.OwnerReportRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class NetworkReportService {

    @Autowired
    private NetworkReportRepository networkReportRepository;

    public Optional<NetworkReport> getById(Long id) {
        return networkReportRepository.findById(id);
    }

    public List<NetworkReport> getAll(){
        return networkReportRepository.findAllByOrderByIdDesc();
    }

    public NetworkReport save(NetworkReport report){
        return networkReportRepository.save(report);
    }

    public void delete(NetworkReport report){
        networkReportRepository.delete(report);
    }


    public List<Asset> filter(List<AssetNetwork> networks, NetworkReport report)
    {

        List<Asset> assets = new ArrayList<>();

        for (AssetNetwork network:  networks){
            if (report.getName() != null && !report.getName().equals("") &&
                    !network.getName().toLowerCase().contains(report.getName().toLowerCase())) {
                continue;
            }
            if (report.getDescription() != null && !report.getDescription().equals("") &&
                    !network.getDescription().toLowerCase().contains(report.getDescription().toLowerCase())) {
                continue;
            }

            if (report.getMinAssetNr() != 0) {
                if (network.getAssets().size() <= report.getMinAssetNr()) {
                    continue;
                }
            }
            if (report.getMaxAssetNr() != 0) {
                if (network.getAssets().size() >= report.getMaxAssetNr()) {
                    continue;
                }
            }
            if (!network.getAssets().isEmpty()) {
                if (assets.isEmpty()) {
                    assets.addAll(network.getAssets());
                } else {
                    for (Asset a: network.getAssets()) {
                        if (!assets.contains(a)){
                            assets.add(a);
                        }
                    }
                }
            }
        }

        return  assets;
    }

}
