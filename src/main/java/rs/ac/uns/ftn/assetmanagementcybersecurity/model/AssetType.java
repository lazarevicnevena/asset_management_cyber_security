package rs.ac.uns.ftn.assetmanagementcybersecurity.model;

public enum AssetType {
    WORK_STATION,
    LAPTOP,
    ROUTER,
    SERVER,
    PRINTER,
    VIRTUAL_MACHINE,
    APPLICATION,
    FILE
}
