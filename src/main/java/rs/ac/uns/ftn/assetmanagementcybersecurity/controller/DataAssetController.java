package rs.ac.uns.ftn.assetmanagementcybersecurity.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rs.ac.uns.ftn.assetmanagementcybersecurity.dto.DataAssetDTO;
import rs.ac.uns.ftn.assetmanagementcybersecurity.dto.SecurityGoalValueDTO;
import rs.ac.uns.ftn.assetmanagementcybersecurity.dto.SecurityMeasureDTO;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.*;
import rs.ac.uns.ftn.assetmanagementcybersecurity.service.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping(value = "/data-assets")
public class DataAssetController {

    @Autowired
    private DataAssetService dataAssetService;

    @Autowired
    private OwnerService ownerService;

    @Autowired
    private SecurityGoalValueService securityGoalValueService;

    @Autowired
    private SecurityGoalService securityGoalService;

    @Autowired
    private SecurityMeasureService securityMeasureService;

    @GetMapping(
            value = "/{uuid}",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<DataAssetDTO> getOne(@PathVariable String uuid) {
        Optional<DataAsset> optional = dataAssetService.getByUuid(uuid);

        if (!optional.isPresent()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(new DataAssetDTO(optional.get()), HttpStatus.OK);
    }


    @GetMapping(
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<List<DataAssetDTO>> getAll() {

        List<DataAsset> list = dataAssetService.getAll();

        if (list.isEmpty()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        List<DataAssetDTO> dtos = new ArrayList<>();
        for (DataAsset dataAsset : list){
            dtos.add(new DataAssetDTO(dataAsset));
        }

        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @SuppressWarnings("Duplicates")
    @PostMapping(
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<String> create(@RequestBody DataAssetDTO dto){

        Optional<DataAsset> dataAsset = dataAssetService.getByUuid(dto.getUuid());

        if (dataAsset.isPresent()){
            return new ResponseEntity<>("Asset not saved!", HttpStatus.BAD_REQUEST);
        }

        Optional<Owner> owner = ownerService.getByEmployeeId(dto.getOwner().getEmployeeId());

        if (!owner.isPresent()){
            return new  ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        DataAsset toDb = new DataAsset(
                dto.getName(),
                dto.getDescription(),
                LocalDateTime.now(),
                dto.getType(),
                "created",
                UUID.randomUUID().toString().replaceAll("-", ""),
                owner.get());

        if (dto.getSecurityGoalValues() != null){
            for (SecurityGoalValueDTO goalValueDTO : dto.getSecurityGoalValues()) {
                Optional<SecurityGoalValue> securityGoalValue = securityGoalValueService.getById(goalValueDTO.getId());
                if (securityGoalValue.isPresent()){
                    toDb.getSecurityGoalValues().add(securityGoalValue.get());
                }
            }
        }

        dataAssetService.save(toDb);

        return new ResponseEntity<>("Asset saved!", HttpStatus.OK);
    }


    @SuppressWarnings("Duplicates")
    @PutMapping(
            value = "/{uuid}",
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<String> update(@RequestBody DataAssetDTO dto,
                                         @PathVariable String uuid){

        Optional<DataAsset> optional = dataAssetService.getByUuid(uuid);

        if (!optional.isPresent()){
            return new ResponseEntity<>("Asset not updated!", HttpStatus.BAD_REQUEST);
        }

        DataAsset fromDb = optional.get();

        fromDb.setDescription(dto.getDescription());
        fromDb.setState(dto.getState());
        fromDb.setType(dto.getType());
        fromDb.setName(dto.getName());

        Optional<Owner> owner = ownerService.getByEmployeeId(dto.getOwner().getEmployeeId());
        if (owner.isPresent()){
            fromDb.setOwner(owner.get());
        }

        fromDb.getSecurityGoalValues().clear();
        if (dto.getSecurityGoalValues() != null){
            for (SecurityGoalValueDTO goalValueDTO : dto.getSecurityGoalValues()) {
                Optional<SecurityGoalValue> securityGoalValue = securityGoalValueService.getById(goalValueDTO.getId());
                if (securityGoalValue.isPresent()){
                    fromDb.getSecurityGoalValues().add(securityGoalValue.get());
                }
            }
        }

        dataAssetService.save(fromDb);

        return new ResponseEntity<>("Asset updated!", HttpStatus.OK);
    }


    @DeleteMapping(
            value = "/{uuid}"
    )
    public ResponseEntity<String> delete(@PathVariable String uuid) {
        Optional<DataAsset> optional = dataAssetService.getByUuid(uuid);

        if (!optional.isPresent()){
            return new ResponseEntity<>("Asset not deleted!", HttpStatus.BAD_REQUEST);
        }

        dataAssetService.delete(optional.get());

        return new ResponseEntity<>("Asset deleted!", HttpStatus.OK);
    }

    @SuppressWarnings("Duplicates")
    @PostMapping(
            value = "/{uuid}/security-goal-values/from-dialog",
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<String> addSecurityGoalValues(@PathVariable String uuid,
                                                        @RequestBody SecurityGoalValueDTO securityGoalValueDTO) {
        Optional<DataAsset> optional = dataAssetService.getByUuid(uuid);

        if (!optional.isPresent()){
            return new ResponseEntity<>("Data asset not found", HttpStatus.BAD_REQUEST);
        }

        Optional<SecurityGoal> securityGoal = securityGoalService.getById(securityGoalValueDTO.getSecurityGoal().getId());

        if (!securityGoal.isPresent()){
            return new ResponseEntity<>("Security goal not found", HttpStatus.BAD_REQUEST);
        }

        DataAsset dataAsset = optional.get();

        SecurityGoalValue toDb = new SecurityGoalValue();
        toDb.setName(securityGoalValueDTO.getName());
        toDb.setDescription(securityGoalValueDTO.getDescription());
        toDb.setSecurityGoal(securityGoal.get());

        if (securityGoalValueDTO.getSecurityMeasures() != null){
            for (SecurityMeasureDTO measureDTO: securityGoalValueDTO.getSecurityMeasures()) {
                Optional<SecurityMeasure> securityMeasure = securityMeasureService.getById(measureDTO.getId());
                if (securityMeasure.isPresent()){
                    toDb.getSecurityMeasures().add(securityMeasure.get());
                }
            }
        }

        SecurityGoalValue value = securityGoalValueService.save(toDb);

        dataAsset.getSecurityGoalValues().add(value);
        dataAssetService.save(dataAsset);

        return new ResponseEntity<>("Data asset updated!", HttpStatus.OK);
    }
}
