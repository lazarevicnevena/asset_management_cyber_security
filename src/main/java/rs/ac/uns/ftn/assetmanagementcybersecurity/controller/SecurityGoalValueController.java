package rs.ac.uns.ftn.assetmanagementcybersecurity.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rs.ac.uns.ftn.assetmanagementcybersecurity.dto.SecurityGoalDTO;
import rs.ac.uns.ftn.assetmanagementcybersecurity.dto.SecurityGoalValueDTO;
import rs.ac.uns.ftn.assetmanagementcybersecurity.dto.SecurityMeasureDTO;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.*;
import rs.ac.uns.ftn.assetmanagementcybersecurity.service.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/security-goal-values")
public class SecurityGoalValueController {

    @Autowired
    private SecurityGoalValueService securityGoalValueService;

    @Autowired
    private SecurityGoalService securityGoalService;

    @Autowired
    private AssetService assetService;

    @Autowired
    private SecurityMeasureService securityMeasureService;

    @Autowired
    private SecurityMeasureTypeService securityMeasureTypeService;

    @GetMapping(
            value = "/{id}",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<SecurityGoalValueDTO> getOne(@PathVariable Long id) {
        Optional<SecurityGoalValue> optional = securityGoalValueService.getById(id);

        if (!optional.isPresent()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(new SecurityGoalValueDTO(optional.get()), HttpStatus.OK);
    }

    @GetMapping(
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<List<SecurityGoalValueDTO>> getAll() {

        List<SecurityGoalValue> list = securityGoalValueService.getAll();

        if (list.isEmpty()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        List<SecurityGoalValueDTO> dtos = new ArrayList<>();
        for (SecurityGoalValue value : list){
            dtos.add(new SecurityGoalValueDTO(value));
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping(
            value = "/{id}/security-measures",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<List<SecurityMeasureDTO>> getValueMeasures(@PathVariable Long id) {
        Optional<SecurityGoalValue> optional = securityGoalValueService.getById(id);

        if (!optional.isPresent()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        List<SecurityMeasure> list = optional.get().getSecurityMeasures();

        if (list.isEmpty()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        List<SecurityMeasureDTO> dtos = new ArrayList<>();
        for (SecurityMeasure measure: list){
            dtos.add(new SecurityMeasureDTO(measure));
        }

        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping(
            value = "/{id}/non-security-measures",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<List<SecurityMeasureDTO>> getNonValueMeasures(@PathVariable Long id) {
        Optional<SecurityGoalValue> optional = securityGoalValueService.getById(id);

        if (!optional.isPresent()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        List<SecurityMeasure> all = securityMeasureService.getAll();
        List<SecurityMeasure> list = optional.get().getSecurityMeasures();

        if (!list.isEmpty()){
            all.removeAll(list);
        }
        if (all.isEmpty()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        List<SecurityMeasureDTO> dtos = new ArrayList<>();
        for (SecurityMeasure measure: all){
            dtos.add(new SecurityMeasureDTO(measure));
        }

        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @SuppressWarnings("Duplicates")
    @PostMapping(
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<String> create(@RequestBody SecurityGoalValueDTO securityGoalValueDTO){


        Optional<SecurityGoal> securityGoal = securityGoalService.getById(securityGoalValueDTO.getSecurityGoal().getId());

        if (!securityGoal.isPresent()){
            return new ResponseEntity<>("Security goal value not created!", HttpStatus.BAD_REQUEST);
        }

        SecurityGoalValue toDb = new SecurityGoalValue();
        toDb.setName(securityGoalValueDTO.getName());
        toDb.setSecurityGoal(securityGoal.get());
        toDb.setDescription(securityGoalValueDTO.getDescription());

        if (securityGoalValueDTO.getSecurityMeasures() != null){
            for (SecurityMeasureDTO securityMeasureDTO : securityGoalValueDTO.getSecurityMeasures()) {
                Optional<SecurityMeasure> measure = securityMeasureService.getById(securityMeasureDTO.getId());
                if (measure.isPresent()){
                    toDb.getSecurityMeasures().add(measure.get());
                }
            }
        }

        securityGoalValueService.save(toDb);

        return new ResponseEntity<>("Security goal value created!", HttpStatus.OK);
    }

    @SuppressWarnings("Duplicates")
    @PutMapping(
            value = "/{id}",
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<String> update(@RequestBody SecurityGoalValueDTO securityGoalValueDTO,
                                         @PathVariable Long id){

        Optional<SecurityGoalValue> optional = securityGoalValueService.getById(id);

        if (!optional.isPresent()){
            return new ResponseEntity<>("Security goal value not updated!", HttpStatus.BAD_REQUEST);
        }

        Optional<SecurityGoal> securityGoal = securityGoalService.getById(securityGoalValueDTO.getSecurityGoal().getId());

        if (!securityGoal.isPresent()) {
            return new ResponseEntity<>("Security goal value not updated!", HttpStatus.BAD_REQUEST);
        }

        SecurityGoalValue fromDb = optional.get();

        fromDb.setName(securityGoalValueDTO.getName());
        fromDb.setDescription(securityGoalValueDTO.getDescription());
        fromDb.setSecurityGoal(securityGoal.get());

        fromDb.getSecurityMeasures().clear();
        if (securityGoalValueDTO.getSecurityMeasures() != null){
            for (SecurityMeasureDTO securityMeasureDTO : securityGoalValueDTO.getSecurityMeasures()) {
                Optional<SecurityMeasure> measure = securityMeasureService.getById(securityMeasureDTO.getId());
                if (measure.isPresent()){
                    fromDb.getSecurityMeasures().add(measure.get());
                }
            }
        }

        securityGoalValueService.save(fromDb);

        return new ResponseEntity<>("Security goal value updated!", HttpStatus.OK);
    }

    @DeleteMapping(
            value = "/{id}")
    public ResponseEntity<String> delete(@PathVariable Long id) {
        Optional<SecurityGoalValue> optional = securityGoalValueService.getById(id);

        if (!optional.isPresent()){
            return new ResponseEntity<>("Security goal value not deleted!", HttpStatus.BAD_REQUEST);
        }

        securityGoalValueService.delete(optional.get());

        return new ResponseEntity<>("Security goal value deleted!", HttpStatus.OK);
    }

    @PostMapping(
            value = "/{id}/security-measures/from-dialog",
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<String> addMeasureDirectly(@PathVariable Long id,
                                                      @RequestBody SecurityMeasureDTO securityMeasureDTO) {
        Optional<SecurityGoalValue> optional = securityGoalValueService.getById(id);

        if (!optional.isPresent()){
            return new ResponseEntity<>("Security goal value not found", HttpStatus.BAD_REQUEST);
        }

        SecurityGoalValue goalValue = optional.get();

        SecurityMeasure toDb = new SecurityMeasure();
        toDb.setName(securityMeasureDTO.getName());
        toDb.setDescription(securityMeasureDTO.getDescription());
        Optional<SecurityMeasureType> optionalSecMesType = securityMeasureTypeService.getById(securityMeasureDTO.getType().getId());

        if (!optionalSecMesType.isPresent()){
            return new ResponseEntity<String>("Security measure type not found!", HttpStatus.BAD_REQUEST);
        }
        toDb.setType(optionalSecMesType.get());
        SecurityMeasure measure = securityMeasureService.save(toDb);

        goalValue.getSecurityMeasures().add(measure);
        securityGoalValueService.save(goalValue);

        return new ResponseEntity<>("Security goal value updated!", HttpStatus.OK);
    }
}
