package rs.ac.uns.ftn.assetmanagementcybersecurity.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rs.ac.uns.ftn.assetmanagementcybersecurity.dto.AssetDTO;
import rs.ac.uns.ftn.assetmanagementcybersecurity.dto.SecurityGoalValueDTO;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.*;
import rs.ac.uns.ftn.assetmanagementcybersecurity.service.*;

import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/assets")
public class AssetController {

    @Autowired
    private AssetService assetService;

    @Autowired
    private HardwareAssetService hardwareAssetService;

    @Autowired
    private SoftwareAssetService softwareAssetService;

    @Autowired
    private VirtualAssetService virtualAssetService;

    @Autowired
    private DataAssetService dataAssetService;

    @Autowired
    private SecurityGoalValueService securityGoalValueService;

    @GetMapping(
            value = "/{uuid}",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<AssetDTO> getOne(@PathVariable String uuid) {
        Optional<Asset> optional = assetService.getByUuid(uuid);

        if (!optional.isPresent()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        AssetDTO dto = null;

        if (optional.get() instanceof HardwareAsset){
            dto = new AssetDTO(hardwareAssetService.getByUuid(uuid).get());
        } else if (optional.get() instanceof SoftwareAsset){
            dto = new AssetDTO(softwareAssetService.getByUuid(uuid).get());
        } else if (optional.get() instanceof VirtualAsset){
            dto = new AssetDTO(virtualAssetService.getByUuid(uuid).get());
        } else if (optional.get() instanceof DataAsset) {
            dto = new AssetDTO(dataAssetService.getByUuid(uuid).get());
        }

        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @GetMapping(
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<List<AssetDTO>> getAll() {

        List<Asset> list = assetService.getAll();

        if (list.isEmpty()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        List<AssetDTO> dtos = assetService.convertAssetsToDTO(list);

        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping(
            value = "/page",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<List<AssetDTO>> getAll(@RequestParam Integer pageNum,
                                                 @RequestParam Integer pageSize) {

        if (pageNum > 100){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        Pageable pageable = PageRequest.of(pageNum, pageSize);

        List<Asset> list = assetService.getAllPageable(pageable);

        if (list.isEmpty()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        List<AssetDTO> dtos = assetService.convertAssetsToDTO(list);

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("total-count", String.valueOf(assetService.getAll().size()));

        return new ResponseEntity<>(dtos, httpHeaders, HttpStatus.OK);
    }

    @GetMapping(
            value = "/{uuid}/security-goal-values",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<List<SecurityGoalValueDTO>> getAssetValues(@PathVariable String uuid) {
        Optional<Asset> optional = assetService.getByUuid(uuid);

        if (!optional.isPresent()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        List<SecurityGoalValue> list = optional.get().getSecurityGoalValues();

        if (list.isEmpty()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        List<SecurityGoalValueDTO> dtos = new ArrayList<>();
        for (SecurityGoalValue securityGoalValue: list){
            dtos.add(new SecurityGoalValueDTO(securityGoalValue));
        }

        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping(
            value = "/{uuid}/non-security-goal-values",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<List<SecurityGoalValueDTO>> getNonAssetValues(@PathVariable String uuid) {
        Optional<Asset> optional = assetService.getByUuid(uuid);

        if (!optional.isPresent()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        List<SecurityGoalValue> all = securityGoalValueService.getAll();
        List<SecurityGoalValue> list = optional.get().getSecurityGoalValues();

        if (!list.isEmpty()){
            all.removeAll(list);
        }
        if (all.isEmpty()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        List<SecurityGoalValueDTO> dtos = new ArrayList<>();
        for (SecurityGoalValue securityGoalValue: all){
            dtos.add(new SecurityGoalValueDTO(securityGoalValue));
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }
}
