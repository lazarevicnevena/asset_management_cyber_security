package rs.ac.uns.ftn.assetmanagementcybersecurity.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rs.ac.uns.ftn.assetmanagementcybersecurity.dto.OwnerDTO;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.Owner;
import rs.ac.uns.ftn.assetmanagementcybersecurity.service.OwnerService;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/owners")
public class OwnerController {

    @Autowired
    private OwnerService ownerService;

    @GetMapping(
            value = "/{id}",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<Owner> getOne(@PathVariable String id) {
        Optional<Owner> optional = ownerService.getByEmployeeId(id);

        if (!optional.isPresent()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(optional.get(), HttpStatus.OK);
    }

    @GetMapping(
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<List<Owner>> getAll() {
        List<Owner> list = ownerService.getAll();

        if (list.isEmpty()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @PostMapping(
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<String> create(@RequestBody OwnerDTO dto){

        if (ownerService.getByEmployeeId(dto.getEmployeeId()).isPresent()){
            return new ResponseEntity<>("Owner not created!", HttpStatus.BAD_REQUEST);
        }

        Owner toDb = new Owner();
        toDb.setFirstName(dto.getFirstName());
        toDb.setMiddleName(dto.getMiddleName());
        toDb.setLastName(dto.getLastName());
        toDb.setPhoneNumber(dto.getPhoneNumber());
        toDb.setEmployeeId(dto.getEmployeeId());
        ownerService.save(toDb);

        return new ResponseEntity<>("Owner created!", HttpStatus.OK);
    }

    @PutMapping(
            value = "/{id}",
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<String> update(@RequestBody Owner owner,
                                         @PathVariable String id){

        Optional<Owner> optional = ownerService.getByEmployeeId(id);

        if (!optional.isPresent()){
            return new ResponseEntity<>("Owner not updated!", HttpStatus.BAD_REQUEST);
        }

        Owner fromDb = optional.get();

        fromDb.setFirstName(owner.getFirstName());
        fromDb.setMiddleName(owner.getMiddleName());
        fromDb.setLastName(owner.getLastName());
        fromDb.setPhoneNumber(owner.getPhoneNumber());
        ownerService.save(fromDb);

        return new ResponseEntity<>("Owner updated!", HttpStatus.OK);
    }

    @DeleteMapping(
            value = "/{id}")
    public ResponseEntity<String> delete(@PathVariable String id) {
        Optional<Owner> optional = ownerService.getByEmployeeId(id);

        if (!optional.isPresent()){
            return new ResponseEntity<>("Owner not deleted!", HttpStatus.BAD_REQUEST);
        }

        ownerService.delete(optional.get());

        return new ResponseEntity<>("Owner deleted!", HttpStatus.OK);
    }
}
