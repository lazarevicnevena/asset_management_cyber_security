package rs.ac.uns.ftn.assetmanagementcybersecurity.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.Location;
import rs.ac.uns.ftn.assetmanagementcybersecurity.service.LocationService;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/locations")
public class LocationController {

    @Autowired
    private LocationService locationService;

    @GetMapping(
            value = "/{id}",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<Location> getOne(@PathVariable Long id) {
        Optional<Location> optional = locationService.getById(id);

        if (!optional.isPresent()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(optional.get(), HttpStatus.OK);
    }

    @GetMapping(
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<List<Location>> getAll() {
        List<Location> list = locationService.getAll();

        if (list.isEmpty()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @PostMapping(
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<String> create(@RequestBody Location location){

        Optional<Location> optional = locationService.getByBuildingAndRoomAndRack(
                location.getBuilding(),
                location.getRoom(),
                location.getRack());

        if (optional.isPresent()){
            return new ResponseEntity<>("Location not created!", HttpStatus.BAD_REQUEST);
        }

        Location toDb = new Location();
        toDb.setBuilding(location.getBuilding());
        toDb.setRoom(location.getRoom());
        toDb.setRack(location.getRack());

        locationService.save(toDb);

        return new ResponseEntity<>("Location created!", HttpStatus.OK);
    }

    @PutMapping(
            value = "/{id}",
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<String> update(@RequestBody Location location,
                                         @PathVariable Long id){

        Optional<Location> optional = locationService.getById(id);

        if (!optional.isPresent()){
            return new ResponseEntity<>("Location not updated!", HttpStatus.BAD_REQUEST);
        }

        Location fromDb = optional.get();

        fromDb.setBuilding(location.getBuilding());
        fromDb.setRoom(location.getRoom());
        fromDb.setRack(location.getRack());
        locationService.save(fromDb);

        return new ResponseEntity<>("Location updated!", HttpStatus.OK);
    }

    @DeleteMapping(
            value = "/{id}")
    public ResponseEntity<String> delete(@PathVariable Long id) {
        Optional<Location> optional = locationService.getById(id);

        if (!optional.isPresent()){
            return new ResponseEntity<>("Location not deleted!", HttpStatus.BAD_REQUEST);
        }

        locationService.delete(optional.get());

        return new ResponseEntity<>("Location deleted!", HttpStatus.OK);
    }
}
