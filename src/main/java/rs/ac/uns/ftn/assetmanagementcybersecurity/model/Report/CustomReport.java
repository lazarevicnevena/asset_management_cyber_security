package rs.ac.uns.ftn.assetmanagementcybersecurity.model.Report;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
public class CustomReport {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;
    private String description;
    private String subtype;
    private String type;
    private String building;
    private String room;
    private String owner;
    private String securityGoalValue;
    private String securityMeasure;

    public CustomReport(String name, String description, String subtype,
                        String type, String building, String room,
                        String owner, String securityGoalValue, String securityMeasure){
        this.name = name;
        this.description = description;
        this.subtype = subtype;
        this.type = type;
        this.building = building;
        this.room = room;
        this.owner = owner;
        this.securityGoalValue = securityGoalValue;
        this.securityMeasure = securityMeasure;
    }
}
