package rs.ac.uns.ftn.assetmanagementcybersecurity.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rs.ac.uns.ftn.assetmanagementcybersecurity.dto.SecurityGoalDTO;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.Asset;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.Location;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.SecurityGoal;
import rs.ac.uns.ftn.assetmanagementcybersecurity.repository.SecurityGoalRepository;
import rs.ac.uns.ftn.assetmanagementcybersecurity.service.AssetService;
import rs.ac.uns.ftn.assetmanagementcybersecurity.service.LocationService;
import rs.ac.uns.ftn.assetmanagementcybersecurity.service.SecurityGoalService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/security-goals")
public class SecurityGoalController {

    @Autowired
    private SecurityGoalService securityGoalService;

    @Autowired
    private AssetService assetService;

    @GetMapping(
            value = "/{id}",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<SecurityGoalDTO> getOne(@PathVariable Long id) {
        Optional<SecurityGoal> optional = securityGoalService.getById(id);

        if (!optional.isPresent()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(new SecurityGoalDTO(optional.get()), HttpStatus.OK);
    }

    @GetMapping(
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<List<SecurityGoalDTO>> getAll() {

        List<SecurityGoal> list = securityGoalService.getAll();

        if (list.isEmpty()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        List<SecurityGoalDTO> dtos = new ArrayList<>();
        for (SecurityGoal goal : list){
            dtos.add(new SecurityGoalDTO(goal));
        }

        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping(
            value = "/page",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<List<SecurityGoalDTO>> getAllByPage( @RequestParam Integer pageNum,
                                                               @RequestParam Integer pageSize) {

        if (pageNum > 100) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        Pageable pageable = PageRequest.of(pageNum, pageSize);

        List<SecurityGoal> list = securityGoalService.getAllPageable(pageable);

        if (list.isEmpty()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        List<SecurityGoalDTO> dtos = new ArrayList<>();
        for (SecurityGoal goal : list){
            dtos.add(new SecurityGoalDTO(goal));
        }

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("total-count", String.valueOf(securityGoalService.getAll().size()));

        return new ResponseEntity<>(dtos, httpHeaders, HttpStatus.OK);
    }


    @PostMapping(
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<String> create(@RequestBody SecurityGoal securityGoal){

        Optional<SecurityGoal> optional = securityGoalService.getByName(securityGoal.getName());

        if (optional.isPresent()){
            return new ResponseEntity<>("Security goal not created!", HttpStatus.BAD_REQUEST);
        }


        SecurityGoal toDb = new SecurityGoal();
        toDb.setName(securityGoal.getName());

        securityGoalService.save(toDb);

        return new ResponseEntity<>("Security goal created!", HttpStatus.OK);
    }

    @PutMapping(
            value = "/{id}",
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<String> update(@RequestBody SecurityGoal securityGoal,
                                         @PathVariable Long id){

        Optional<SecurityGoal> optional = securityGoalService.getById(id);

        if (!optional.isPresent()){
            return new ResponseEntity<>("Security goal not updated!", HttpStatus.BAD_REQUEST);
        }

        SecurityGoal fromDb = optional.get();

        fromDb.setName(securityGoal.getName());
        securityGoalService.save(fromDb);

        return new ResponseEntity<>("Security goal updated!", HttpStatus.OK);
    }

    @DeleteMapping(
            value = "/{id}")
    public ResponseEntity<String> delete(@PathVariable Long id) {
        Optional<SecurityGoal> optional = securityGoalService.getById(id);

        if (!optional.isPresent()){
            return new ResponseEntity<>("Security goal not deleted!", HttpStatus.BAD_REQUEST);
        }

        securityGoalService.delete(optional.get());

        return new ResponseEntity<>("Security goal deleted!", HttpStatus.OK);
    }
}
