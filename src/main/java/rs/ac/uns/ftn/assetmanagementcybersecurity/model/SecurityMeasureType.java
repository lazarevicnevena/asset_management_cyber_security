package rs.ac.uns.ftn.assetmanagementcybersecurity.model;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
public class SecurityMeasureType {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;
    private String description;
    private MeasureType type;

    public SecurityMeasureType( String name,
                                String description,
                                MeasureType type ){
        this.name = name;
        this.description = description;
        this.type = type;
    }

}