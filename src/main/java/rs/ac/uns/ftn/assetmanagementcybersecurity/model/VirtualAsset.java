package rs.ac.uns.ftn.assetmanagementcybersecurity.model;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
public class VirtualAsset extends Asset {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String macAddress;
    private String ipAddress;
    private LocalDateTime lastUpdated;
    private String operatingSystem;

    @ManyToOne(fetch = FetchType.LAZY)
    private HardwareAsset hardwareAsset;

    public VirtualAsset(String name,
                        String description,
                        String macAddress,
                        String ipAddress,
                        LocalDateTime created,
                        LocalDateTime lastUpdated,
                        AssetType type,
                        String state,
                        String uuid,
                        Owner owner,
                        String os,
                        HardwareAsset hardwareAsset){
        this.setName(name);
        this.setDescription(description);
        this.setMacAddress(macAddress);
        this.setIpAddress(ipAddress);
        this.setCreated(created);
        this.setLastUpdated(lastUpdated);
        this.setType(type);
        this.setState(state);
        this.setUuid(uuid);
        this.setOwner(owner);
        this.setOperatingSystem(os);
        this.hardwareAsset = hardwareAsset;
    }
}
