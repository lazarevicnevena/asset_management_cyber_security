package rs.ac.uns.ftn.assetmanagementcybersecurity.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.DataAsset;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.VirtualAsset;

import java.util.List;
import java.util.Optional;

public interface DataAssetRepository extends JpaRepository<DataAsset, Long> {
    Optional<DataAsset> getByUuid(String uuid);
    List<DataAsset> findAllByOrderByIdDesc();
    Page<DataAsset> findAll(Pageable pageable);
}
