package rs.ac.uns.ftn.assetmanagementcybersecurity.service.Report;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.*;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.Report.CustomReport;
import rs.ac.uns.ftn.assetmanagementcybersecurity.repository.Report.CustomReportRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CustomReportService {

    @Autowired
    private CustomReportRepository customReportRepository;

    public Optional<CustomReport> getById(Long id) {
        return customReportRepository.findById(id);
    }

    public List<CustomReport> getAll(){
        return customReportRepository.findAllByOrderByIdDesc();
    }

    public CustomReport save(CustomReport customReport){
        return customReportRepository.save(customReport);
    }

    public void delete(CustomReport customReport){
        customReportRepository.delete(customReport);
    }

    public List<Asset> filter(List<Asset> list, CustomReport report)
    {
        List<Asset> assets = new ArrayList<>();

        for (Asset asset: list){
            if (report.getName() != null && !report.getName().equals("") && !asset.getName().toLowerCase().contains(report.getName().toLowerCase())) {
                continue;
            }
            if (report.getDescription() != null && !report.getDescription().equals("") && !asset.getDescription().toLowerCase().contains(report.getDescription().toLowerCase())) {
                continue;
            }
            if (report.getSubtype() != null && !report.getSubtype().equals("ANY")) {
                if ((report.getSubtype().equals("HARDWARE") && !(asset instanceof HardwareAsset)) ||
                        (report.getSubtype().equals("SOFTWARE") && !(asset instanceof SoftwareAsset)) ||
                        (report.getSubtype().equals("VIRTUAL") && !(asset instanceof VirtualAsset)) ||
                        (report.getSubtype().equals("DATA") && !(asset instanceof DataAsset))){
                    continue;
                }
            }
            if (report.getType() != null &&!report.getType().equals("ANY") && !asset.getType().equals(AssetType.valueOf(report.getType()))) {
                continue;
            }
            if (report.getBuilding() != null &&!report.getBuilding().equals("") && asset instanceof HardwareAsset &&
                    !(((HardwareAsset) asset).getLocation().getBuilding().toLowerCase().equals(report.getBuilding().toLowerCase()))) {
                continue;
            }
            if (report.getRoom() != null &&!report.getRoom().equals("") && asset instanceof HardwareAsset &&
                    !(((HardwareAsset) asset).getLocation().getRoom().toLowerCase().equals(report.getRoom().toLowerCase()))) {
                continue;
            }
            if (report.getOwner() != null &&!report.getOwner().equals("") &&
                    !asset.getOwner().getFirstName().toLowerCase().contains(report.getOwner().toLowerCase()) &&
                    !asset.getOwner().getLastName().toLowerCase().contains(report.getOwner().toLowerCase()) &&
                    !asset.getOwner().getMiddleName().toLowerCase().contains(report.getOwner().toLowerCase())) {
                continue;
            }
            boolean secValueFlag = false;
            if (report.getSecurityGoalValue() != null &&!report.getSecurityGoalValue().equals("") && !asset.getSecurityGoalValues().isEmpty()){
                for (SecurityGoalValue value : asset.getSecurityGoalValues()){
                    if (value.getName().toLowerCase().contains(report.getSecurityGoalValue().toLowerCase())){
                        secValueFlag = true;
                        break;
                    }
                }
                if (!secValueFlag){
                    continue;
                }
            }
            boolean secMeasureFlag = false;
            if (report.getSecurityMeasure() != null &&!report.getSecurityMeasure().equals("") && !asset.getSecurityGoalValues().isEmpty()){
                for (SecurityGoalValue value : asset.getSecurityGoalValues()){
                    if (!value.getSecurityMeasures().isEmpty()){
                        for (SecurityMeasure securityMeasure: value.getSecurityMeasures()){
                            if (securityMeasure.getName().toLowerCase().contains(report.getSecurityMeasure().toLowerCase())){
                                secMeasureFlag = true;
                                break;
                            }
                        }
                    }
                }
                if (!secMeasureFlag){
                    continue;
                }
            }
            assets.add(asset);
        }

        return  assets;
    }

}
