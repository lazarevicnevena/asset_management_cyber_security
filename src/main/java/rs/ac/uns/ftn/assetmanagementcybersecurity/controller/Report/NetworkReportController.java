package rs.ac.uns.ftn.assetmanagementcybersecurity.controller.Report;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rs.ac.uns.ftn.assetmanagementcybersecurity.dto.AssetDTO;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.Asset;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.AssetNetwork;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.Report.NetworkReport;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.Report.SecurityReport;
import rs.ac.uns.ftn.assetmanagementcybersecurity.service.AssetNetworkService;
import rs.ac.uns.ftn.assetmanagementcybersecurity.service.AssetService;
import rs.ac.uns.ftn.assetmanagementcybersecurity.service.Report.NetworkReportService;
import rs.ac.uns.ftn.assetmanagementcybersecurity.service.Report.SecurityReportService;
import sun.nio.ch.Net;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/network-reports")
public class NetworkReportController {

    @Autowired
    private AssetService assetService;

    @Autowired
    private NetworkReportService networkReportService;

    @Autowired
    private AssetNetworkService assetNetworkService;

    @SuppressWarnings("Duplicates")
    @GetMapping(
            value = "/{id}/filter",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<List<AssetDTO>> filter(@PathVariable Long id) {

        Optional<NetworkReport> optional = networkReportService.getById(id);

        List<AssetNetwork> list = assetNetworkService.getAll();

        if (list.isEmpty() || !optional.isPresent()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        List<Asset> finalList = networkReportService.filter(list, optional.get());

        if (finalList.isEmpty()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        List<AssetDTO> dtos = assetService.convertAssetsToDTO(finalList);

        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping(
            value = "/{id}",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<NetworkReport> getOne(@PathVariable Long id) {
        Optional<NetworkReport> optional = networkReportService.getById(id);

        if (!optional.isPresent()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(optional.get(), HttpStatus.OK);
    }

    @GetMapping(
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<List<NetworkReport>> getAll() {
        List<NetworkReport> list = networkReportService.getAll();

        if (list.isEmpty()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @PostMapping(
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<String> create(@RequestBody NetworkReport dto){

        if (networkReportService.getById(dto.getId()).isPresent()){
            return new ResponseEntity<>("Network report not created!", HttpStatus.BAD_REQUEST);
        }

        NetworkReport toDb = new NetworkReport();
        toDb.setName(dto.getName());
        toDb.setDescription(dto.getDescription());
        toDb.setMinAssetNr(dto.getMinAssetNr());
        toDb.setMaxAssetNr(dto.getMaxAssetNr());
        networkReportService.save(toDb);

        return new ResponseEntity<>("Network report created!", HttpStatus.OK);
    }

    @PutMapping(
            value = "/{id}",
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<String> update(@RequestBody NetworkReport dto,
                                         @PathVariable Long id){

        Optional<NetworkReport> optional = networkReportService.getById(id);

        if (!optional.isPresent()){
            return new ResponseEntity<>("Network report not updated!", HttpStatus.BAD_REQUEST);
        }

        NetworkReport fromDb = optional.get();

        fromDb.setName(dto.getName());

        fromDb.setDescription(dto.getDescription());
        fromDb.setMinAssetNr(dto.getMinAssetNr());
        fromDb.setMaxAssetNr(dto.getMaxAssetNr());
        networkReportService.save(fromDb);

        return new ResponseEntity<>("Network report updated!", HttpStatus.OK);
    }

    @DeleteMapping(
            value = "/{id}")
    public ResponseEntity<String> delete(@PathVariable Long id) {
        Optional<NetworkReport> optional = networkReportService.getById(id);

        if (!optional.isPresent()){
            return new ResponseEntity<>("Network report not deleted!", HttpStatus.BAD_REQUEST);
        }

        networkReportService.delete(optional.get());

        return new ResponseEntity<>("Network report deleted!", HttpStatus.OK);
    }

}
