package rs.ac.uns.ftn.assetmanagementcybersecurity.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.Location;

import java.util.List;
import java.util.Optional;

public interface LocationRepository extends JpaRepository<Location, Long> {
    List<Location> findByBuildingAndRoom(String building, String room);
    List<Location> findAllByOrderByIdDesc();
    Optional<Location> findByBuildingAndRoomAndRack(String building, String room, Integer rack);
}
