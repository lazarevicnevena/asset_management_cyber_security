package rs.ac.uns.ftn.assetmanagementcybersecurity.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.Location;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.SecurityGoal;
import rs.ac.uns.ftn.assetmanagementcybersecurity.repository.LocationRepository;
import rs.ac.uns.ftn.assetmanagementcybersecurity.repository.SecurityGoalRepository;

import java.util.List;
import java.util.Optional;

@Service
public class SecurityGoalService {

    @Autowired
    private SecurityGoalRepository securityGoalRepository;

    public Optional<SecurityGoal> getById(Long id) {
        return securityGoalRepository.findById(id);
    }

    public List<SecurityGoal> getAll(){
        return securityGoalRepository.findAllByOrderByIdDesc();
    }

    public List<SecurityGoal> getAllPageable(Pageable pageable){
        return securityGoalRepository.findAll(pageable).getContent();
    }

    public List<SecurityGoal> getAllPageableByName(Pageable pageable, String name){
        return securityGoalRepository.findAllByName(pageable, name).getContent();
    }

    public Optional<SecurityGoal> getByName(String name){
        return securityGoalRepository.getByName(name);
    }

    public SecurityGoal save(SecurityGoal securityGoal){
        return securityGoalRepository.save(securityGoal);
    }

    public void delete(SecurityGoal securityGoal){
        securityGoalRepository.delete(securityGoal);
    }

}
