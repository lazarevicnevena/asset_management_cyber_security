package rs.ac.uns.ftn.assetmanagementcybersecurity.model.Report;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
public class SecurityReport {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;
    private String securityGoalValues;
    private String securityMeasures;

    public SecurityReport(String name, String securityGoalValues, String securityMeasures){
        this.name = name;
        this.securityGoalValues = securityGoalValues;
        this.securityMeasures = securityMeasures;
    }
}
