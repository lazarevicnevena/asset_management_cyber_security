package rs.ac.uns.ftn.assetmanagementcybersecurity.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rs.ac.uns.ftn.assetmanagementcybersecurity.dto.SecurityMeasureDTO;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.SecurityGoalValue;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.SecurityMeasure;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.SecurityMeasureType;
import rs.ac.uns.ftn.assetmanagementcybersecurity.service.SecurityGoalValueService;
import rs.ac.uns.ftn.assetmanagementcybersecurity.service.SecurityMeasureService;
import rs.ac.uns.ftn.assetmanagementcybersecurity.service.SecurityMeasureTypeService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/security-measure-types")
public class SecurityMeasureTypeController {

    @Autowired
    private SecurityMeasureTypeService securityMeasureTypeService;

    @Autowired
    private SecurityMeasureService securityMeasureService;

    @GetMapping(
            value = "/{id}",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<SecurityMeasureType> getOne(@PathVariable Long id) {
        Optional<SecurityMeasureType> optional = securityMeasureTypeService.getById(id);

        if (!optional.isPresent()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(optional.get(), HttpStatus.OK);
    }

    @GetMapping(
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<List<SecurityMeasureType>> getAllByPage() {

        List<SecurityMeasureType> list = securityMeasureTypeService.getAll();

        if (list.isEmpty()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @PostMapping(
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<String> create(@RequestBody SecurityMeasureType securityMeasureType){


        SecurityMeasureType toDb = new SecurityMeasureType();

        toDb.setName(securityMeasureType.getName());
        toDb.setDescription(securityMeasureType.getDescription());
        toDb.setType(securityMeasureType.getType());
        securityMeasureTypeService.save(toDb);

        return new ResponseEntity<>("Security measure type created!", HttpStatus.OK);
    }

    @PutMapping(
            value = "/{id}",
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<String> update(@RequestBody SecurityMeasureType securityMeasureType,
                                         @PathVariable Long id){

        Optional<SecurityMeasureType> optional = securityMeasureTypeService.getById(id);

        if (!optional.isPresent()){
            return new ResponseEntity<>("Security measure type not updated!", HttpStatus.BAD_REQUEST);
        }

        SecurityMeasureType fromDb = optional.get();

        fromDb.setName(securityMeasureType.getName());
        fromDb.setDescription(securityMeasureType.getDescription());
        fromDb.setType(securityMeasureType.getType());
        securityMeasureTypeService.save(fromDb);

        return new ResponseEntity<>("Security measure type updated!", HttpStatus.OK);
    }

    @DeleteMapping(
            value = "/{id}")
    public ResponseEntity<String> delete(@PathVariable Long id) {
        Optional<SecurityMeasureType> optional = securityMeasureTypeService.getById(id);

        if (!optional.isPresent()){
            return new ResponseEntity<>("Security measure type not deleted!", HttpStatus.BAD_REQUEST);
        }

        securityMeasureTypeService.delete(optional.get());

        return new ResponseEntity<>("Security measure type deleted!", HttpStatus.OK);
    }
}
