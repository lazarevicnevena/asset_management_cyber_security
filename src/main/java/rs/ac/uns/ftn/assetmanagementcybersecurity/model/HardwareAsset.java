package rs.ac.uns.ftn.assetmanagementcybersecurity.model;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
public class HardwareAsset extends Asset {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String hostName;
    private String macAddress;
    private String ipAddress;
    private LocalDateTime lastUpdated;
    private String serialNumber;
    private String model;
    private String operatingSystem;

    @ManyToOne(fetch = FetchType.EAGER)
    private Location location;

    public HardwareAsset(String name, String description,
                         String hostName, String macAddress,
                         String ipAddress, LocalDateTime created,
                         LocalDateTime lastUpdated, AssetType type,
                         String state, String uuid, Owner owner,
                         String serialNumber, String model,
                         String os, Location location){
        this.setName(name);
        this.setDescription(description);
        this.setHostName(hostName);
        this.setMacAddress(macAddress);
        this.setIpAddress(ipAddress);
        this.setCreated(created);
        this.setLastUpdated(lastUpdated);
        this.setType(type);
        this.setState(state);
        this.setUuid(uuid);
        this.setOwner(owner);
        this.setSerialNumber(serialNumber);
        this.setModel(model);
        this.setOperatingSystem(os);
        this.setLocation(location);
    }
}
