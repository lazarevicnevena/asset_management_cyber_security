package rs.ac.uns.ftn.assetmanagementcybersecurity.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.HardwareAsset;
import rs.ac.uns.ftn.assetmanagementcybersecurity.repository.HardwareAssetRepository;

import java.util.List;
import java.util.Optional;

@Service
public class HardwareAssetService {

    @Autowired
    private HardwareAssetRepository hardwareAssetRepository;

    public Optional<HardwareAsset> getByUuid(String uuid) {
        return hardwareAssetRepository.getByUuid(uuid);
    }

    public List<HardwareAsset> getAll(){
        return hardwareAssetRepository.findAllByOrderByIdDesc();
    }

    public List<HardwareAsset> getAllPageable(Pageable pageable){
        return hardwareAssetRepository.findAll(pageable).getContent();
    }

    public HardwareAsset save(HardwareAsset asset){
        return hardwareAssetRepository.save(asset);
    }

    public void delete(HardwareAsset asset){
        hardwareAssetRepository.delete(asset);
    }
}
