package rs.ac.uns.ftn.assetmanagementcybersecurity.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.AssetNetwork;
import rs.ac.uns.ftn.assetmanagementcybersecurity.repository.AssetNetworkRepository;

import java.util.List;
import java.util.Optional;

@Service
public class AssetNetworkService {

    @Autowired
    private AssetNetworkRepository assetNetworkRepository;

    public Optional<AssetNetwork> getById(Long id) {
        return assetNetworkRepository.findById(id);
    }

    public Optional<AssetNetwork> getByName(String name) {
        return assetNetworkRepository.getByName(name);
    }

    public List<AssetNetwork> getAll(){
        return assetNetworkRepository.findAllByOrderByIdDesc();
    }

    public List<AssetNetwork> getAllPageable(Pageable pageable){
        Page<AssetNetwork> page = assetNetworkRepository.findAll(pageable);
        return page.getContent();
    }

    public AssetNetwork save(AssetNetwork network){
        return assetNetworkRepository.save(network);
    }

    public void delete(AssetNetwork network){
        assetNetworkRepository.delete(network);
    }
}
