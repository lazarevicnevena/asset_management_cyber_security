package rs.ac.uns.ftn.assetmanagementcybersecurity.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.Location;
import rs.ac.uns.ftn.assetmanagementcybersecurity.repository.LocationRepository;

import java.util.List;
import java.util.Optional;

@Service
public class LocationService {

    @Autowired
    private LocationRepository locationRepository;

    public Optional<Location> getById(Long id) {
        return locationRepository.findById(id);
    }

    public List<Location> getAll(){
        return locationRepository.findAllByOrderByIdDesc();
    }

    public List<Location> getAllByBuildingAndRoom(String building, String room){
        return locationRepository.findByBuildingAndRoom(building, room);
    }

    public Optional<Location> getByBuildingAndRoomAndRack(String building, String room, Integer rack) {
        return locationRepository.findByBuildingAndRoomAndRack(building, room, rack);
    }

    public Location save(Location location){
        return locationRepository.save(location);
    }

    public void delete(Location location){
        locationRepository.delete(location);
    }


}
