package rs.ac.uns.ftn.assetmanagementcybersecurity.model;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
public class SoftwareAsset extends Asset {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToMany(fetch = FetchType.LAZY)
    private List<HardwareAsset> hardwareAssets = new ArrayList<>();

    public SoftwareAsset(String name,
                         String description,
                         LocalDateTime created,
                         AssetType type,
                         String state,
                         String uuid,
                         Owner owner){
        this.setName(name);
        this.setDescription(description);
        this.setCreated(created);
        this.setType(type);
        this.setState(state);
        this.setUuid(uuid);
        this.setOwner(owner);
    }
}
