package rs.ac.uns.ftn.assetmanagementcybersecurity.service.Report;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.*;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.Report.CustomReport;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.Report.SecurityReport;
import rs.ac.uns.ftn.assetmanagementcybersecurity.repository.Report.CustomReportRepository;
import rs.ac.uns.ftn.assetmanagementcybersecurity.repository.Report.SecurityReportRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class SecurityReportService {

    @Autowired
    private SecurityReportRepository securityReportRepository;

    public Optional<SecurityReport> getById(Long id) {
        return securityReportRepository.findById(id);
    }

    public List<SecurityReport> getAll(){
        return securityReportRepository.findAllByOrderByIdDesc();
    }

    public SecurityReport save(SecurityReport securityReport){
        return securityReportRepository.save(securityReport);
    }

    public void delete(SecurityReport securityReport){
        securityReportRepository.delete(securityReport);
    }


    public List<Asset> filter(List<Asset> list, SecurityReport report)
    {
        List<Asset> assets = new ArrayList<>();

        for (Asset asset: list){
            if (report.getName() != null && !report.getName().equals("") && !asset.getName().toLowerCase().contains(report.getName().toLowerCase())) {
                continue;
            }

            boolean secValueFlag = false;
            if (report.getSecurityGoalValues() != null && !report.getSecurityGoalValues().equals("") && !asset.getSecurityGoalValues().isEmpty()){
                String[] orValues = report.getSecurityGoalValues().split("||");
                String[] andValues = report.getSecurityGoalValues().split("&&");
                for (SecurityGoalValue value : asset.getSecurityGoalValues()){
                    if (orValues.length == 0 && andValues.length == 0){
                        if (value.getName().toLowerCase().contains(report.getSecurityGoalValues().toLowerCase())){
                            secValueFlag = true;
                            break;
                        }
                    } else {
                        if (orValues.length > 0) {
                            for (String v: orValues){
                                if (value.getName().toLowerCase().contains(v)){
                                    secValueFlag = true;
                                    break;
                                }
                            }
                        }
                        if (andValues.length > 0){
                            for (String v: andValues){
                                if (!value.getName().toLowerCase().contains(v)){
                                    secValueFlag = false;
                                    break;
                                }
                            }
                        }
                    }
                }
                if (!secValueFlag){
                    continue;
                }
            }
            boolean secMeasureFlag = false;
            if (report.getSecurityMeasures() != null && !report.getSecurityMeasures().equals("") && !asset.getSecurityGoalValues().isEmpty()){
                String[] orValues = report.getSecurityMeasures().split("||");
                String[] andValues = report.getSecurityMeasures().split("&&");
                for (SecurityGoalValue value : asset.getSecurityGoalValues()){
                    if (!value.getSecurityMeasures().isEmpty()){
                        for (SecurityMeasure securityMeasure: value.getSecurityMeasures()){
                            if (orValues.length == 0 && andValues.length == 0) {
                                if (securityMeasure.getName().toLowerCase().contains(report.getSecurityMeasures().toLowerCase())) {
                                    secMeasureFlag = true;
                                    break;
                                }
                            } else {
                                if (orValues.length > 0) {
                                    for (String v: orValues){
                                        if (value.getName().toLowerCase().contains(v)){
                                            secMeasureFlag = true;
                                            break;
                                        }
                                    }
                                }
                                if (andValues.length > 0){
                                    for (String v: andValues){
                                        if (!value.getName().toLowerCase().contains(v)){
                                            secMeasureFlag = false;
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                if (!secMeasureFlag){
                    continue;
                }
            }
            assets.add(asset);
        }

        return  assets;
    }

}
