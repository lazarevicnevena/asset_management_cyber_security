package rs.ac.uns.ftn.assetmanagementcybersecurity.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.Owner;

import java.util.List;
import java.util.Optional;

public interface OwnerRepository extends JpaRepository<Owner, Long> {
    List<Owner> findAllByOrderByIdDesc();
    Optional<Owner> findByEmployeeId(String employeeId);
}
