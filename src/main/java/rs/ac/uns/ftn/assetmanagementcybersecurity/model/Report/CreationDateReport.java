package rs.ac.uns.ftn.assetmanagementcybersecurity.model.Report;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
public class CreationDateReport {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;
    private String subtype;
    private String type;
    private LocalDateTime createdFrom;
    private LocalDateTime createdTo;

    public CreationDateReport(String name, String subtype, String type,
                              LocalDateTime createdFrom, LocalDateTime createdTo){
        this.name = name;
        this.subtype = subtype;
        this.type = type;
        this.createdFrom = createdFrom;
        this.createdTo = createdTo;
    }
}
