package rs.ac.uns.ftn.assetmanagementcybersecurity.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.SoftwareAsset;
import rs.ac.uns.ftn.assetmanagementcybersecurity.repository.SoftwareAssetRepository;

import java.util.List;
import java.util.Optional;

@Service
public class SoftwareAssetService {

    @Autowired
    private SoftwareAssetRepository softwareAssetRepository;

    public Optional<SoftwareAsset> getByUuid(String uuid) {
        return softwareAssetRepository.getByUuid(uuid);
    }

    public List<SoftwareAsset> getAll(){
        return softwareAssetRepository.findAllByOrderByIdDesc();
    }

    public List<SoftwareAsset> getAllPageable(Pageable pageable){
        return softwareAssetRepository.findAll(pageable).getContent();
    }

    public SoftwareAsset save(SoftwareAsset asset){
        return softwareAssetRepository.save(asset);
    }

    public void delete(SoftwareAsset asset){
        softwareAssetRepository.delete(asset);
    }
}
