package rs.ac.uns.ftn.assetmanagementcybersecurity.repository.Report;

import org.springframework.data.jpa.repository.JpaRepository;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.Report.CustomReport;

import java.util.List;


public interface CustomReportRepository extends JpaRepository<CustomReport, Long> {
    List<CustomReport> findAllByOrderByIdDesc();
}
