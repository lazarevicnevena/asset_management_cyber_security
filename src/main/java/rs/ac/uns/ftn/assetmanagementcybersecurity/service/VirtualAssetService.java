package rs.ac.uns.ftn.assetmanagementcybersecurity.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.VirtualAsset;
import rs.ac.uns.ftn.assetmanagementcybersecurity.repository.VirtualAssetRepository;

import java.util.List;
import java.util.Optional;

@Service
public class VirtualAssetService {

    @Autowired
    private VirtualAssetRepository virtualAssetRepository;

    public Optional<VirtualAsset> getByUuid(String uuid) {
        return virtualAssetRepository.getByUuid(uuid);
    }

    public List<VirtualAsset> getAll(){
        return virtualAssetRepository.findAllByOrderByIdDesc();
    }

    public List<VirtualAsset> getAllPageable(Pageable pageable){
        return virtualAssetRepository.findAll(pageable).getContent();
    }

    public VirtualAsset save(VirtualAsset asset){
        return virtualAssetRepository.save(asset);
    }

    public void delete(VirtualAsset asset){
        virtualAssetRepository.delete(asset);
    }
}
