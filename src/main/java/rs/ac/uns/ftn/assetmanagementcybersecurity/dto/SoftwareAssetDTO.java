package rs.ac.uns.ftn.assetmanagementcybersecurity.dto;

import lombok.*;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.AssetType;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.HardwareAsset;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.SecurityGoalValue;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.SoftwareAsset;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class SoftwareAssetDTO {

    private String name;
    private String description;
    private LocalDateTime created;
    private AssetType type;
    private String state;
    private String uuid;
    private OwnerDTO owner;
    private List<HardwareAssetDTO> hardwareAssets = new ArrayList<>();
    private List<SecurityGoalValueDTO> securityGoalValues = new ArrayList<>();

    public SoftwareAssetDTO(SoftwareAsset softwareAsset){
        this.name = softwareAsset.getName();
        this.description = softwareAsset.getDescription();
        this.created = softwareAsset.getCreated();
        this.type = softwareAsset.getType();
        this.state = softwareAsset.getState();
        this.uuid = softwareAsset.getUuid();
        this.owner = new OwnerDTO(softwareAsset.getOwner());
        for (HardwareAsset hardwareAsset: softwareAsset.getHardwareAssets()){
            this.hardwareAssets.add(new HardwareAssetDTO(hardwareAsset));
        }
        for (SecurityGoalValue value: softwareAsset.getSecurityGoalValues()){
            this.securityGoalValues.add(new SecurityGoalValueDTO(value));
        }
    }
}
