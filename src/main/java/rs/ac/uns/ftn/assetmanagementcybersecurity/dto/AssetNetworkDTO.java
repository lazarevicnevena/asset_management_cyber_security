package rs.ac.uns.ftn.assetmanagementcybersecurity.dto;

import lombok.*;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.*;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class AssetNetworkDTO {

    private Long id;
    private String name;
    private String description;

    private List<AssetDTO> assets = new ArrayList<>();

    public AssetNetworkDTO(AssetNetwork computerNetwork){
        this.id = computerNetwork.getId();
        this.name = computerNetwork.getName();
        this.description = computerNetwork.getDescription();
        for (Asset asset: computerNetwork.getAssets()){
            if (asset instanceof HardwareAsset){
                this.assets.add(new AssetDTO((HardwareAsset)asset));
            } else if (asset instanceof VirtualAsset){
                this.assets.add(new AssetDTO((VirtualAsset) asset));
            } else if (asset instanceof SoftwareAsset){
                this.assets.add(new AssetDTO((SoftwareAsset) asset));
            } else if (asset instanceof DataAsset){
                this.assets.add(new AssetDTO((DataAsset) asset));
            }
        }
    }

}
