package rs.ac.uns.ftn.assetmanagementcybersecurity;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.*;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.Report.CreationDateReport;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.Report.CustomReport;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.Report.SecurityReport;
import rs.ac.uns.ftn.assetmanagementcybersecurity.service.*;
import rs.ac.uns.ftn.assetmanagementcybersecurity.service.Report.CreationDateReportService;
import rs.ac.uns.ftn.assetmanagementcybersecurity.service.Report.CustomReportService;
import rs.ac.uns.ftn.assetmanagementcybersecurity.service.Report.SecurityReportService;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.UUID;

@Component
public class Initializer implements ApplicationRunner {

    @Autowired
    private AssetNetworkService assetNetworkService;

    @Autowired
    private HardwareAssetService hardwareAssetService;

    @Autowired
    private SoftwareAssetService softwareAssetService;

    @Autowired
    private VirtualAssetService virtualAssetService;

    @Autowired
    private DataAssetService dataAssetService;

    @Autowired
    private OwnerService ownerService;

    @Autowired
    private LocationService locationService;

    @Autowired
    private SecurityGoalService securityGoalService;

    @Autowired
    private SecurityGoalValueService securityGoalValueService;

    @Autowired
    private SecurityMeasureService securityMeasureService;

    @Autowired
    private SecurityMeasureTypeService securityMeasureTypeService;

    @Autowired
    private CustomReportService customReportService;

    @Autowired
    private CreationDateReportService creationDateReportService;

    @Autowired
    private SecurityReportService securityReportService;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        // this.initializeDb();
        System.out.println("Database initialized.");
    }

    public void initializeDb(){
        Owner owner = new Owner("Milan","Miki", "Malic", "004152635248", "1854236958");
        owner = ownerService.save(owner);

        Owner owner2 = new Owner("Ana", "", "Rudic", "00415962225", "4545232589");
        owner2 = ownerService.save(owner2);

        Location location = new Location("A1B", "102-S", 15);
        location = locationService.save(location);

        Location location2 = new Location("B1B", "JS-101", -1);
        location2 = locationService.save(location2);

        HardwareAsset hardwareAsset = new HardwareAsset(
                "Asus Laptop",
                "Personal computer",
                "DESKTOP-O3PEF57",
                "00-FF-6B-80-5B-E4",
                "192.168.0.11",
                LocalDateTime.now(),
                null,
                AssetType.LAPTOP,
                "created",
                UUID.randomUUID().toString().replaceAll("-",""),
                owner,
                "G9N0CX17K474384",
                "ASUS X540LJ",
                "Windows 10",
                location);

        hardwareAsset = hardwareAssetService.save(hardwareAsset);

        HardwareAsset hardwareAsset2 = new HardwareAsset(
                "Lenovo Laptop",
                "Personal computer",
                "DESKTOP-O3PSS31",
                "F0-03-8C-00-8C-DB",
                "192.168.0.15",
                LocalDateTime.now(),
                null,
                AssetType.LAPTOP,
                "created",
                UUID.randomUUID().toString().replaceAll("-",""),
                owner2,
                "G5K1CX17K474852",
                "Lenovo 2320KJ",
                "Windows 8",
                location2);

        hardwareAsset2 = hardwareAssetService.save(hardwareAsset2);

        VirtualAsset virtualAsset = new VirtualAsset(
                "VM 1",
                "Virtual Machine",
                "70-8B-CD-8E-C8-21",
                "192.168.0.88",
                LocalDateTime.now(),
                null,
                AssetType.VIRTUAL_MACHINE,
                "created",
                UUID.randomUUID().toString().replaceAll("-",""),
                owner2,
                "Windows 8",
                hardwareAsset2
        );

        virtualAsset = virtualAssetService.save(virtualAsset);

        DataAsset dataAsset = new DataAsset(
                "Database file",
                "SQL file for entities",
                LocalDateTime.now(),
                AssetType.FILE,
                "created",
                UUID.randomUUID().toString().replaceAll("-",""),
                owner
        );

        dataAssetService.save(dataAsset);

        SoftwareAsset softwareAsset = new SoftwareAsset(
                "Application Game One",
                "Java app",
                LocalDateTime.now(),
                AssetType.APPLICATION,
                "created",
                UUID.randomUUID().toString().replaceAll("-",""),
                owner2
        );

        softwareAsset.setHardwareAssets(Arrays.asList(hardwareAsset));

        softwareAsset = softwareAssetService.save(softwareAsset);


        SecurityGoal securityGoal = new SecurityGoal("Integrity");
        securityGoal = securityGoalService.save(securityGoal);

        SecurityGoalValue securityGoalValue = new SecurityGoalValue(
                "Integrity of user",
                "description",
                securityGoal);
        securityGoalValue = securityGoalValueService.save(securityGoalValue);

        SecurityMeasureType securityMeasureType = new SecurityMeasureType(
                "Criptography stuff",
                "description",
                MeasureType.CRYPTOGRAPHY
        );

        securityMeasureType = securityMeasureTypeService.save(securityMeasureType);

        SecurityMeasure securityMeasure = new SecurityMeasure(
                "Digital signature",
                "Digital signature of file",
                securityMeasureType);

        securityMeasure = securityMeasureService.save(securityMeasure);

        securityGoalValue.getSecurityMeasures().add(securityMeasure);
        securityGoalValue = securityGoalValueService.save(securityGoalValue);

        softwareAsset.getSecurityGoalValues().add(securityGoalValue);
        softwareAsset = softwareAssetService.save(softwareAsset);

        AssetNetwork assetNetwork = new AssetNetwork();
        assetNetwork.setName("Prva mreza");
        assetNetwork.setDescription("Opis neki 123");
        assetNetwork.setAssets(Arrays.asList(hardwareAsset, softwareAsset));
        assetNetworkService.save(assetNetwork);

        CustomReport customReport = new CustomReport(
                "o",
                "o",
                "ANY",
                "LAPTOP",
                "",
                "",
                "",
                "integrity",
                ""
        );

        customReportService.save(customReport);

        SecurityReport securityReport = new SecurityReport(
                "o",
                "integrity",
                "signature"
        );

        securityReportService.save(securityReport);

        LocalDateTime today = LocalDateTime.now();
        CreationDateReport creationDateReport = new CreationDateReport(
                "o",
                "ANY",
                "ANY",
                today.minusDays(2),
                today.plusDays(3)
        );

        creationDateReportService.save(creationDateReport);

    }
}
