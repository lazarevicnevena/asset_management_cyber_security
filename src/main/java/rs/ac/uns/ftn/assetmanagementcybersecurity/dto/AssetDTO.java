package rs.ac.uns.ftn.assetmanagementcybersecurity.dto;

import lombok.*;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class AssetDTO {

    private String name;
    private String description;
    private String hostName;
    private String macAddress;
    private String ipAddress;
    private LocalDateTime created;
    private LocalDateTime lastUpdated;
    private String state;
    private String uuid;
    private OwnerDTO owner;
    private String operatingSystem;
    private AssetType type;
    private String dtype;
    private List<SecurityGoalValueDTO> securityGoalValues = new ArrayList<>();

    public AssetDTO(HardwareAsset hardwareAsset){
        this.name = hardwareAsset.getName();
        this.description = hardwareAsset.getDescription();
        this.hostName = hardwareAsset.getHostName();
        this.macAddress = hardwareAsset.getMacAddress();
        this.ipAddress = hardwareAsset.getIpAddress();
        this.created = hardwareAsset.getCreated();
        this.lastUpdated = hardwareAsset.getLastUpdated();
        this.state = hardwareAsset.getState();
        this.uuid = hardwareAsset.getUuid();
        this.owner = new OwnerDTO(hardwareAsset.getOwner());
        this.operatingSystem = hardwareAsset.getOperatingSystem();
        this.type = hardwareAsset.getType();
        this.dtype = "Hardware asset";
        for (SecurityGoalValue value: hardwareAsset.getSecurityGoalValues()){
            this.securityGoalValues.add(new SecurityGoalValueDTO(value));
        }
    }

    public AssetDTO(SoftwareAsset softwareAsset){
        this.name = softwareAsset.getName();
        this.description = softwareAsset.getDescription();
        this.hostName = "";
        this.macAddress = "";
        this.ipAddress = "";
        this.created = softwareAsset.getCreated();
        this.lastUpdated = null;
        this.state = softwareAsset.getState();
        this.uuid = softwareAsset.getUuid();
        this.owner = new OwnerDTO(softwareAsset.getOwner());
        this.operatingSystem = "";
        this.type = softwareAsset.getType();
        this.dtype = "Software asset";
        for (SecurityGoalValue value: softwareAsset.getSecurityGoalValues()){
            this.securityGoalValues.add(new SecurityGoalValueDTO(value));
        }
    }

    public AssetDTO(VirtualAsset virtualAsset){
        this.name = virtualAsset.getName();
        this.description = virtualAsset.getDescription();
        this.macAddress = virtualAsset.getMacAddress();
        this.ipAddress = virtualAsset.getIpAddress();
        this.created = virtualAsset.getCreated();
        this.lastUpdated = virtualAsset.getLastUpdated();
        this.state = virtualAsset.getState();
        this.uuid = virtualAsset.getUuid();
        this.owner = new OwnerDTO(virtualAsset.getOwner());
        this.operatingSystem = virtualAsset.getOperatingSystem();
        this.type = virtualAsset.getType();
        this.dtype = "Virtual asset";
        for (SecurityGoalValue value: virtualAsset.getSecurityGoalValues()){
            this.securityGoalValues.add(new SecurityGoalValueDTO(value));
        }
    }

    public AssetDTO(DataAsset dataAsset){
        this.name = dataAsset.getName();
        this.description = dataAsset.getDescription();
        this.created = dataAsset.getCreated();
        this.state = dataAsset.getState();
        this.uuid = dataAsset.getUuid();
        this.owner = new OwnerDTO(dataAsset.getOwner());
        this.type = dataAsset.getType();
        this.dtype = "Data asset";
        for (SecurityGoalValue value: dataAsset.getSecurityGoalValues()){
            this.securityGoalValues.add(new SecurityGoalValueDTO(value));
        }
    }

}
