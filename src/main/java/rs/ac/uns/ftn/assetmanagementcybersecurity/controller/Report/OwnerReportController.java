package rs.ac.uns.ftn.assetmanagementcybersecurity.controller.Report;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rs.ac.uns.ftn.assetmanagementcybersecurity.dto.AssetDTO;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.Asset;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.Owner;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.Report.OwnerReport;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.Report.SecurityReport;
import rs.ac.uns.ftn.assetmanagementcybersecurity.service.AssetService;
import rs.ac.uns.ftn.assetmanagementcybersecurity.service.Report.OwnerReportService;
import rs.ac.uns.ftn.assetmanagementcybersecurity.service.Report.SecurityReportService;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/owner-reports")
public class OwnerReportController {

    @Autowired
    private AssetService assetService;

    @Autowired
    private OwnerReportService ownerReportService;

    @SuppressWarnings("Duplicates")
    @GetMapping(
            value = "/{id}/filter",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<List<AssetDTO>> securityReport(@PathVariable Long id) {

        Optional<OwnerReport> optional = ownerReportService.getById(id);

        List<Asset> list = assetService.getAll();

        if (list.isEmpty() || !optional.isPresent()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        List<Asset> finalList = ownerReportService.filter(list, optional.get());

        if (finalList.isEmpty()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        List<AssetDTO> dtos = assetService.convertAssetsToDTO(finalList);

        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping(
            value = "/{id}",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<OwnerReport> getOne(@PathVariable Long id) {
        Optional<OwnerReport> optional = ownerReportService.getById(id);

        if (!optional.isPresent()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(optional.get(), HttpStatus.OK);
    }

    @GetMapping(
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<List<OwnerReport>> getAll() {
        List<OwnerReport> list = ownerReportService.getAll();

        if (list.isEmpty()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @PostMapping(
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<String> create(@RequestBody OwnerReport dto){

        if (ownerReportService.getById(dto.getId()).isPresent()){
            return new ResponseEntity<>("Owner report not created!", HttpStatus.BAD_REQUEST);
        }

        OwnerReport toDb = new OwnerReport();
        toDb.setFirstName(dto.getFirstName());
        toDb.setLastName(dto.getLastName());
        toDb.setMiddleName(dto.getMiddleName());
        toDb.setPhoneNumber(dto.getPhoneNumber());
        ownerReportService.save(toDb);

        return new ResponseEntity<>("Owner report created!", HttpStatus.OK);
    }

    @PutMapping(
            value = "/{id}",
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<String> update(@RequestBody OwnerReport dto,
                                         @PathVariable Long id){

        Optional<OwnerReport> optional = ownerReportService.getById(id);

        if (!optional.isPresent()){
            return new ResponseEntity<>("Owner report not updated!", HttpStatus.BAD_REQUEST);
        }

        OwnerReport fromDb = optional.get();

        fromDb.setFirstName(dto.getFirstName());
        fromDb.setLastName(dto.getLastName());
        fromDb.setMiddleName(dto.getMiddleName());
        fromDb.setEmployeeNr(dto.getEmployeeNr());
        fromDb.setPhoneNumber(dto.getPhoneNumber());
        ownerReportService.save(fromDb);

        return new ResponseEntity<>("Owner report updated!", HttpStatus.OK);
    }

    @DeleteMapping(
            value = "/{id}")
    public ResponseEntity<String> delete(@PathVariable Long id) {
        Optional<OwnerReport> optional = ownerReportService.getById(id);

        if (!optional.isPresent()){
            return new ResponseEntity<>("Owner report not deleted!", HttpStatus.BAD_REQUEST);
        }

        ownerReportService.delete(optional.get());

        return new ResponseEntity<>("Owner report deleted!", HttpStatus.OK);
    }

}
