package rs.ac.uns.ftn.assetmanagementcybersecurity.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.SoftwareAsset;

import java.util.List;
import java.util.Optional;

public interface SoftwareAssetRepository extends JpaRepository<SoftwareAsset, Long> {
    Optional<SoftwareAsset> getByUuid(String uuid);
    List<SoftwareAsset> findAllByOrderByIdDesc();
    Page<SoftwareAsset> findAll(Pageable pageable);
}
