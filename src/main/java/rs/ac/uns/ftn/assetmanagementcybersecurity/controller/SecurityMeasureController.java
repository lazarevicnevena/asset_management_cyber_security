package rs.ac.uns.ftn.assetmanagementcybersecurity.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rs.ac.uns.ftn.assetmanagementcybersecurity.dto.SecurityGoalDTO;
import rs.ac.uns.ftn.assetmanagementcybersecurity.dto.SecurityMeasureDTO;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.*;
import rs.ac.uns.ftn.assetmanagementcybersecurity.service.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/security-measures")
public class SecurityMeasureController {

    @Autowired
    private SecurityMeasureService securityMeasureService;

    @Autowired
    private SecurityGoalValueService securityGoalValueService;

    @Autowired
    private SecurityMeasureTypeService securityMeasureTypeService;

    @GetMapping(
            value = "/{id}",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<SecurityMeasureDTO> getOne(@PathVariable Long id) {
        Optional<SecurityMeasure> optional = securityMeasureService.getById(id);

        if (!optional.isPresent()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(new SecurityMeasureDTO(optional.get()), HttpStatus.OK);
    }

    @GetMapping(
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<List<SecurityMeasureDTO>> getAll() {

        List<SecurityMeasure> list = securityMeasureService.getAll();

        if (list.isEmpty()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        List<SecurityMeasureDTO> dtos = new ArrayList<>();
        for (SecurityMeasure measure : list){
            dtos.add(new SecurityMeasureDTO(measure));
        }

        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PostMapping(
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<String> create(@RequestBody SecurityMeasure securityMeasure){

        SecurityMeasure toDb = new SecurityMeasure();

        toDb.setName(securityMeasure.getName());
        toDb.setDescription(securityMeasure.getDescription());

        Optional<SecurityMeasureType> optional = securityMeasureTypeService.getById(securityMeasure.getType().getId());

        if (!optional.isPresent()){
            return new ResponseEntity<String>("Security measure not created!", HttpStatus.BAD_REQUEST);
        }
        toDb.setType(optional.get());
        securityMeasureService.save(toDb);

        return new ResponseEntity<>("Security measure created!", HttpStatus.OK);
    }


    @PutMapping(
            value = "/{id}",
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<String> update(@RequestBody SecurityMeasure securityMeasure,
                                         @PathVariable Long id){

        Optional<SecurityMeasure> optional = securityMeasureService.getById(id);

        if (!optional.isPresent()){
            return new ResponseEntity<>("Security measure not updated!", HttpStatus.BAD_REQUEST);
        }

        SecurityMeasure fromDb = optional.get();

        fromDb.setName(securityMeasure.getName());
        fromDb.setDescription(securityMeasure.getDescription());
        fromDb.setType(securityMeasure.getType());
        securityMeasureService.save(fromDb);

        return new ResponseEntity<>("Security measure updated!", HttpStatus.OK);
    }

    @DeleteMapping(
            value = "/{id}")
    public ResponseEntity<String> delete(@PathVariable Long id) {
        Optional<SecurityMeasure> optional = securityMeasureService.getById(id);

        if (!optional.isPresent()){
            return new ResponseEntity<>("Security measure not deleted!", HttpStatus.BAD_REQUEST);
        }

        securityMeasureService.delete(optional.get());

        return new ResponseEntity<>("Security measure deleted!", HttpStatus.OK);
    }
}
