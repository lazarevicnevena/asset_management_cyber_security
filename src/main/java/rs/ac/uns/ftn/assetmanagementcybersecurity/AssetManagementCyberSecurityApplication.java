package rs.ac.uns.ftn.assetmanagementcybersecurity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AssetManagementCyberSecurityApplication {

	public static void main(String[] args) {
		SpringApplication.run(AssetManagementCyberSecurityApplication.class, args);
	}

}
