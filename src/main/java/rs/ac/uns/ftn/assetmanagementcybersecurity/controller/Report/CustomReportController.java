package rs.ac.uns.ftn.assetmanagementcybersecurity.controller.Report;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rs.ac.uns.ftn.assetmanagementcybersecurity.dto.AssetDTO;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.Asset;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.Report.CustomReport;
import rs.ac.uns.ftn.assetmanagementcybersecurity.service.AssetService;
import rs.ac.uns.ftn.assetmanagementcybersecurity.service.Report.CustomReportService;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/custom-reports")
public class CustomReportController {

    @Autowired
    private AssetService assetService;

    @Autowired
    private CustomReportService customReportService;

    @SuppressWarnings("Duplicates")
    @GetMapping(
            value = "/{id}/filter",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<List<AssetDTO>> customReport(@PathVariable Long id) {

        Optional<CustomReport> optional = customReportService.getById(id);

        List<Asset> list = assetService.getAll();

        if (list.isEmpty() || !optional.isPresent()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        List<Asset> finalList = customReportService.filter(list, optional.get());

        if (finalList.isEmpty()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        List<AssetDTO> dtos = assetService.convertAssetsToDTO(finalList);

        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping(
            value = "/{id}",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<CustomReport> getOne(@PathVariable Long id) {
        Optional<CustomReport> optional = customReportService.getById(id);

        if (!optional.isPresent()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(optional.get(), HttpStatus.OK);
    }

    @GetMapping(
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<List<CustomReport>> getAll() {
        List<CustomReport> list = customReportService.getAll();

        if (list.isEmpty()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @PostMapping(
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<String> create(@RequestBody CustomReport dto){

        if (customReportService.getById(dto.getId()).isPresent()){
            return new ResponseEntity<>("Custom report not created!", HttpStatus.BAD_REQUEST);
        }

        CustomReport toDb = new CustomReport();
        toDb.setName(dto.getName());
        toDb.setDescription(dto.getDescription());
        toDb.setSubtype(dto.getSubtype());
        toDb.setType(dto.getType());
        toDb.setBuilding(dto.getBuilding());
        toDb.setRoom(dto.getRoom());
        toDb.setOwner(dto.getOwner());
        toDb.setSecurityGoalValue(dto.getSecurityGoalValue());
        toDb.setSecurityMeasure(dto.getSecurityMeasure());
        customReportService.save(toDb);

        return new ResponseEntity<>("Custom report created!", HttpStatus.OK);
    }

    @PutMapping(
            value = "/{id}",
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<String> update(@RequestBody CustomReport dto,
                                         @PathVariable Long id){

        Optional<CustomReport> optional = customReportService.getById(id);

        if (!optional.isPresent()){
            return new ResponseEntity<>("Custom report not updated!", HttpStatus.BAD_REQUEST);
        }

        CustomReport fromDb = optional.get();

        fromDb.setName(dto.getName());
        fromDb.setDescription(dto.getDescription());
        fromDb.setSubtype(dto.getSubtype());
        fromDb.setType(dto.getType());
        fromDb.setBuilding(dto.getBuilding());
        fromDb.setRoom(dto.getRoom());
        fromDb.setOwner(dto.getOwner());
        fromDb.setSecurityGoalValue(dto.getSecurityGoalValue());
        fromDb.setSecurityMeasure(dto.getSecurityMeasure());
        customReportService.save(fromDb);

        return new ResponseEntity<>("Custom report updated!", HttpStatus.OK);
    }

    @DeleteMapping(
            value = "/{id}")
    public ResponseEntity<String> delete(@PathVariable Long id) {
        Optional<CustomReport> optional = customReportService.getById(id);

        if (!optional.isPresent()){
            return new ResponseEntity<>("Custom report not deleted!", HttpStatus.BAD_REQUEST);
        }

        customReportService.delete(optional.get());

        return new ResponseEntity<>("Custom report deleted!", HttpStatus.OK);
    }

}
