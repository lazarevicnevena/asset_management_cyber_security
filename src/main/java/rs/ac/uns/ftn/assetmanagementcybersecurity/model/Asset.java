package rs.ac.uns.ftn.assetmanagementcybersecurity.model;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
public abstract class Asset {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;
    private String description;
    private String state;
    private String uuid;
    private LocalDateTime created;
    private AssetType type;

    @ManyToOne(fetch = FetchType.EAGER)
    private Owner owner;

    @ManyToMany(fetch = FetchType.LAZY)
    private List<SecurityGoalValue> securityGoalValues = new ArrayList<>();

}
