package rs.ac.uns.ftn.assetmanagementcybersecurity.model;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
public class Owner {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String firstName;
    private String middleName;
    private String lastName;
    private String phoneNumber;
    private String employeeId;

    public Owner(String fName,
                 String mName,
                 String lastName,
                 String phoneNr,
                 String employeeId){
        this.firstName = fName;
        this.middleName = mName;
        this.lastName = lastName;
        this.phoneNumber = phoneNr;
        this.employeeId = employeeId;
    }

}
