package rs.ac.uns.ftn.assetmanagementcybersecurity.repository.Report;

import org.springframework.data.jpa.repository.JpaRepository;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.Report.SecurityReport;

import java.util.List;


public interface SecurityReportRepository extends JpaRepository<SecurityReport, Long> {
    List<SecurityReport> findAllByOrderByIdDesc();
}
