package rs.ac.uns.ftn.assetmanagementcybersecurity.dto;

import lombok.*;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.SecurityGoal;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.SecurityGoalValue;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.SecurityMeasure;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class SecurityGoalValueDTO {

    private Long id;

    private String name;
    private String description;

    private SecurityGoalDTO securityGoal;

    private List<SecurityMeasureDTO> securityMeasures = new ArrayList<>();

    public SecurityGoalValueDTO(SecurityGoalValue securityGoalValue){
        this.id = securityGoalValue.getId();
        this.name = securityGoalValue.getName();
        this.description = securityGoalValue.getDescription();
        this.securityGoal = new SecurityGoalDTO(securityGoalValue.getSecurityGoal());
        for (SecurityMeasure measure: securityGoalValue.getSecurityMeasures()){
            this.securityMeasures.add(new SecurityMeasureDTO(measure));
        }
    }
}
