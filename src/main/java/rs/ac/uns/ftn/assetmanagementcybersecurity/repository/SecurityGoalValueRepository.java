package rs.ac.uns.ftn.assetmanagementcybersecurity.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.SecurityGoal;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.SecurityGoalValue;

import java.util.List;

public interface SecurityGoalValueRepository extends JpaRepository<SecurityGoalValue, Long> {
    List<SecurityGoalValue> getByName(String name);
    List<SecurityGoalValue> getBySecurityGoal_Name(String name);
    List<SecurityGoalValue> findAllByOrderByIdDesc();
    Page<SecurityGoalValue> findAll(Pageable pageable);
    Page<SecurityGoalValue> findAllBySecurityGoal_Name(Pageable pageable, String name);
}
