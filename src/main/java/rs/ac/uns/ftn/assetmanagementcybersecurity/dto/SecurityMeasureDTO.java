package rs.ac.uns.ftn.assetmanagementcybersecurity.dto;

import lombok.*;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.SecurityGoal;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.SecurityMeasure;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.SecurityMeasureType;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class SecurityMeasureDTO {

    private Long id;

    private String name;
    private String description;

    private SecurityMeasureType type;

    public SecurityMeasureDTO(SecurityMeasure securityMeasure){
        this.id = securityMeasure.getId();
        this.name = securityMeasure.getName();
        this.description = securityMeasure.getDescription();
        this.type = securityMeasure.getType();
    }

}