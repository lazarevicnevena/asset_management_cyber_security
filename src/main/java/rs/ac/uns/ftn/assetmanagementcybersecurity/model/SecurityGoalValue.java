package rs.ac.uns.ftn.assetmanagementcybersecurity.model;

import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
public class SecurityGoalValue {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;
    private String description;

    @ManyToMany(fetch = FetchType.LAZY)
    private List<SecurityMeasure> securityMeasures = new ArrayList<>();

    @ManyToOne(fetch = FetchType.EAGER)
    private SecurityGoal securityGoal;

    public SecurityGoalValue(String name,
                             String description,
                             SecurityGoal securityGoal){
        this.name = name;
        this.description = description;
        this.securityGoal = securityGoal;
    }
}
