package rs.ac.uns.ftn.assetmanagementcybersecurity.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.Owner;
import rs.ac.uns.ftn.assetmanagementcybersecurity.repository.OwnerRepository;

import java.util.List;
import java.util.Optional;

@Service
public class OwnerService {

    @Autowired
    private OwnerRepository ownerRepository;

    public Optional<Owner> getById(Long id) {
        return ownerRepository.findById(id);
    }

    public Optional<Owner> getByEmployeeId(String employeeId) {
        return ownerRepository.findByEmployeeId(employeeId);
    }

    public List<Owner> getAll(){
        return ownerRepository.findAllByOrderByIdDesc();
    }

    public Owner save(Owner owner){
        return ownerRepository.save(owner);
    }

    public void delete(Owner owner){
        ownerRepository.delete(owner);
    }
}
