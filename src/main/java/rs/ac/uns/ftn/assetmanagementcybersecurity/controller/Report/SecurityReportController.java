package rs.ac.uns.ftn.assetmanagementcybersecurity.controller.Report;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rs.ac.uns.ftn.assetmanagementcybersecurity.dto.AssetDTO;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.Asset;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.Report.CustomReport;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.Report.SecurityReport;
import rs.ac.uns.ftn.assetmanagementcybersecurity.service.AssetService;
import rs.ac.uns.ftn.assetmanagementcybersecurity.service.Report.CustomReportService;
import rs.ac.uns.ftn.assetmanagementcybersecurity.service.Report.SecurityReportService;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/security-reports")
public class SecurityReportController {

    @Autowired
    private AssetService assetService;

    @Autowired
    private SecurityReportService securityReportService;

    @SuppressWarnings("Duplicates")
    @GetMapping(
            value = "/{id}/filter",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<List<AssetDTO>> securityReport(@PathVariable Long id) {

        Optional<SecurityReport> optional = securityReportService.getById(id);

        List<Asset> list = assetService.getAll();

        if (list.isEmpty() || !optional.isPresent()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        List<Asset> finalList = securityReportService.filter(list, optional.get());

        if (finalList.isEmpty()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        List<AssetDTO> dtos = assetService.convertAssetsToDTO(finalList);

        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping(
            value = "/{id}",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<SecurityReport> getOne(@PathVariable Long id) {
        Optional<SecurityReport> optional = securityReportService.getById(id);

        if (!optional.isPresent()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(optional.get(), HttpStatus.OK);
    }

    @GetMapping(
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<List<SecurityReport>> getAll() {
        List<SecurityReport> list = securityReportService.getAll();

        if (list.isEmpty()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @PostMapping(
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<String> create(@RequestBody SecurityReport dto){

        if (securityReportService.getById(dto.getId()).isPresent()){
            return new ResponseEntity<>("Security report not created!", HttpStatus.BAD_REQUEST);
        }

        SecurityReport toDb = new SecurityReport();
        toDb.setName(dto.getName());
        toDb.setSecurityGoalValues(dto.getSecurityGoalValues());
        toDb.setSecurityMeasures(dto.getSecurityMeasures());
        securityReportService.save(toDb);

        return new ResponseEntity<>("Security report created!", HttpStatus.OK);
    }

    @PutMapping(
            value = "/{id}",
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<String> update(@RequestBody SecurityReport dto,
                                         @PathVariable Long id){

        Optional<SecurityReport> optional = securityReportService.getById(id);

        if (!optional.isPresent()){
            return new ResponseEntity<>("Security report not updated!", HttpStatus.BAD_REQUEST);
        }

        SecurityReport fromDb = optional.get();

        fromDb.setName(dto.getName());

        fromDb.setSecurityGoalValues(dto.getSecurityGoalValues());
        fromDb.setSecurityMeasures(dto.getSecurityMeasures());
        securityReportService.save(fromDb);

        return new ResponseEntity<>("Security report updated!", HttpStatus.OK);
    }

    @DeleteMapping(
            value = "/{id}")
    public ResponseEntity<String> delete(@PathVariable Long id) {
        Optional<SecurityReport> optional = securityReportService.getById(id);

        if (!optional.isPresent()){
            return new ResponseEntity<>("Security report not deleted!", HttpStatus.BAD_REQUEST);
        }

        securityReportService.delete(optional.get());

        return new ResponseEntity<>("Security report deleted!", HttpStatus.OK);
    }

}
