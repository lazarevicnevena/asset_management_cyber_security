package rs.ac.uns.ftn.assetmanagementcybersecurity.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rs.ac.uns.ftn.assetmanagementcybersecurity.dto.*;
import rs.ac.uns.ftn.assetmanagementcybersecurity.model.*;
import rs.ac.uns.ftn.assetmanagementcybersecurity.service.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping(value = "/asset-networks")
public class AssetNetworkController {

    @Autowired
    private AssetNetworkService assetNetworkService;

    @Autowired
    private AssetService assetService;

    @Autowired
    private SoftwareAssetService softwareAssetService;

    @Autowired
    private HardwareAssetService hardwareAssetService;

    @Autowired
    private VirtualAssetService virtualAssetService;

    @Autowired
    private DataAssetService dataAssetService;

    @Autowired
    private LocationService locationService;

    @Autowired
    private SecurityGoalValueService securityGoalValueService;

    @Autowired
    private OwnerService ownerService;

    @GetMapping(
            value = "/{id}",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<AssetNetworkDTO> getOne(@PathVariable Long id) {
        Optional<AssetNetwork> optional = assetNetworkService.getById(id);

        if (!optional.isPresent()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(new AssetNetworkDTO(optional.get()), HttpStatus.OK);
    }

    @GetMapping(
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<List<AssetNetworkDTO>> getAll() {
        List<AssetNetwork> list = assetNetworkService.getAll();

        if (list.isEmpty()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        List<AssetNetworkDTO> dtos = new ArrayList<>();
        for (AssetNetwork network: list){
            dtos.add(new AssetNetworkDTO(network));
        }

        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @SuppressWarnings("Duplicates")
    @PostMapping(
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<String> create(@RequestBody AssetNetworkDTO dto){

        if (assetNetworkService.getByName(dto.getName()).isPresent()){
            return new ResponseEntity<>("Asset network not created!", HttpStatus.BAD_REQUEST);
        }

        AssetNetwork toDb = new AssetNetwork();
        toDb.setName(dto.getName());
        toDb.setDescription(dto.getDescription());

        if (dto.getAssets() != null){
            for (AssetDTO assetDTO : dto.getAssets()) {
                Optional<Asset> asset = assetService.getByUuid(assetDTO.getUuid());
                if (asset.isPresent()){
                    toDb.getAssets().add(asset.get());
                }
            }
        }

        assetNetworkService.save(toDb);

        return new ResponseEntity<>("Asset network created!", HttpStatus.OK);
    }

    @SuppressWarnings("Duplicates")
    @PutMapping(
            value = "/{id}",
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<String> update(@RequestBody AssetNetworkDTO assetNetworkDTO,
                                         @PathVariable Long id){

        Optional<AssetNetwork> optional = assetNetworkService.getById(id);

        if (!optional.isPresent()){
            return new ResponseEntity<>("Asset network not updated!", HttpStatus.BAD_REQUEST);
        }

        AssetNetwork fromDb = optional.get();

        fromDb.setName(assetNetworkDTO.getName());
        fromDb.setDescription(assetNetworkDTO.getDescription());
        fromDb.getAssets().clear();

        if (assetNetworkDTO.getAssets() != null){
            for (AssetDTO assetDTO : assetNetworkDTO.getAssets()) {
                Optional<Asset> assetDb = assetService.getByUuid(assetDTO.getUuid());
                if (assetDb.isPresent()){
                    fromDb.getAssets().add(assetDb.get());
                }
            }
        }

        assetNetworkService.save(fromDb);

        return new ResponseEntity<>("Asset network updated!", HttpStatus.OK);
    }

    @DeleteMapping(
            value = "/{id}")
    public ResponseEntity<String> delete(@PathVariable Long id) {
        Optional<AssetNetwork> optional = assetNetworkService.getById(id);

        if (!optional.isPresent()){
            return new ResponseEntity<>("Asset network not deleted!", HttpStatus.BAD_REQUEST);
        }

        assetNetworkService.delete(optional.get());

        return new ResponseEntity<>("Asset network deleted!", HttpStatus.OK);
    }

    @SuppressWarnings("Duplicates")
    @PostMapping(
            value = "/{id}/hardware-assets/from-dialog",
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<String> addHardwareAsset(@PathVariable Long id,
                                           @RequestBody HardwareAssetDTO hardwareAssetDTO) {
        Optional<AssetNetwork> optional = assetNetworkService.getById(id);

        if (!optional.isPresent()){
            return new ResponseEntity<>("Asset network not found", HttpStatus.BAD_REQUEST);
        }

        AssetNetwork assetNetwork = optional.get();

        Optional<Location> locationOptional = locationService.getById(hardwareAssetDTO.getLocation().getId());
        if (!locationOptional.isPresent()){
            return new ResponseEntity<>("Location not found", HttpStatus.BAD_REQUEST);
        }

        Optional<Owner> owner = ownerService.getByEmployeeId(hardwareAssetDTO.getOwner().getEmployeeId());

        if (!owner.isPresent()){
            return new ResponseEntity<>("Owner not found", HttpStatus.BAD_REQUEST);
        }

        HardwareAsset toDb = new HardwareAsset(
                hardwareAssetDTO.getName(),
                hardwareAssetDTO.getDescription(),
                hardwareAssetDTO.getHostName(),
                hardwareAssetDTO.getMacAddress(),
                hardwareAssetDTO.getIpAddress(),
                LocalDateTime.now(),
                null,
                hardwareAssetDTO.getType(),
                "created",
                UUID.randomUUID().toString().replaceAll("-", ""),
                owner.get(),
                hardwareAssetDTO.getSerialNumber(),
                hardwareAssetDTO.getModel(),
                hardwareAssetDTO.getOperatingSystem(),
                locationOptional.get());

        if (hardwareAssetDTO.getSecurityGoalValues() != null){
            for (SecurityGoalValueDTO goalValueDTO : hardwareAssetDTO.getSecurityGoalValues()) {
                Optional<SecurityGoalValue> securityGoalValue = securityGoalValueService.getById(goalValueDTO.getId());
                if (securityGoalValue.isPresent()){
                    toDb.getSecurityGoalValues().add(securityGoalValue.get());
                }
            }
        }

        HardwareAsset ha = hardwareAssetService.save(toDb);

        assetNetwork.getAssets().add(ha);
        assetNetworkService.save(assetNetwork);

        return new ResponseEntity<>("Asset network updated!", HttpStatus.OK);
    }

    @SuppressWarnings("Duplicates")
    @PostMapping(
            value = "/{id}/software-assets/from-dialog",
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<String> addSoftwareAsset(@PathVariable Long id,
                                           @RequestBody SoftwareAssetDTO softwareAssetDTO) {
        Optional<AssetNetwork> optional = assetNetworkService.getById(id);

        if (!optional.isPresent()){
            return new ResponseEntity<>("Asset network not found", HttpStatus.BAD_REQUEST);
        }

        AssetNetwork assetNetwork = optional.get();

        Optional<Owner> owner = ownerService.getByEmployeeId(softwareAssetDTO.getOwner().getEmployeeId());

        if (!owner.isPresent()){
            return new  ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        SoftwareAsset toDb = new SoftwareAsset(
                softwareAssetDTO.getName(),
                softwareAssetDTO.getDescription(),
                LocalDateTime.now(),
                softwareAssetDTO.getType(),
                "created",
                UUID.randomUUID().toString().replaceAll("-", ""),
                owner.get());

        if (softwareAssetDTO.getHardwareAssets() != null){
            for (HardwareAssetDTO hardwareAssetDTO : softwareAssetDTO.getHardwareAssets()) {
                Optional<HardwareAsset> hardwareAsset = hardwareAssetService.getByUuid(hardwareAssetDTO.getUuid());
                if (hardwareAsset.isPresent()){
                    toDb.getHardwareAssets().add(hardwareAsset.get());
                }
            }
        }

        if (softwareAssetDTO.getSecurityGoalValues() != null){
            for (SecurityGoalValueDTO goalValueDTO : softwareAssetDTO.getSecurityGoalValues()) {
                Optional<SecurityGoalValue> securityGoalValue = securityGoalValueService.getById(goalValueDTO.getId());
                if (securityGoalValue.isPresent()){
                    toDb.getSecurityGoalValues().add(securityGoalValue.get());
                }
            }
        }

        SoftwareAsset softwareAsset = softwareAssetService.save(toDb);
        assetNetwork.getAssets().add(softwareAsset);
        assetNetworkService.save(assetNetwork);
        return new ResponseEntity<>("Asset network updated!", HttpStatus.OK);

    }

    @SuppressWarnings("Duplicates")
    @PostMapping(
            value = "/{id}/virtual-assets/from-dialog",
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<String> addVirtualAsset(@PathVariable Long id,
                                                  @RequestBody VirtualAssetDTO virtualAssetDTO) {
        Optional<AssetNetwork> optional = assetNetworkService.getById(id);

        if (!optional.isPresent()){
            return new ResponseEntity<>("Asset network not found", HttpStatus.BAD_REQUEST);
        }

        AssetNetwork assetNetwork = optional.get();

        Optional<Owner> owner = ownerService.getByEmployeeId(virtualAssetDTO.getOwner().getEmployeeId());

        if (!owner.isPresent()){
            return new  ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        Optional<HardwareAsset> hardwareAsset = hardwareAssetService.getByUuid(virtualAssetDTO.getHardwareAsset().getUuid());

        if (!hardwareAsset.isPresent()){
            return new  ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        VirtualAsset toDb = new VirtualAsset(
                virtualAssetDTO.getName(),
                virtualAssetDTO.getDescription(),
                virtualAssetDTO.getMacAddress(),
                virtualAssetDTO.getIpAddress(),
                LocalDateTime.now(),
                null,
                virtualAssetDTO.getType(),
                "created",
                UUID.randomUUID().toString().replaceAll("-", ""),
                owner.get(),
                virtualAssetDTO.getOperatingSystem(),
                hardwareAsset.get());

        if (virtualAssetDTO.getSecurityGoalValues() != null){
            for (SecurityGoalValueDTO goalValueDTO : virtualAssetDTO.getSecurityGoalValues()) {
                Optional<SecurityGoalValue> securityGoalValue = securityGoalValueService.getById(goalValueDTO.getId());
                if (securityGoalValue.isPresent()){
                    toDb.getSecurityGoalValues().add(securityGoalValue.get());
                }
            }
        }

        VirtualAsset virtualAsset = virtualAssetService.save(toDb);
        assetNetwork.getAssets().add(virtualAsset);
        assetNetworkService.save(assetNetwork);
        return new ResponseEntity<>("Asset network updated!", HttpStatus.OK);
    }

    @SuppressWarnings("Duplicates")
    @PostMapping(
            value = "/{id}/data-assets/from-dialog",
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<String> addDataAsset(@PathVariable Long id,
                                               @RequestBody DataAssetDTO dataAssetDTO) {
        Optional<AssetNetwork> optional = assetNetworkService.getById(id);

        if (!optional.isPresent()){
            return new ResponseEntity<>("Asset network not found", HttpStatus.BAD_REQUEST);
        }

        AssetNetwork assetNetwork = optional.get();

        Optional<Owner> owner = ownerService.getByEmployeeId(dataAssetDTO.getOwner().getEmployeeId());

        if (!owner.isPresent()){
            return new  ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        DataAsset toDb = new DataAsset(
                dataAssetDTO.getName(),
                dataAssetDTO.getDescription(),
                LocalDateTime.now(),
                dataAssetDTO.getType(),
                "created",
                UUID.randomUUID().toString().replaceAll("-", ""),
                owner.get());

        if (dataAssetDTO.getSecurityGoalValues() != null){
            for (SecurityGoalValueDTO goalValueDTO : dataAssetDTO.getSecurityGoalValues()) {
                Optional<SecurityGoalValue> securityGoalValue = securityGoalValueService.getById(goalValueDTO.getId());
                if (securityGoalValue.isPresent()){
                    toDb.getSecurityGoalValues().add(securityGoalValue.get());
                }
            }
        }

        DataAsset dataAsset = dataAssetService.save(toDb);
        assetNetwork.getAssets().add(dataAsset);
        assetNetworkService.save(assetNetwork);
        return new ResponseEntity<>("Asset network updated!", HttpStatus.OK);

    }
}