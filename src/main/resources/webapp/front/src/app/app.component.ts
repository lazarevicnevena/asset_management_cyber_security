import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  title = 'asset-management-front';

  constructor( private router: Router) { }

  ngOnInit() {
  }

  locationList() {
    this.router.navigateByUrl('locations');
  }

  ownerList() {
    this.router.navigateByUrl('owners');
  }

  assetsList() {
    this.router.navigateByUrl('assets');
  }

  assetsHardwareList() {
    this.router.navigateByUrl('hardware-assets');
  }

  assetsSoftwareList() {
    this.router.navigateByUrl('software-assets');
  }

  assetsVirtualList() {
    this.router.navigateByUrl('virtual-assets');
  }

  assetsDataList() {
    this.router.navigateByUrl('data-assets');
  }

  networkList() {
    this.router.navigateByUrl('asset-networks');
  }

  goalList() {
    this.router.navigateByUrl('security-goals');
  }

  goalValueList() {
    this.router.navigateByUrl('security-goal-values');
  }

  measureList() {
    this.router.navigateByUrl('security-measures');
  }

  measureTypeList() {
    this.router.navigateByUrl('security-measure-types');
  }

  customRList() {
    this.router.navigateByUrl('custom-reports');
  }

  securityRList() {
    this.router.navigateByUrl('security-reports');
  }

  creationDateRList() {
    this.router.navigateByUrl('creation-date-reports');
  }

  ownerRList() {
    this.router.navigateByUrl('owner-reports');
  }

  networkRList() {
    this.router.navigateByUrl('network-reports');
  }
}
