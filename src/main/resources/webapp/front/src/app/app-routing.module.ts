import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LocationListComponent} from './components/location/location-list/location-list.component';
import {OwnerListComponent} from './components/owner/owner-list/owner-list.component';
import {AssetListComponent} from './components/asset/asset/asset-list/asset-list.component';
import {HardwareAssetListComponent} from './components/asset/hardware-asset/hardware-asset-list/hardware-asset-list.component';
import {SoftwareAssetListComponent} from './components/asset/software-asset/software-asset-list/software-asset-list.component';
import {VirtualAssetListComponent} from './components/asset/virtual-asset/virtual-asset-list/virtual-asset-list.component';
import {DataAssetListComponent} from './components/asset/data-asset/data-asset-list/data-asset-list.component';
import {SecurityGoalListComponent} from './components/security-goal/security-goal-list/security-goal-list.component';
import {SecurityGoalValueListComponent} from './components/security-goal-value/security-goal-value-list/security-goal-value-list.component';
import {SecurityMeasureListComponent} from './components/security-measure/security-measure-list/security-measure-list.component';
import {SecurityMeasureTypeListComponent} from './components/security-measure-type/security-measure-type-list/security-measure-type-list.component';
import {AssetNetworkListComponent} from './components/asset-network/asset-network-list/asset-network-list.component';
import {CustomReportListComponent} from './components/report/custom/custom-report-list/custom-report-list.component';
import {SecurityReportListComponent} from './components/report/security/security-report-list/security-report-list.component';
import {CreationDateReportListComponent} from './components/report/creation-date/creation-date-report-list/creation-date-report-list.component';
import {OwnerReportListComponent} from './components/report/owner/owner-report-list/owner-report-list.component';
import {NetworkReportListComponent} from './components/report/network/network-report-list/network-report-list.component';

const routes: Routes = [
  {
    path: 'assets',
    component: AssetListComponent
  },
  {
    path: 'hardware-assets',
    component: HardwareAssetListComponent
  },
  {
    path: 'software-assets',
    component: SoftwareAssetListComponent
  },
  {
    path: 'virtual-assets',
    component: VirtualAssetListComponent
  },
  {
    path: 'data-assets',
    component: DataAssetListComponent
  },
  {
    path: 'asset-networks',
    component: AssetNetworkListComponent
  },
  {
    path: 'security-goals',
    component: SecurityGoalListComponent
  },
  {
    path: 'security-goal-values',
    component: SecurityGoalValueListComponent
  },
  {
    path: 'security-measures',
    component: SecurityMeasureListComponent
  },
  {
    path: 'security-measure-types',
    component: SecurityMeasureTypeListComponent
  },
  {
    path: 'locations',
    component: LocationListComponent
  },
  {
    path: 'owners',
    component: OwnerListComponent
  },
  {
    path: 'custom-reports',
    component: CustomReportListComponent
  },
  {
    path: 'security-reports',
    component: SecurityReportListComponent
  },
  {
    path: 'creation-date-reports',
    component: CreationDateReportListComponent
  },
  {
    path: 'owner-reports',
    component: OwnerReportListComponent
  },
  {
    path: 'network-reports',
    component: NetworkReportListComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
