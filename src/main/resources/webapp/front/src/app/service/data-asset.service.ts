import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {DataAsset} from '../model/data-asset';
import {SecurityGoalValue} from '../model/security-goal-value';

@Injectable()
export class DataAssetService {
  private url = '/api/data-assets';

  constructor(private http: HttpClient) {
  }

  getAll(): Promise<DataAsset[]> {
    return this.http.get(this.url)
      .toPromise()
      .then(res => res as DataAsset[]);
  }

  addGoalValueDirectly(uuid: string, goalValue: SecurityGoalValue): Promise<string> {
    return this.http.post(`${this.url}/${uuid}/security-goal-values/from-dialog`, goalValue, {responseType: 'text'})
      .toPromise()
      .then(res =>
        res as string);
  }


  getOne(id: string): Promise<DataAsset> {
    return this.http.get(`${this.url}/${id}`)
      .toPromise()
      .then(res =>
        res as DataAsset);
  }

  create(entity: DataAsset): Promise<string> {

    return this.http
      .post(this.url, entity, {responseType: 'text'})
      .toPromise()
      .then(res => res as string);
  }

  update(entity: DataAsset): Promise<string> {
    const url = `${this.url}/${entity.uuid}`;
    return this.http
      .put(url, entity, {responseType: 'text'})
      .toPromise()
      .then(res => res as string);
  }

  delete(id: string): Promise<{}> {
    const url = `${this.url}/${id}`;
    return this.http
      .delete(url, {responseType: 'text'})
      .toPromise();
  }

}
