import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {SoftwareAsset} from '../model/software-asset';
import {HardwareAsset} from '../model/hardware-asset';
import {SecurityGoalValue} from '../model/security-goal-value';

@Injectable()
export class SoftwareAssetService {
  private url = '/api/software-assets';

  constructor(private http: HttpClient) {
  }

  getAll(): Promise<SoftwareAsset[]> {
    return this.http.get(this.url)
      .toPromise()
      .then(res => res as SoftwareAsset[]);
  }

  addGoalValueDirectly(uuid: string, goalValue: SecurityGoalValue): Promise<string> {
    return this.http.post(`${this.url}/${uuid}/security-goal-values/from-dialog`, goalValue, {responseType: 'text'})
      .toPromise()
      .then(res =>
        res as string);
  }

  addHardwaresDirectly(uuid: string, hardware: HardwareAsset): Promise<string> {
    return this.http.post(`${this.url}/${uuid}/hardware-assets/from-dialog`, hardware, {responseType: 'text'})
      .toPromise()
      .then(res =>
        res as string);
  }


  getOne(id: string): Promise<SoftwareAsset> {
    return this.http.get(`${this.url}/${id}`)
      .toPromise()
      .then(res =>
        res as SoftwareAsset);
  }

  getSoftwareHAssets(id: string): Promise<HardwareAsset> {
    return this.http.get(`${this.url}/${id}/hardware-assets`)
      .toPromise()
      .then(res =>
        res as HardwareAsset);
  }

  getNonSoftwareHAssets(id: string): Promise<HardwareAsset> {
    return this.http.get(`${this.url}/${id}/non-hardware-assets`)
      .toPromise()
      .then(res =>
        res as HardwareAsset);
  }

  changeSoftwareHAssets(id: string, flag: boolean, assets: HardwareAsset[]): Promise<string> {
    return this.http.put(`${this.url}/${id}/hardware-assets/${flag}`, assets, {responseType: 'text'})
      .toPromise()
      .then(res =>
        res as string);
  }

  create(entity: SoftwareAsset): Promise<string> {

    return this.http
      .post(this.url, entity, {responseType: 'text'})
      .toPromise()
      .then(res => res as string);
  }

  update(entity: SoftwareAsset): Promise<string> {
    const url = `${this.url}/${entity.uuid}`;
    return this.http
      .put(url, entity, {responseType: 'text'})
      .toPromise()
      .then(res => res as string);
  }

  delete(id: string): Promise<{}> {
    const url = `${this.url}/${id}`;
    return this.http
      .delete(url, {responseType: 'text'})
      .toPromise();
  }

}
