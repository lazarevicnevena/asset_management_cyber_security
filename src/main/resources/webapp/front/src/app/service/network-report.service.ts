import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Asset} from '../model/asset';
import {NetworkReport} from '../model/network-report';

@Injectable()
export class NetworkReportService {
  private url = '/api/network-reports';

  constructor(private http: HttpClient) {
  }

  getAll(): Promise<NetworkReport[]> {
    return this.http.get(this.url)
      .toPromise()
      .then(res => res as NetworkReport[]);
  }

  getOne(id: number): Promise<NetworkReport> {
    return this.http.get(`${this.url}/${id}`)
      .toPromise()
      .then(res =>
        res as NetworkReport);
  }

  create(entity: NetworkReport): Promise<string> {

    return this.http
      .post(this.url, entity, {responseType: 'text'})
      .toPromise()
      .then(res => res as string);
  }

  update(entity: NetworkReport): Promise<string> {
    const url = `${this.url}/${entity.id}`;
    return this.http
      .put(url, entity, {responseType: 'text'})
      .toPromise()
      .then(res => res as string);
  }

  delete(id: number): Promise<{}> {
    const url = `${this.url}/${id}`;
    return this.http
      .delete(url, {responseType: 'text'})
      .toPromise();
  }

  filter(id: number): Promise<Asset[]> {
    const url = `${this.url}/${id}/filter`;
    return this.http.get(url)
      .toPromise()
      .then(res => res as Asset[]);
  }

}
