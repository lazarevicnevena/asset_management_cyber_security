import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {AssetNetwork} from '../model/asset-network';
import {HardwareAsset} from '../model/hardware-asset';
import {SoftwareAsset} from '../model/software-asset';
import {DataAsset} from '../model/data-asset';
import {VirtualAsset} from '../model/virtual-asset';

@Injectable()
export class AssetNetworkService {
  private url = '/api/asset-networks';

  constructor(private http: HttpClient) {
  }

  getAll(): Promise<AssetNetwork[]> {
    return this.http.get(this.url)
      .toPromise()
      .then(res => res as AssetNetwork[]);
  }

  getOne(id: number): Promise<AssetNetwork> {
    return this.http.get(`${this.url}/${id}`)
      .toPromise()
      .then(res =>
        res as AssetNetwork);
  }

  addHAssetsDirectly(id: number, asset: HardwareAsset): Promise<string> {
    return this.http.post(`${this.url}/${id}/hardware-assets/from-dialog`, asset, {responseType: 'text'})
      .toPromise()
      .then(res =>
        res as string);
  }

  addSAssetsDirectly(id: number, asset: SoftwareAsset): Promise<string> {
    return this.http.post(`${this.url}/${id}/software-assets/from-dialog`, asset, {responseType: 'text'})
      .toPromise()
      .then(res =>
        res as string);
  }

  addVAssetsDirectly(id: number, asset: VirtualAsset): Promise<string> {
    return this.http.post(`${this.url}/${id}/virtual-assets/from-dialog`, asset, {responseType: 'text'})
      .toPromise()
      .then(res =>
        res as string);
  }

  addDAssetsDirectly(id: number, asset: DataAsset): Promise<string> {
    return this.http.post(`${this.url}/${id}/data-assets/from-dialog`, asset, {responseType: 'text'})
      .toPromise()
      .then(res =>
        res as string);
  }

  create(entity: AssetNetwork): Promise<string> {

    return this.http
      .post(this.url, entity, {responseType: 'text'})
      .toPromise()
      .then(res => res as string);
  }

  update(entity: AssetNetwork): Promise<string> {
    const url = `${this.url}/${entity.id}`;
    return this.http
      .put(url, entity, {responseType: 'text'})
      .toPromise()
      .then(res => res as string);
  }

  delete(id: number): Promise<{}> {
    const url = `${this.url}/${id}`;
    return this.http
      .delete(url, {responseType: 'text'})
      .toPromise();
  }

}
