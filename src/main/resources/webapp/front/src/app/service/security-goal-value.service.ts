import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {SecurityGoalValue} from '../model/security-goal-value';
import {SecurityMeasure} from '../model/security-measure';

@Injectable()
export class SecurityGoalValueService {
  private url = '/api/security-goal-values';

  constructor(private http: HttpClient) {
  }

  getAll(): Promise<SecurityGoalValue[]> {
    return this.http.get(this.url)
      .toPromise()
      .then(res => res as SecurityGoalValue[]);
  }

  getOne(id: number): Promise<SecurityGoalValue> {
    return this.http.get(`${this.url}/${id}`)
      .toPromise()
      .then(res =>
        res as SecurityGoalValue);
  }

  getValueMeasures(id: number): Promise<SecurityMeasure> {
    return this.http.get(`${this.url}/${id}/security-measures`)
      .toPromise()
      .then(res =>
        res as SecurityMeasure);
  }

  getNonValueMeasures(id: number): Promise<SecurityMeasure> {
    return this.http.get(`${this.url}/${id}/non-security-measures`)
      .toPromise()
      .then(res =>
        res as SecurityMeasure);
  }

  addMeasureDirectly(id: number, measure: SecurityMeasure): Promise<string> {
    return this.http.post(`${this.url}/${id}/security-measures/from-dialog`, measure, {responseType: 'text'})
      .toPromise()
      .then(res =>
        res as string);
  }

  create(entity: SecurityGoalValue): Promise<string> {

    return this.http
      .post(this.url, entity, {responseType: 'text'})
      .toPromise()
      .then(res => res as string);
  }

  update(entity: SecurityGoalValue): Promise<string> {
    const url = `${this.url}/${entity.id}`;
    return this.http
      .put(url, entity, {responseType: 'text'})
      .toPromise()
      .then(res => res as string);
  }

  delete(id: number): Promise<{}> {
    const url = `${this.url}/${id}`;
    return this.http
      .delete(url, {responseType: 'text'})
      .toPromise();
  }

}
