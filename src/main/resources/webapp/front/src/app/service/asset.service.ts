import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Asset} from '../model/asset';
import {SecurityGoalValue} from '../model/security-goal-value';

@Injectable()
export class AssetService {
  private url = '/api/assets';

  constructor(private http: HttpClient) {
  }

  getAll(): Promise<Asset[]> {
    return this.http.get(this.url)
      .toPromise()
      .then(res => res as Asset[]);
  }

  getOne(id: string): Promise<Asset> {
    return this.http.get(`${this.url}/${id}`)
      .toPromise()
      .then(res =>
        res as Asset);
  }

  getAssetValues(id: string): Promise<SecurityGoalValue> {
    return this.http.get(`${this.url}/${id}/security-goal-values`)
      .toPromise()
      .then(res =>
        res as SecurityGoalValue);
  }

  getNonAssetValues(id: string): Promise<SecurityGoalValue> {
    return this.http.get(`${this.url}/${id}/non-security-goal-values`)
      .toPromise()
      .then(res =>
        res as SecurityGoalValue);
  }

  changeAssetValues(id: string, flag: boolean, values: SecurityGoalValue[]): Promise<string> {
    return this.http.put(`${this.url}/${id}/security-goal-values/${flag}`, values, {responseType: 'text'})
      .toPromise()
      .then(res =>
        res as string);
  }

  create(entity: Asset): Promise<string> {

    return this.http
      .post(this.url, entity, {responseType: 'text'})
      .toPromise()
      .then(res => res as string);
  }

  update(entity: Asset): Promise<string> {
    const url = `${this.url}/${entity.uuid}`;
    return this.http
      .put(url, entity, {responseType: 'text'})
      .toPromise()
      .then(res => res as string);
  }

  delete(id: string): Promise<{}> {
    const url = `${this.url}/${id}`;
    return this.http
      .delete(url, {responseType: 'text'})
      .toPromise();
  }

}
