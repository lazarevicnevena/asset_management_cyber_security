import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {SecurityGoal} from '../model/security-goal';

@Injectable()
export class SecurityGoalService {
  private url = '/api/security-goals';

  constructor(private http: HttpClient) {
  }

  getAll(): Promise<SecurityGoal[]> {
    return this.http.get(this.url)
      .toPromise()
      .then(res => res as SecurityGoal[]);
  }

  getOne(id: number): Promise<SecurityGoal> {
    return this.http.get(`${this.url}/${id}`)
      .toPromise()
      .then(res =>
        res as SecurityGoal);
  }

  create(entity: SecurityGoal): Promise<string> {

    return this.http
      .post(this.url, entity, {responseType: 'text'})
      .toPromise()
      .then(res => res as string);
  }

  update(entity: SecurityGoal): Promise<string> {
    const url = `${this.url}/${entity.id}`;
    return this.http
      .put(url, entity, {responseType: 'text'})
      .toPromise()
      .then(res => res as string);
  }

  delete(id: number): Promise<{}> {
    const url = `${this.url}/${id}`;
    return this.http
      .delete(url, {responseType: 'text'})
      .toPromise();
  }

}
