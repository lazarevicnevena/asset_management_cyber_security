import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {AssetLocation} from '../model/location';

@Injectable()
export class LocationService {
  private url = '/api/locations';

  constructor(private http: HttpClient) {
  }

  getAll(): Promise<AssetLocation[]> {
    return this.http.get(this.url)
      .toPromise()
      .then(res => res as AssetLocation[]);
  }

  getOne(id: number): Promise<AssetLocation> {
    return this.http.get(`${this.url}/${id}`)
      .toPromise()
      .then(res =>
        res as AssetLocation);
  }

  create(entity: AssetLocation): Promise<string> {

    return this.http
      .post(this.url, entity, {responseType: 'text'})
      .toPromise()
      .then(res => res as string);
  }

  update(entity: AssetLocation): Promise<string> {
    const url = `${this.url}/${entity.id}`;
    return this.http
      .put(url, entity, {responseType: 'text'})
      .toPromise()
      .then(res => res as string);
  }

  delete(id: number): Promise<{}> {
    const url = `${this.url}/${id}`;
    return this.http
      .delete(url, {responseType: 'text'})
      .toPromise();
  }

}
