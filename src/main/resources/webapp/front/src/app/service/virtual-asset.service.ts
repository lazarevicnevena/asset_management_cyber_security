import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {VirtualAsset} from '../model/virtual-asset';
import {SecurityGoalValue} from '../model/security-goal-value';

@Injectable()
export class VirtualAssetService {
  private url = '/api/virtual-assets';

  constructor(private http: HttpClient) {
  }

  getAll(): Promise<VirtualAsset[]> {
    return this.http.get(this.url)
      .toPromise()
      .then(res => res as VirtualAsset[]);
  }

  addGoalValueDirectly(uuid: string, goalValue: SecurityGoalValue): Promise<string> {
    return this.http.post(`${this.url}/${uuid}/security-goal-values/from-dialog`, goalValue, {responseType: 'text'})
      .toPromise()
      .then(res =>
        res as string);
  }

  getOne(id: string): Promise<VirtualAsset> {
    return this.http.get(`${this.url}/${id}`)
      .toPromise()
      .then(res =>
        res as VirtualAsset);
  }

  create(entity: VirtualAsset): Promise<string> {

    return this.http
      .post(this.url, entity, {responseType: 'text'})
      .toPromise()
      .then(res => res as string);
  }

  update(entity: VirtualAsset): Promise<string> {
    const url = `${this.url}/${entity.uuid}`;
    return this.http
      .put(url, entity, {responseType: 'text'})
      .toPromise()
      .then(res => res as string);
  }

  delete(id: string): Promise<{}> {
    const url = `${this.url}/${id}`;
    return this.http
      .delete(url, {responseType: 'text'})
      .toPromise();
  }

}
