import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Asset} from '../model/asset';
import {SecurityReport} from '../model/security-report';

@Injectable()
export class SecurityReportService {
  private url = '/api/security-reports';

  constructor(private http: HttpClient) {
  }

  getAll(): Promise<SecurityReport[]> {
    return this.http.get(this.url)
      .toPromise()
      .then(res => res as SecurityReport[]);
  }

  getOne(id: number): Promise<SecurityReport> {
    return this.http.get(`${this.url}/${id}`)
      .toPromise()
      .then(res =>
        res as SecurityReport);
  }

  create(entity: SecurityReport): Promise<string> {

    return this.http
      .post(this.url, entity, {responseType: 'text'})
      .toPromise()
      .then(res => res as string);
  }

  update(entity: SecurityReport): Promise<string> {
    const url = `${this.url}/${entity.id}`;
    return this.http
      .put(url, entity, {responseType: 'text'})
      .toPromise()
      .then(res => res as string);
  }

  delete(id: number): Promise<{}> {
    const url = `${this.url}/${id}`;
    return this.http
      .delete(url, {responseType: 'text'})
      .toPromise();
  }

  filter(id: number): Promise<Asset[]> {
    const url = `${this.url}/${id}/filter`;
    return this.http.get(url)
      .toPromise()
      .then(res => res as Asset[]);
  }

}
