import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Asset} from '../model/asset';
import {OwnerReport} from '../model/owner-report';

@Injectable()
export class OwnerReportService {
  private url = '/api/owner-reports';

  constructor(private http: HttpClient) {
  }

  getAll(): Promise<OwnerReport[]> {
    return this.http.get(this.url)
      .toPromise()
      .then(res => res as OwnerReport[]);
  }

  getOne(id: number): Promise<OwnerReport> {
    return this.http.get(`${this.url}/${id}`)
      .toPromise()
      .then(res =>
        res as OwnerReport);
  }

  create(entity: OwnerReport): Promise<string> {

    return this.http
      .post(this.url, entity, {responseType: 'text'})
      .toPromise()
      .then(res => res as string);
  }

  update(entity: OwnerReport): Promise<string> {
    const url = `${this.url}/${entity.id}`;
    return this.http
      .put(url, entity, {responseType: 'text'})
      .toPromise()
      .then(res => res as string);
  }

  delete(id: number): Promise<{}> {
    const url = `${this.url}/${id}`;
    return this.http
      .delete(url, {responseType: 'text'})
      .toPromise();
  }

  filter(id: number): Promise<Asset[]> {
    const url = `${this.url}/${id}/filter`;
    return this.http.get(url)
      .toPromise()
      .then(res => res as Asset[]);
  }

}
