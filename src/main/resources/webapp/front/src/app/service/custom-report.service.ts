import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Asset} from '../model/asset';
import {CustomReport} from '../model/custom-report';

@Injectable()
export class CustomReportService {
  private url = '/api/custom-reports';

  constructor(private http: HttpClient) {
  }

  getAll(): Promise<CustomReport[]> {
    return this.http.get(this.url)
      .toPromise()
      .then(res => res as CustomReport[]);
  }

  getOne(id: number): Promise<CustomReport> {
    return this.http.get(`${this.url}/${id}`)
      .toPromise()
      .then(res =>
        res as CustomReport);
  }

  create(entity: CustomReport): Promise<string> {

    return this.http
      .post(this.url, entity, {responseType: 'text'})
      .toPromise()
      .then(res => res as string);
  }

  update(entity: CustomReport): Promise<string> {
    const url = `${this.url}/${entity.id}`;
    return this.http
      .put(url, entity, {responseType: 'text'})
      .toPromise()
      .then(res => res as string);
  }

  delete(id: number): Promise<{}> {
    const url = `${this.url}/${id}`;
    return this.http
      .delete(url, {responseType: 'text'})
      .toPromise();
  }

  filter(id: number): Promise<Asset[]> {
    const url = `${this.url}/${id}/filter`;
    return this.http.get(url)
      .toPromise()
      .then(res => res as Asset[]);
  }

}
