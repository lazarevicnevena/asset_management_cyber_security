import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Asset} from '../model/asset';
import {CreationDateReport} from '../model/creation-date-report';

@Injectable()
export class CreationDateReportService {
  private url = '/api/creation-date-reports';

  constructor(private http: HttpClient) {
  }

  getAll(): Promise<CreationDateReport[]> {
    return this.http.get(this.url)
      .toPromise()
      .then(res => res as CreationDateReport[]);
  }

  getOne(id: number): Promise<CreationDateReport> {
    return this.http.get(`${this.url}/${id}`)
      .toPromise()
      .then(res =>
        res as CreationDateReport);
  }

  create(entity: CreationDateReport): Promise<string> {

    return this.http
      .post(this.url, entity, {responseType: 'text'})
      .toPromise()
      .then(res => res as string);
  }

  update(entity: CreationDateReport): Promise<string> {
    const url = `${this.url}/${entity.id}`;
    return this.http
      .put(url, entity, {responseType: 'text'})
      .toPromise()
      .then(res => res as string);
  }

  delete(id: number): Promise<{}> {
    const url = `${this.url}/${id}`;
    return this.http
      .delete(url, {responseType: 'text'})
      .toPromise();
  }

  filter(id: number): Promise<Asset[]> {
    const url = `${this.url}/${id}/filter`;
    return this.http.get(url)
      .toPromise()
      .then(res => res as Asset[]);
  }

}
