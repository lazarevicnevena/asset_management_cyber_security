import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {SecurityMeasureType} from '../model/security-measure-type';

@Injectable()
export class SecurityMeasureTypeService {
  private url = '/api/security-measure-types';

  constructor(private http: HttpClient) {
  }

  getAll(): Promise<SecurityMeasureType[]> {
    return this.http.get(this.url)
      .toPromise()
      .then(res => res as SecurityMeasureType[]);
  }

  getOne(id: number): Promise<SecurityMeasureType> {
    return this.http.get(`${this.url}/${id}`)
      .toPromise()
      .then(res =>
        res as SecurityMeasureType);
  }

  create(entity: SecurityMeasureType): Promise<string> {

    return this.http
      .post(this.url, entity, {responseType: 'text'})
      .toPromise()
      .then(res => res as string);
  }

  update(entity: SecurityMeasureType): Promise<string> {
    const url = `${this.url}/${entity.id}`;
    return this.http
      .put(url, entity, {responseType: 'text'})
      .toPromise()
      .then(res => res as string);
  }

  delete(id: number): Promise<{}> {
    const url = `${this.url}/${id}`;
    return this.http
      .delete(url, {responseType: 'text'})
      .toPromise();
  }

}
