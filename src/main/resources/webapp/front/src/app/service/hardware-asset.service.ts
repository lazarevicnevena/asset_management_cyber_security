import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {HardwareAsset} from '../model/hardware-asset';
import {SecurityGoalValue} from '../model/security-goal-value';

@Injectable()
export class HardwareAssetService {
  private url = '/api/hardware-assets';

  constructor(private http: HttpClient) {
  }

  getAll(): Promise<HardwareAsset[]> {
    return this.http.get(this.url)
      .toPromise()
      .then(res => res as HardwareAsset[]);
  }

  addGoalValueDirectly(uuid: string, goalValue: SecurityGoalValue): Promise<string> {
    return this.http.post(`${this.url}/${uuid}/security-goal-values/from-dialog`, goalValue, {responseType: 'text'})
      .toPromise()
      .then(res =>
        res as string);
  }


  getOne(id: string): Promise<HardwareAsset> {
    return this.http.get(`${this.url}/${id}`)
      .toPromise()
      .then(res =>
        res as HardwareAsset);
  }

  create(entity: HardwareAsset): Promise<string> {

    return this.http
      .post(this.url, entity, {responseType: 'text'})
      .toPromise()
      .then(res => res as string);
  }

  update(entity: HardwareAsset): Promise<string> {
    const url = `${this.url}/${entity.uuid}`;
    return this.http
      .put(url, entity, {responseType: 'text'})
      .toPromise()
      .then(res => res as string);
  }

  delete(id: string): Promise<{}> {
    const url = `${this.url}/${id}`;
    return this.http
      .delete(url, {responseType: 'text'})
      .toPromise();
  }

}
