import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Owner} from '../model/owner';

@Injectable()
export class OwnerService {
  private url = '/api/owners';

  constructor(private http: HttpClient) {
  }

  getAll(): Promise<Owner[]> {
    return this.http.get(this.url)
      .toPromise()
      .then(res => res as Owner[]);
  }

  getOne(id: string): Promise<Owner> {
    return this.http.get(`${this.url}/${id}`)
      .toPromise()
      .then(res =>
        res as Owner);
  }

  create(entity: Owner): Promise<string> {

    return this.http
      .post(this.url, entity, {responseType: 'text'})
      .toPromise()
      .then(res => res as string);
  }

  update(entity: Owner): Promise<string> {
    const url = `${this.url}/${entity.employeeId}`;
    return this.http
      .put(url, entity, {responseType: 'text'})
      .toPromise()
      .then(res => res as string);
  }

  delete(id: string): Promise<{}> {
    const url = `${this.url}/${id}`;
    return this.http
      .delete(url, {responseType: 'text'})
      .toPromise();
  }

}
