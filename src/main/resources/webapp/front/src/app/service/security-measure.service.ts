import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {SecurityMeasure} from '../model/security-measure';

@Injectable()
export class SecurityMeasureService {
  private url = '/api/security-measures';

  constructor(private http: HttpClient) {
  }

  getAll(): Promise<SecurityMeasure[]> {
    return this.http.get(this.url)
      .toPromise()
      .then(res => res as SecurityMeasure[]);
  }

  getOne(id: number): Promise<SecurityMeasure> {
    return this.http.get(`${this.url}/${id}`)
      .toPromise()
      .then(res =>
        res as SecurityMeasure);
  }

  create(entity: SecurityMeasure): Promise<string> {

    return this.http
      .post(this.url, entity, {responseType: 'text'})
      .toPromise()
      .then(res => res as string);
  }

  update(entity: SecurityMeasure): Promise<string> {
    const url = `${this.url}/${entity.id}`;
    return this.http
      .put(url, entity, {responseType: 'text'})
      .toPromise()
      .then(res => res as string);
  }

  delete(id: number): Promise<{}> {
    const url = `${this.url}/${id}`;
    return this.http
      .delete(url, {responseType: 'text'})
      .toPromise();
  }

}
