import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {FormlyMaterialModule} from '@ngx-formly/material';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {FormlyModule} from '@ngx-formly/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {LocationService} from './service/location.service';
import {HttpClientModule} from '@angular/common/http';
import { LocationListComponent } from './components/location/location-list/location-list.component';
import {MomentumTableModule} from 'momentum-table';
import {
  MatButtonModule, MatCardModule, MatCheckboxModule, MatDialogModule, MatFormFieldModule, MatIconModule, MatInputModule,
  MatMenuModule,
  MatNativeDateModule, MatSelectModule, MatSlideToggleModule,
  MatSnackBarModule,
  MatTableModule, MatToolbarModule
} from '@angular/material';
import { OwnerListComponent } from './components/owner/owner-list/owner-list.component';
import {OwnerService} from './service/owner.service';
import {AssetNetworkService} from './service/asset-network.service';
import {AssetService} from './service/asset.service';
import {HardwareAssetService} from './service/hardware-asset.service';
import {SoftwareAssetService} from './service/software-asset.service';
import {VirtualAssetService} from './service/virtual-asset.service';
import {DataAssetService} from './service/data-asset.service';
import {SecurityMeasureService} from './service/security-measure.service';
import {SecurityMeasureTypeService} from './service/security-measure-type.service';
import {SecurityGoalService} from './service/security-goal.service';
import {SecurityGoalValueService} from './service/security-goal-value.service';
import { AssetNetworkListComponent } from './components/asset-network/asset-network-list/asset-network-list.component';
import { DataAssetListComponent } from './components/asset/data-asset/data-asset-list/data-asset-list.component';
import { HardwareAssetListComponent } from './components/asset/hardware-asset/hardware-asset-list/hardware-asset-list.component';
import { SoftwareAssetListComponent } from './components/asset/software-asset/software-asset-list/software-asset-list.component';
import { VirtualAssetListComponent } from './components/asset/virtual-asset/virtual-asset-list/virtual-asset-list.component';
import { AssetListComponent } from './components/asset/asset/asset-list/asset-list.component';
import { SecurityGoalListComponent } from './components/security-goal/security-goal-list/security-goal-list.component';
import { SecurityGoalValueListComponent } from './components/security-goal-value/security-goal-value-list/security-goal-value-list.component';
import { SecurityMeasureListComponent } from './components/security-measure/security-measure-list/security-measure-list.component';
import { SecurityMeasureTypeListComponent } from './components/security-measure-type/security-measure-type-list/security-measure-type-list.component';
import {FormlyMatDatepickerModule} from '@ngx-formly/material/datepicker';
import { HardwareAssetDialogComponent } from './components/asset/software-asset/hardware-asset-dialog/hardware-asset-dialog.component';
import { SecurityMeasureDialogComponent } from './components/security-goal-value/security-measure-dialog/security-measure-dialog.component';
import { SecurityGoalValuesDialogComponent } from './components/asset/asset/security-goal-values-dialog/security-goal-values-dialog.component';
import { CustomReportListComponent } from './components/report/custom/custom-report-list/custom-report-list.component';
import {CustomReportService} from './service/custom-report.service';
import { CreationDateReportListComponent } from './components/report/creation-date/creation-date-report-list/creation-date-report-list.component';
import { SecurityReportListComponent } from './components/report/security/security-report-list/security-report-list.component';
import {CreationDateReportService} from './service/creation-date-report.service';
import {SecurityReportService} from './service/security-report.service';
import { ResultDialogComponent } from './components/report/result-dialog/result-dialog.component';
import {NgxMatSelectSearchModule} from 'ngx-mat-select-search';
import { SoftwareAssetDialogComponent } from './components/asset-network/asset-dialog/software-asset-dialog/software-asset-dialog.component';
import { VirtualAssetDialogComponent } from './components/asset-network/asset-dialog/virtual-asset-dialog/virtual-asset-dialog.component';
import { DataAssetDialogComponent } from './components/asset-network/asset-dialog/data-asset-dialog/data-asset-dialog.component';
import { OwnerReportListComponent } from './components/report/owner/owner-report-list/owner-report-list.component';
import { NetworkReportListComponent } from './components/report/network/network-report-list/network-report-list.component';
import {OwnerReportService} from './service/owner-report.service';
import {NetworkReportService} from './service/network-report.service';

@NgModule({
  declarations: [
    AppComponent,
    LocationListComponent,
    OwnerListComponent,
    AssetNetworkListComponent,
    DataAssetListComponent,
    HardwareAssetListComponent,
    SoftwareAssetListComponent,
    VirtualAssetListComponent,
    AssetListComponent,
    SecurityGoalListComponent,
    SecurityGoalValueListComponent,
    SecurityMeasureListComponent,
    SecurityMeasureTypeListComponent,
    HardwareAssetDialogComponent,
    SecurityMeasureDialogComponent,
    SecurityGoalValuesDialogComponent,
    CustomReportListComponent,
    CreationDateReportListComponent,
    SecurityReportListComponent,
    ResultDialogComponent,
    SoftwareAssetDialogComponent,
    VirtualAssetDialogComponent,
    DataAssetDialogComponent,
    OwnerReportListComponent,
    NetworkReportListComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    FormlyMaterialModule,
    FormlyMatDatepickerModule,
    ReactiveFormsModule,
    FormsModule,
    MomentumTableModule,
    MatIconModule,
    MatButtonModule,
    MatTableModule,
    MatMenuModule,
    MatNativeDateModule,
    MatDialogModule,
    MatSnackBarModule,
    MatSelectModule,
    MatInputModule,
    MatCheckboxModule,
    MatFormFieldModule,
    NgxMatSelectSearchModule,
    MatSlideToggleModule,
    MatCardModule,
    MatToolbarModule,
    FormlyModule.forRoot()
  ],
  providers: [
    LocationService,
    OwnerService,
    AssetService,
    HardwareAssetService,
    SoftwareAssetService,
    VirtualAssetService,
    DataAssetService,
    AssetNetworkService,
    SecurityGoalService,
    SecurityGoalValueService,
    SecurityMeasureService,
    SecurityMeasureTypeService,
    CustomReportService,
    CreationDateReportService,
    SecurityReportService,
    OwnerReportService,
    NetworkReportService
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    HardwareAssetDialogComponent,
    SoftwareAssetDialogComponent,
    VirtualAssetDialogComponent,
    DataAssetDialogComponent,
    SecurityMeasureDialogComponent,
    SecurityGoalValuesDialogComponent,
    ResultDialogComponent
  ]
})
export class AppModule { }
