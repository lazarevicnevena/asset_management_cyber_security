import {Asset} from './asset';

export class DataAsset extends Asset {
  id: number;

  public constructor() {
    super();
    this.id = 0;
  }
}
