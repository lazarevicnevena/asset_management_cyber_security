import {SecurityMeasureType} from './security-measure-type';

export class SecurityMeasure {
  id: number;
  name: string;
  description: string;
  type: SecurityMeasureType;

  public constructor() {
    this.id = 0;
    this.name = '';
    this.description = '';
    this.type = new SecurityMeasureType();
  }
}
