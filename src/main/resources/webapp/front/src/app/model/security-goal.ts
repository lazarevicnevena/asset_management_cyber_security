export class SecurityGoal {
  id: number;
  name: string;

  public constructor() {
    this.id = 0;
    this.name = '';
  }
}
