import {AssetLocation} from './location';
import {Asset} from './asset';

export class HardwareAsset extends Asset {
  id: number;
  hostName: string;
  macAddress: string;
  ipAddress: string;
  lastUpdated: Date;
  serialNumber: string;
  model: string;
  operatingSystem: string;
  location: AssetLocation;

  public constructor() {
    super();
    this.id = 0;
    this.hostName = '';
    this.macAddress = '';
    this.ipAddress = '';
    this.lastUpdated = new Date();
    this.serialNumber = '';
    this.model = '';
    this.operatingSystem = '';
    this.location = new AssetLocation();
  }
}
