export class CustomReport {
  id: number;
  name: string;
  description: string;
  subtype: string;
  type: string;
  building: string;
  room: string;
  owner: string;
  securityGoalValue: string;
  securityMeasure: string;
  public constructor() {
    this.id = 0;
    this.name = '';
    this.description = '';
    this.subtype = 'ANY';
    this.type = 'ANY';
    this.building = '';
    this.room = '';
    this.owner = '';
    this.securityGoalValue = '';
    this.securityMeasure = '';
  }
}
