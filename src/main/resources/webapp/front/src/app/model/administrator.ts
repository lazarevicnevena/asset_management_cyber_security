export class Administrator {
  id: number;
  name: string;
  username: string;
  password: string;
  public constructor() {
    this.id = 0;
    this.name = '';
    this.username = '';
    this.password = '';
  }
}
