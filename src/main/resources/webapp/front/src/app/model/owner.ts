export class Owner {
  id: number;
  firstName: string;
  middleName: string;
  lastName: string;
  phoneNumber: string;
  employeeId: string;
  public constructor() {
    this.id = 0;
    this.firstName = '';
    this.middleName = '';
    this.lastName = '';
    this.phoneNumber = '';
    this.employeeId = '';
  }
}
