import {MeasureType} from './measure-type';

export class SecurityMeasureType {
  id: number;
  name: string;
  description: string;
  type: MeasureType;

  public constructor() {
    this.id = 0;
    this.name = '';
    this.description = '';
    this.type = MeasureType.ANTIVIRUS;
  }
}
