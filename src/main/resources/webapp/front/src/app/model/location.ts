export class AssetLocation {
  id: number;
  building: string;
  room: string;
  rack: number;
  public constructor() {
    this.id = 0;
    this.building = '';
    this.room = '';
    this.rack = 0;
  }
}
