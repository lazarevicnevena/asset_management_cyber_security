import {HardwareAsset} from './hardware-asset';
import {Asset} from './asset';

export class SoftwareAsset extends Asset {
  id: number;
  hardwareAssets: HardwareAsset[];

  public constructor() {
    super();
    this.id = 0;
    this.hardwareAssets = [];
  }
}
