export class CreationDateReport {
  id: number;
  name: string;
  description: string;
  subtype: string;
  type: string;
  createdFrom: Date;
  createdTo: Date;
  public constructor() {
    this.id = 0;
    this.name = '';
    this.description = '';
    this.subtype = 'ANY';
    this.type = 'ANY';
    this.createdFrom = new Date();
    this.createdTo = new Date();
  }
}
