export class SecurityReport {
  id: number;
  name: string;
  securityGoalValues: string;
  securityMeasures: string;
  public constructor() {
    this.id = 0;
    this.name = '';
    this.securityGoalValues = '';
    this.securityMeasures = '';
  }
}
