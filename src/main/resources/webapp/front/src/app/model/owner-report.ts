export class OwnerReport {
  id: number;
  firstName: string;
  lastName: string;
  middleName: string;
  employeeNr: string;
  phoneNumber: string;

  public constructor() {
    this.id = 0;
    this.firstName = '';
    this.lastName = '';
    this.middleName = '';
    this.employeeNr = '';
    this.phoneNumber = '';
  }
}
