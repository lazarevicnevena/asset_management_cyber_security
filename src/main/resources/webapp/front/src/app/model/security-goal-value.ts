import {SecurityMeasure} from './security-measure';
import {SecurityGoal} from './security-goal';

export class SecurityGoalValue {
  id: number;
  name: string;
  description: string;
  securityMeasures: SecurityMeasure[];
  securityGoal: SecurityGoal;

  public constructor() {
    this.id = 0;
    this.name = '';
    this.description = '';
    this.securityMeasures = [];
    this.securityGoal = new SecurityGoal();
  }
}
