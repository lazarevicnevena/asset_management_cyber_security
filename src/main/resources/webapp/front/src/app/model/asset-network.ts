import {Asset} from './asset';

export class AssetNetwork {
  id: number;
  name: string;
  description: string;
  assets: Asset[];

  public constructor() {
    this.id = 0;
    this.name = '';
    this.description = '';
    this.assets = [];
  }
}
