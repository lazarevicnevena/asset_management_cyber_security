import {AssetType} from './asset-type';
import {Owner} from './owner';
import {SecurityGoalValue} from './security-goal-value';

export abstract class Asset {
  id: number;
  name: string;
  description: string;
  state: string;
  uuid: string;
  created: Date;
  type: AssetType;
  owner: Owner;
  dtype: string;
  securityGoalValues: SecurityGoalValue[];

  public constructor() {
    this.id = 0;
    this.name = '';
    this.description = '';
    this.state = '';
    this.uuid = '';
    this.created = new Date();
    this.type = AssetType.WORK_STATION;
    this.owner = new Owner();
    this.dtype = '';
    this.securityGoalValues = [];
  }
}
