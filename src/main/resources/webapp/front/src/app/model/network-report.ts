export class NetworkReport {
  id: number;
  name: string;
  description: string;
  minAssetNr: number;
  maxAssetNr: number;

  public constructor() {
    this.id = 0;
    this.name = '';
    this.description = '';
    this.minAssetNr = 0;
    this.maxAssetNr = 0;
  }
}
