import {HardwareAsset} from './hardware-asset';
import {Asset} from './asset';

export class VirtualAsset extends Asset {
  id: number;
  macAddress: string;
  ipAddress: string;
  lastUpdated: Date;
  operatingSystem: string;
  hardwareAsset: HardwareAsset;


  public constructor() {
    super();
    this.id = 0;
    this.macAddress = '';
    this.ipAddress = '';
    this.lastUpdated = new Date();
    this.operatingSystem = '';
    this.hardwareAsset = new HardwareAsset();
  }
}
