import {Component, OnDestroy, OnInit} from '@angular/core';
import {CreationDateReport} from '../../../../model/creation-date-report';
import {CreationDateReportService} from '../../../../service/creation-date-report.service';
import {MatDialog, MatDialogConfig, MatSnackBar} from '@angular/material';
import {ResultDialogComponent} from '../../result-dialog/result-dialog.component';
import {FormlyFieldConfig} from '@ngx-formly/core';
import {FormControl, FormGroup} from '@angular/forms';
import {ReplaySubject, Subject} from 'rxjs';
import {takeUntil} from 'rxjs/internal/operators';
import {AssetType} from '../../../../model/asset-type';

@Component({
  selector: 'app-creation-date-report-list',
  templateUrl: './creation-date-report-list.component.html',
  styleUrls: ['./creation-date-report-list.component.css']
})
export class CreationDateReportListComponent implements OnInit, OnDestroy {

  entities: CreationDateReport[];
  form = new FormGroup({});
  selectedRows: CreationDateReport[];
  entity: CreationDateReport;
  fields: FormlyFieldConfig[];
  mode = '';
  selectedSubType: number;
  selectListSubType = [];
  selectFilterCtrlSubType: FormControl = new FormControl();
  filteredSelectListSubType: ReplaySubject<string[]> = new ReplaySubject<string[]>(1);
  onDestroyVar = new Subject<void>();
  selectListType = [];
  selectedType: number;
  selectFilterCtrlType: FormControl = new FormControl();
  filteredSelectListType: ReplaySubject<AssetType[]> = new ReplaySubject<AssetType[]>(1);

  constructor( private creationDateReportService: CreationDateReportService,
               private dialog: MatDialog,
               private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.mode = 'CREATE';
    this.setFormFields();
    this.entity = new CreationDateReport();
    this.setAssetTypes();
    this.filteredSelectListType.next(this.selectListType.slice());
    this.selectFilterCtrlType.valueChanges
      .pipe(takeUntil(this.onDestroyVar))
      .subscribe(() => {
        this.filterSelectType();
      });
    this.setSubTypes();
    this.filteredSelectListSubType.next(this.selectListSubType.slice());
    this.selectFilterCtrlSubType.valueChanges
      .pipe(takeUntil(this.onDestroyVar))
      .subscribe(() => {
        this.filterSelectSubType();
      });
    this.getAll();
  }

  ngOnDestroy() {
    this.onDestroyVar.next();
    this.onDestroyVar.complete();
  }

  setAssetTypes() {
    for (const eMember in AssetType) {
      if (typeof AssetType[eMember] !== 'number') {
        this.selectListType.push(AssetType[eMember]);
      }
    }
  }

  setSubTypes() {
    this.selectListSubType.push(
      'ANY',
      'HARDWARE',
      'SOFTWARE',
      'VIRTUAL',
      'DATA'
    );
  }

  getAll() {
    this.entities = [];
    this.creationDateReportService.getAll().then(
      res => {
        this.entities = res;
      }
    );
  }

  delete() {
    if (this.mode !== 'CREATE') {
      this.entity = this.selectedRows[0];
      this.creationDateReportService.delete(this.entity.id)
        .then(res => {
          this.getAll();
        });
    }
  }


  edit() {
    this.entity = this.selectedRows[0];
    this.mode = 'EDIT';
    this.setFormFields();
    if (this.entity.type !== null) {
      this.setCheckedValueType(AssetType[this.entity.type]);
    }
    if (this.entity.subtype !== '') {
      this.setCheckedValueSubType(this.entity.subtype);
    }
  }

  create() {
    this.mode = 'CREATE';
    this.entity = new CreationDateReport();
    this.selectedRows = [];
    this.selectedRows.push(this.entity);
    this.setCheckedValueType(null);
    this.setCheckedValueSubType('');
    this.setFormFields();
  }

  submit() {
    if (this.mode === 'EDIT') {
      this.creationDateReportService.update(this.entity).then(
        res => {
          this.close();
          this.getAll();
        }
      );
    } else if (this.mode === 'CREATE') {
      this.creationDateReportService.create(this.entity).then(
        res => {
          this.close();
          this.getAll();
        }
      );
    }
  }

  close() {
    this.selectedRows = [];
    this.entity = new CreationDateReport();
    this.mode = 'CREATE';
  }

  filter() {
    if (this.selectedRows.length > 0) {
      this.entity = this.selectedRows[0];
      const id = this.entity.id;
      console.log('Filtering for report: ' + id);
      this.creationDateReportService.filter(id).then(
        res => {
          const dialogConfig = new MatDialogConfig();

          dialogConfig.disableClose = true;
          dialogConfig.autoFocus = true;
          dialogConfig.data = {
            assets: res
          };

          this.dialog.open(ResultDialogComponent, dialogConfig);
        }
      ).catch(
        res => {
          this.openAlert('No asset satisfies criteria for this report!');
        }
      );
    }
  }

  openAlert(message: string) {
    this.snackBar.open(message, 'x', {
      verticalPosition: 'top',
      duration: 2000,
      panelClass: ['snackbar-class']
    });
  }

  setFormFields() {
    this.fields = [
      {
        key: 'name',
        type: 'input',
        templateOptions: {
          label: 'Name',
          placeholder: 'Enter asset name'}
      },
      {
        key: 'description',
        type: 'input',
        templateOptions: {
          label: 'Description',
          placeholder: 'Enter asset description'}
      },
      {
        key: 'createdFrom',
        type: 'datepicker',
        templateOptions: {
          label: 'Created from date',
          placeholder: 'Enter creation from date',
          required: false }
      },
      {
        key: 'createdTo',
        type: 'datepicker',
        templateOptions: {
          label: 'Created to date',
          placeholder: 'Enter creation to date',
          required: false }
      }
    ];
  }

  setCheckedValueSubType(subtype: string) {
    if (subtype !== '') {
      this.selectListSubType.forEach((item, index) => {
        if (item === subtype) {
          this.selectedSubType = index;
          this.entity.subtype = item;
          return false;
        }
      });
    } else {
      this.selectedSubType = 0;
      this.entity.subtype = this.selectListSubType[0];
    }
  }

  setCheckedValueType(type: AssetType) {
    if (type !== null) {
      this.selectListType.forEach((item, index) => {
        if (item === type) {
          this.selectedType = index;
          this.entity.type = item;
          return false;
        }
      });
    } else {
      this.selectedType = 0;
      this.entity.type = this.selectListType[0];
    }
  }

  protected filterSelectType() {
    if (!this.selectListType) {
      return;
    }
    let search = this.selectFilterCtrlType.value;
    if (!search) {
      this.filteredSelectListType.next(this.selectListType.slice());
      return;
    } else {
      search = search.toLowerCase();
    }

    this.filteredSelectListType.next(
      this.selectListType.filter(entity => entity.toString().toLowerCase().indexOf(search) > -1)
    );
  }

  protected filterSelectSubType() {
    if (!this.selectListSubType) {
      return;
    }
    let search = this.selectFilterCtrlSubType.value;
    if (!search) {
      this.filteredSelectListSubType.next(this.selectListSubType.slice());
      return;
    } else {
      search = search.toLowerCase();
    }

    this.filteredSelectListSubType.next(
      this.selectListSubType.filter(entity => entity.toString().toLowerCase().indexOf(search) > -1)
    );
  }

  rowSelected() {
    if (this.selectedRows.length === 0) {
      this.entity = new CreationDateReport();
      this.mode = 'CREATE';
    } else {
      this.edit();
    }
  }

}
