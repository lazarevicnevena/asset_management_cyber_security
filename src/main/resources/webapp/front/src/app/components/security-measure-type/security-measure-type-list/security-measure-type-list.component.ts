import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import {SecurityMeasureType} from '../../../model/security-measure-type';
import {SecurityMeasureTypeService} from '../../../service/security-measure-type.service';
import {Router} from '@angular/router';
import {FormlyFieldConfig} from '@ngx-formly/core';
import {FormControl, FormGroup} from '@angular/forms';
import {MeasureType} from '../../../model/measure-type';
import {ReplaySubject, Subject} from 'rxjs';
import {MatSelect} from '@angular/material';
import {takeUntil} from 'rxjs/internal/operators';

@Component({
  selector: 'app-security-measure-type-list',
  templateUrl: './security-measure-type-list.component.html',
  styleUrls: ['./security-measure-type-list.component.css']
})
export class SecurityMeasureTypeListComponent implements OnInit, OnDestroy {

  entities: SecurityMeasureType[];
  form = new FormGroup({});
  selectedRows: SecurityMeasureType[];
  entity: SecurityMeasureType;
  fields: FormlyFieldConfig[];
  mode = '';
  selectList = [];
  selected: number;
  selectFilterCtrl: FormControl = new FormControl();
  filteredSelectList: ReplaySubject<MeasureType[]> = new ReplaySubject<MeasureType[]>(1);
  @ViewChild('singleSelect') singleSelect: MatSelect;
  onDestroyVar = new Subject<void>();

  constructor(private securityMeasureTypeService: SecurityMeasureTypeService,
              private router: Router) {
  }

  ngOnInit() {
    this.mode = 'CREATE';
    this.setFormFields();
    this.entity = new SecurityMeasureType();
    this.selected = -1;
    this.setMeasureTypes();
    this.filteredSelectList.next(this.selectList.slice());

    // listen for search field value changes
    this.selectFilterCtrl.valueChanges
      .pipe(takeUntil(this.onDestroyVar))
      .subscribe(() => {
        this.filterSelect();
      });
    this.getAll();
  }

  getAll() {
    this.entities = [];
    this.securityMeasureTypeService.getAll().then(
      res => {
        this.entities = res;
      }
    );
  }
  setMeasureTypes() {
    for (const eMember in MeasureType) {
      if (typeof MeasureType[eMember] !== 'number') {
        this.selectList.push(MeasureType[eMember]);
      }
    }
  }

  ngOnDestroy() {
    this.onDestroyVar.next();
    this.onDestroyVar.complete();
  }

  delete() {
    if (this.mode !== 'CREATE') {
      this.entity = this.selectedRows[0];
      console.log('Deleting security measure type with id: ' + this.entity.id);
      this.securityMeasureTypeService.delete(this.entity.id)
        .then(res => {
          this.getAll();
        });
    }
  }


  edit() {
    this.entity = this.selectedRows[0];
    console.log('Editing security measure type with id: ' + this.entity.id);
    this.mode = 'EDIT';
    if (this.entity.type !== null) {
      this.setCheckedValue(this.entity.type);
    }
    this.setFormFields();
  }

  create() {
    this.mode = 'CREATE';
    this.entity = new SecurityMeasureType();
    this.selectedRows = [];
    this.setCheckedValue(null);
    this.setFormFields();
  }

  submit() {
    if (this.mode === 'EDIT') {
      this.securityMeasureTypeService.update(this.entity).then(
        res => {
          this.close();
          this.getAll();
        }
      );
    } else if (this.mode === 'CREATE') {
      this.securityMeasureTypeService.create(this.entity).then(
        res => {
          this.close();
          this.getAll();
        }
      );
    }
  }

  close() {
    this.selectedRows = [];
    this.entity = new SecurityMeasureType();
    this.mode = 'CREATE';
  }

  setFormFields() {
    this.fields = [
      {
        key: 'name',
        type: 'input',
        templateOptions: {
          label: 'Name',
          placeholder: 'Enter name',
          required: true
        }
      },
      {
        key: 'description',
        type: 'input',
        templateOptions: {
          label: 'Description',
          placeholder: 'Enter description',
          required: true
        }
      }
    ];
  }

  setCheckedValue(type: MeasureType) {
    if (type !== null) {
      this.selectList.forEach((item, index) => {
        if (item === type) {
          this.selected = index;
          this.entity.type = item;
          return false;
        }
      });
    } else {
      this.selected = 0;
      this.entity.type = this.selectList[0];
    }
  }

  protected filterSelect() {
    if (!this.selectList) {
      return;
    }
    // get the search keyword
    let search = this.selectFilterCtrl.value;
    if (!search) {
      this.filteredSelectList.next(this.selectList.slice());
      return;
    } else {
      search = search.toLowerCase();
    }

    this.filteredSelectList.next(
      this.selectList.filter(entity => entity.toString().toLowerCase().indexOf(search) > -1)
    );
  }

  rowSelected() {
    if (this.selectedRows.length === 0) {
      this.entity = new SecurityMeasureType();
      this.mode = 'CREATE';
    } else {
      this.edit();
    }
  }
}
