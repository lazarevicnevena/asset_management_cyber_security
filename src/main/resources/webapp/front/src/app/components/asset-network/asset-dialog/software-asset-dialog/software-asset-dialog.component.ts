import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {AssetType} from '../../../../model/asset-type';
import {HardwareAssetService} from '../../../../service/hardware-asset.service';
import {OwnerService} from '../../../../service/owner.service';
import {Owner} from '../../../../model/owner';
import {ReplaySubject, Subject} from 'rxjs';
import {takeUntil} from 'rxjs/internal/operators';
import {HardwareAsset} from '../../../../model/hardware-asset';
import {SecurityGoalValue} from '../../../../model/security-goal-value';
import {SecurityGoalValueService} from '../../../../service/security-goal-value.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {FormlyFieldConfig} from '@ngx-formly/core';
import {FormControl, FormGroup} from '@angular/forms';
import {AssetNetworkService} from '../../../../service/asset-network.service';
import {SoftwareAsset} from '../../../../model/software-asset';

@Component({
  selector: 'app-software-asset-dialog',
  templateUrl: './software-asset-dialog.component.html',
  styleUrls: ['./software-asset-dialog.component.css']
})
export class SoftwareAssetDialogComponent implements OnInit, OnDestroy {

  networkId: number;
  name: string;
  flag = false;

  form = new FormGroup({});
  entity: SoftwareAsset;
  fields: FormlyFieldConfig[];
  selectList: Owner[];
  selected: number;
  selectFilterCtrl: FormControl = new FormControl();
  filteredSelectList: ReplaySubject<Owner[]> = new ReplaySubject<Owner[]>(1);
  onDestroyVar = new Subject<void>();
  selectListType = [];
  selectedType: number;
  selectFilterCtrlType: FormControl = new FormControl();
  filteredSelectListType: ReplaySubject<AssetType[]> = new ReplaySubject<AssetType[]>(1);
  showSecValues: boolean;
  // for Security goal values
  multipleCtrl: FormControl = new FormControl();
  selectListSecurityValues: SecurityGoalValue[];
  selectFilterCtrlSecurityValues: FormControl = new FormControl();
  filteredSelectListSecurityValues: ReplaySubject<SecurityGoalValue[]> = new ReplaySubject<SecurityGoalValue[]>(1);
// for Security hardwares
  multipleCtrlHardwares: FormControl = new FormControl();
  selectListHardwares: HardwareAsset[];
  selectFilterCtrlHardwares: FormControl = new FormControl();
  filteredSelectListHardwares: ReplaySubject<HardwareAsset[]> = new ReplaySubject<HardwareAsset[]>(1);


  constructor( private hardwareService: HardwareAssetService,
               private securityGoalValueService: SecurityGoalValueService,
               private ownerService: OwnerService,
               private dialogRef: MatDialogRef<SoftwareAssetDialogComponent>,
               private assetNetworkService: AssetNetworkService,
               @Inject(MAT_DIALOG_DATA) data) {
    this.name = data.name;
    this.networkId = data.networkId;
  }

  ngOnInit() {
    this.setFormFields();
    this.entity = new SoftwareAsset();
    this.selectListSecurityValues = [];
    this.selectListHardwares = [];
    this.ownerService.getAll().then(
      res => {
        this.selectList = res;
        this.filteredSelectList.next(this.selectList.slice());
        this.selectFilterCtrl.valueChanges
          .pipe(takeUntil(this.onDestroyVar))
          .subscribe(() => {
            this.filterSelect();
          });
        this.setAssetTypes();
        this.filteredSelectListType.next(this.selectListType.slice());
        this.selectFilterCtrlType.valueChanges
          .pipe(takeUntil(this.onDestroyVar))
          .subscribe(() => {
            this.filterSelectType();
          });
        this.securityGoalValueService.getAll().then(
          resSec => {
            this.selectListSecurityValues = resSec;
            this.filteredSelectListSecurityValues.next(this.selectListSecurityValues.slice());
            this.selectFilterCtrlSecurityValues.valueChanges
              .pipe(takeUntil(this.onDestroyVar))
              .subscribe(() => {
                this.filterSelectSecurityValues();
              });
            this.hardwareService.getAll().then(
              resH => {
                this.selectListHardwares = resH;
                this.filteredSelectListHardwares.next(this.selectListHardwares.slice());
                this.flag = true;
                this.selectFilterCtrlHardwares.valueChanges
                  .pipe(takeUntil(this.onDestroyVar))
                  .subscribe(() => {
                    this.filterSelectHardwares();
                  });
              }
            );
          }
        );
      }
    );
  }

  ngOnDestroy() {
    this.onDestroyVar.next();
    this.onDestroyVar.complete();
  }

  setAssetTypes() {
    for (const eMember in AssetType) {
      if (typeof AssetType[eMember] !== 'number') {
        this.selectListType.push(AssetType[eMember]);
      }
    }
  }

  setFormFields() {
    this.fields = [
      {
        key: 'name',
        type: 'input',
        templateOptions: {
          label: 'Name',
          placeholder: 'Enter name',
          required: true }
      },
      {
        key: 'description',
        type: 'input',
        templateOptions: {
          label: 'Description',
          placeholder: 'Enter description',
          required: true }
      }
    ];
  }

  protected filterSelect() {
    if (!this.selectList) {
      return;
    }
    // get the search keyword
    let search = this.selectFilterCtrl.value;
    if (!search) {
      this.filteredSelectList.next(this.selectList.slice());
      return;
    } else {
      search = search.toLowerCase();
    }

    this.filteredSelectList.next(
      this.selectList.filter(entity => (entity.firstName + ' ' + entity.lastName).toLowerCase().indexOf(search) > -1 )
    );
  }

  protected filterSelectType() {
    if (!this.selectListType) {
      return;
    }
    let search = this.selectFilterCtrlType.value;
    if (!search) {
      this.filteredSelectListType.next(this.selectListType.slice());
      return;
    } else {
      search = search.toLowerCase();
    }

    this.filteredSelectListType.next(
      this.selectListType.filter(entity => entity.toString().toLowerCase().indexOf(search) > -1)
    );
  }

  protected filterSelectSecurityValues() {
    if (!this.selectListSecurityValues) {
      return;
    }
    let search = this.selectFilterCtrlSecurityValues.value;
    if (!search) {
      this.filteredSelectListSecurityValues.next(this.selectListSecurityValues.slice());
      return;
    } else {
      search = search.toLowerCase();
    }

    this.filteredSelectListSecurityValues.next(
      this.selectListSecurityValues.filter(entity => entity.name.toString().toLowerCase().indexOf(search) > -1)
    );
  }

  protected filterSelectHardwares() {
    if (!this.selectListHardwares) {
      return;
    }
    let search = this.selectFilterCtrlHardwares.value;
    if (!search) {
      this.filteredSelectListHardwares.next(this.selectListHardwares.slice());
      return;
    } else {
      search = search.toLowerCase();
    }

    this.filteredSelectListHardwares.next(
      this.selectListHardwares.filter(entity => entity.name.toString().toLowerCase().indexOf(search) > -1)
    );
  }

  compareFunc(c1: SecurityGoalValue, c2: SecurityGoalValue): boolean {
    return c1 && c2 ? c1.id === c2.id : c1 === c2;
  }

  compareFuncHardwares(c1: HardwareAsset, c2: HardwareAsset): boolean {
    return c1 && c2 ? c1.uuid === c2.uuid : c1 === c2;
  }

  close() {
    this.dialogRef.close();
  }

  submit() {
    this.entity.securityGoalValues = this.multipleCtrl.value;
    this.entity.hardwareAssets = this.multipleCtrlHardwares.value;
    this.assetNetworkService.addSAssetsDirectly(this.networkId, this.entity).then(
      res => {
        this.close();
      }
    );
  }

}
