import { Component, Inject, OnInit } from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Asset} from '../../../model/asset';
import {AssetNetworkService} from '../../../service/asset-network.service';

@Component({
  selector: 'app-result-dialog',
  templateUrl: './result-dialog.component.html',
  styleUrls: ['./result-dialog.component.css']
})
export class ResultDialogComponent implements OnInit {

  entities: Asset[] = [];

  constructor( private assetNetworkService: AssetNetworkService,
               private dialogRef: MatDialogRef<ResultDialogComponent>,
               @Inject(MAT_DIALOG_DATA) data) {
    this.entities = data.assets;
  }

  ngOnInit() {
  }

  close() {
    this.dialogRef.close();
  }

}
