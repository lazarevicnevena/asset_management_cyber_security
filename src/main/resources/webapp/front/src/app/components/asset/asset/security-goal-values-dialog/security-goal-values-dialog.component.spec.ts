import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SecurityGoalValuesDialogComponent } from './security-goal-values-dialog.component';

describe('SecurityGoalValuesDialogComponent', () => {
  let component: SecurityGoalValuesDialogComponent;
  let fixture: ComponentFixture<SecurityGoalValuesDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SecurityGoalValuesDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SecurityGoalValuesDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
