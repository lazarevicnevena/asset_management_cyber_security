import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {SoftwareAsset} from '../../../../model/software-asset';
import {SoftwareAssetService} from '../../../../service/software-asset.service';
import {HardwareAssetDialogComponent} from '../hardware-asset-dialog/hardware-asset-dialog.component';
import {MatDialog, MatDialogConfig, MatSnackBar} from '@angular/material';
import {FormlyFieldConfig} from '@ngx-formly/core';
import {FormControl, FormGroup} from '@angular/forms';
import {AssetType} from '../../../../model/asset-type';
import {OwnerService} from '../../../../service/owner.service';
import {Owner} from '../../../../model/owner';
import {ReplaySubject, Subject} from 'rxjs';
import {takeUntil} from 'rxjs/internal/operators';
import {SecurityGoalValue} from '../../../../model/security-goal-value';
import {SecurityGoalValueService} from '../../../../service/security-goal-value.service';
import {HardwareAssetService} from '../../../../service/hardware-asset.service';
import {HardwareAsset} from '../../../../model/hardware-asset';
import {SecurityGoalValuesDialogComponent} from '../../asset/security-goal-values-dialog/security-goal-values-dialog.component';

@Component({
  selector: 'app-software-asset-list',
  templateUrl: './software-asset-list.component.html',
  styleUrls: ['./software-asset-list.component.css']
})
export class SoftwareAssetListComponent implements OnInit, OnDestroy {

  entities: SoftwareAsset[];
  form = new FormGroup({});
  selectedRows: SoftwareAsset[] = [];
  entity: SoftwareAsset;
  fields: FormlyFieldConfig[];
  mode = '';
  selectList: Owner[];
  selected: number;
  selectFilterCtrl: FormControl = new FormControl();
  filteredSelectList: ReplaySubject<Owner[]> = new ReplaySubject<Owner[]>(1);
  onDestroyVar = new Subject<void>();
  selectListType = [];
  selectedType: number;
  selectFilterCtrlType: FormControl = new FormControl();
  filteredSelectListType: ReplaySubject<AssetType[]> = new ReplaySubject<AssetType[]>(1);
  showSecValues: boolean;
  // for Security goal values
  multipleCtrl: FormControl = new FormControl();
  selectListSecurityValues: SecurityGoalValue[];
  selectFilterCtrlSecurityValues: FormControl = new FormControl();
  filteredSelectListSecurityValues: ReplaySubject<SecurityGoalValue[]> = new ReplaySubject<SecurityGoalValue[]>(1);
// for Security hardwares
  multipleCtrlHardwares: FormControl = new FormControl();
  selectListHardwares: HardwareAsset[];
  selectFilterCtrlHardwares: FormControl = new FormControl();
  filteredSelectListHardwares: ReplaySubject<HardwareAsset[]> = new ReplaySubject<HardwareAsset[]>(1);


  constructor( private softwareAssetService: SoftwareAssetService,
               private securityGoalValueService: SecurityGoalValueService,
               private hardwareService: HardwareAssetService,
               private ownerService: OwnerService,
               private snackBar: MatSnackBar,
               private dialog: MatDialog,
               private router: Router) { }

  ngOnInit() {
    this.mode = 'CREATE';
    this.setFormFields();
    this.showSecValues = true;
    this.entity = new SoftwareAsset();
    this.selectListSecurityValues = [];
    this.selectListHardwares = [];
    this.ownerService.getAll().then(
      res => {
        this.selectList = res;
        this.filteredSelectList.next(this.selectList.slice());
        this.selectFilterCtrl.valueChanges
          .pipe(takeUntil(this.onDestroyVar))
          .subscribe(() => {
            this.filterSelect();
          });
        this.setAssetTypes();
        this.filteredSelectListType.next(this.selectListType.slice());
        this.selectFilterCtrlType.valueChanges
          .pipe(takeUntil(this.onDestroyVar))
          .subscribe(() => {
            this.filterSelectType();
          });
        this.securityGoalValueService.getAll().then(
          resSec => {
            this.selectListSecurityValues = resSec;
            this.filteredSelectListSecurityValues.next(this.selectListSecurityValues.slice());
            this.selectFilterCtrlSecurityValues.valueChanges
              .pipe(takeUntil(this.onDestroyVar))
              .subscribe(() => {
                this.filterSelectSecurityValues();
              });
            this.hardwareService.getAll().then(
              resH => {
                this.selectListHardwares = resH;
                this.filteredSelectListHardwares.next(this.selectListHardwares.slice());
                this.selectFilterCtrlHardwares.valueChanges
                  .pipe(takeUntil(this.onDestroyVar))
                  .subscribe(() => {
                    this.filterSelectHardwares();
                  });
                this.getAll();
              }
            );
          }
        );
      }
    );
  }

  ngOnDestroy() {
    this.onDestroyVar.next();
    this.onDestroyVar.complete();
  }

  setAssetTypes() {
    for (const eMember in AssetType) {
      if (typeof AssetType[eMember] !== 'number') {
        this.selectListType.push(AssetType[eMember]);
      }
    }
  }

  getAll() {
    this.entities = [];
    this.softwareAssetService.getAll().then(
      res => {
        this.entities = res;
      }
    );
  }

  delete() {
    if (this.mode !== 'CREATE') {
      this.entity = this.selectedRows[0];
      this.softwareAssetService.delete(this.entity.uuid)
        .then(res => {
          this.getAll();
        });
    }
  }

  edit() {
    this.entity = this.selectedRows[0];
    this.mode = 'EDIT';
    if (this.entity.type !== null) {
      this.setCheckedValueType(this.entity.type);
    }
    if (this.entity.owner.firstName !== '' && this.entity.owner.lastName !== '') {
      this.setCheckedValue(this.entity.owner.firstName, this.entity.owner.lastName);
    }
    if (this.entity.securityGoalValues.length > 0) {
      this.multipleCtrl.setValue(this.entity.securityGoalValues);
    }
    if (this.entity.hardwareAssets) {
      if (this.entity.hardwareAssets.length > 0) {
        this.multipleCtrlHardwares.setValue(this.entity.hardwareAssets);
      }
    }
    this.setFormFields();
  }

  create() {
    this.mode = 'CREATE';
    this.entity = new SoftwareAsset();
    this.selectedRows = [];
    this.selectedRows.push(this.entity);
    this.setCheckedValueType(null);
    this.setCheckedValue('', '');
    this.setFormFields();
  }

  submit() {
    this.entity.securityGoalValues = this.multipleCtrl.value;
    this.entity.hardwareAssets = this.multipleCtrlHardwares.value;
    if (this.mode === 'EDIT') {
      this.softwareAssetService.update(this.entity).then(
        res => {
          this.close();
          this.getAll();
        }
      );
    } else if (this.mode === 'CREATE') {
      this.softwareAssetService.create(this.entity).then(
        res => {
          this.close();
          this.getAll();
        }
      );
    }
  }

  close() {
    this.selectedRows = [];
    this.entity = new SoftwareAsset();
    this.multipleCtrl.setValue([]);
    this.mode = 'CREATE';
  }

  openDialog() {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '30vw';
    dialogConfig.maxWidth = '30vw';
    dialogConfig.data = {
      id: this.entity.uuid,
      name: this.entity.name,
      subtype: 'software'
    };

    const d = this.dialog.open(SecurityGoalValuesDialogComponent, dialogConfig);

    d.afterClosed().subscribe(() => {
      // update selected entity!!
      this.refresh();
    });
  }

  openHardwareDialog() {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '30vw';
    dialogConfig.maxWidth = '30vw';
    dialogConfig.data = {
      id: this.entity.uuid,
      name: this.entity.name,
      flag: true,
      networkId: 0
    };

    const d = this.dialog.open(HardwareAssetDialogComponent, dialogConfig);

    d.afterClosed().subscribe(() => {
      // update selected entity!!
      this.refresh();
    });
  }

  refresh() {
    this.entities = [];
    this.softwareAssetService.getAll().then(
      res => {
        this.entities = res;
        for (const v of this.entities) {
          if (v.uuid === this.entity.uuid) {
            this.entity.securityGoalValues = v.securityGoalValues;
            this.entity.hardwareAssets = v.hardwareAssets;
          }
        }
      }
    );
  }

  openAlert(message: string) {
    this.snackBar.open(message, 'x', {
      verticalPosition: 'top',
      duration: 2000,
      panelClass: ['snackbar-class']
    });
  }

  setFormFields() {
    this.fields = [
      {
        key: 'name',
        type: 'input',
        templateOptions: {
          label: 'Name',
          placeholder: 'Enter name',
          required: true }
      },
      {
        key: 'description',
        type: 'input',
        templateOptions: {
          label: 'Description',
          placeholder: 'Enter description',
          required: true }
      }
    ];
  }

  setCheckedValue(name: string, lname: string) {
    if (name !== '' && lname !== '') {
      this.selectList.forEach((item, index) => {
        if (item.firstName === name && item.lastName === lname) {
          this.selected = index;
          this.entity.owner = item;
          return false;
        }
      });
    } else {
      this.selected = 0;
      this.entity.owner = this.selectList[0];
    }
  }

  setCheckedValueType(type: AssetType) {
    if (type !== null) {
      this.selectListType.forEach((item, index) => {
        if (item === type) {
          this.selectedType = index;
          this.entity.type = item;
          return false;
        }
      });
    } else {
      this.selectedType = 0;
      this.entity.type = this.selectListType[0];
    }
  }

  protected filterSelect() {
    if (!this.selectList) {
      return;
    }
    // get the search keyword
    let search = this.selectFilterCtrl.value;
    if (!search) {
      this.filteredSelectList.next(this.selectList.slice());
      return;
    } else {
      search = search.toLowerCase();
    }

    this.filteredSelectList.next(
      this.selectList.filter(entity => (entity.firstName + ' ' + entity.lastName).toLowerCase().indexOf(search) > -1 )
    );
  }

  protected filterSelectType() {
    if (!this.selectListType) {
      return;
    }
    let search = this.selectFilterCtrlType.value;
    if (!search) {
      this.filteredSelectListType.next(this.selectListType.slice());
      return;
    } else {
      search = search.toLowerCase();
    }

    this.filteredSelectListType.next(
      this.selectListType.filter(entity => entity.toString().toLowerCase().indexOf(search) > -1)
    );
  }

  protected filterSelectSecurityValues() {
    if (!this.selectListSecurityValues) {
      return;
    }
    let search = this.selectFilterCtrlSecurityValues.value;
    if (!search) {
      this.filteredSelectListSecurityValues.next(this.selectListSecurityValues.slice());
      return;
    } else {
      search = search.toLowerCase();
    }

    this.filteredSelectListSecurityValues.next(
      this.selectListSecurityValues.filter(entity => entity.name.toString().toLowerCase().indexOf(search) > -1)
    );
  }

  protected filterSelectHardwares() {
    if (!this.selectListHardwares) {
      return;
    }
    let search = this.selectFilterCtrlHardwares.value;
    if (!search) {
      this.filteredSelectListHardwares.next(this.selectListHardwares.slice());
      return;
    } else {
      search = search.toLowerCase();
    }

    this.filteredSelectListHardwares.next(
      this.selectListHardwares.filter(entity => entity.name.toString().toLowerCase().indexOf(search) > -1)
    );
  }

  compareFunc(c1: SecurityGoalValue, c2: SecurityGoalValue): boolean {
    return c1 && c2 ? c1.id === c2.id : c1 === c2;
  }

  compareFuncHardwares(c1: HardwareAsset, c2: HardwareAsset): boolean {
    return c1 && c2 ? c1.uuid === c2.uuid : c1 === c2;
  }

  rowSelected() {
    if (this.selectedRows.length === 0) {
      this.selectedRows = [];
      this.multipleCtrl.setValue([]);
      this.entity = new SoftwareAsset();
      this.mode = 'CREATE';
    } else {
      this.edit();
    }
  }

   onChangeSlideToggle() {
    this.delay(250).then(
      res => {
        if (this.showSecValues) {
          this.showSecValues = false;
        } else {
          this.showSecValues = true;
        }
      }
    );
  }

  async delay(ms: number) {
    await new Promise(resolve => setTimeout(() => resolve(), ms));
  }
}
