import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssetNetworkListComponent } from './asset-network-list.component';

describe('AssetNetworkListComponent', () => {
  let component: AssetNetworkListComponent;
  let fixture: ComponentFixture<AssetNetworkListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssetNetworkListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssetNetworkListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
