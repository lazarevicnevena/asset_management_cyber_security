import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {HardwareAsset} from '../../../../model/hardware-asset';
import {SoftwareAssetService} from '../../../../service/software-asset.service';
import {FormControl, FormGroup} from '@angular/forms';
import {FormlyFieldConfig} from '@ngx-formly/core';
import {Owner} from '../../../../model/owner';
import {ReplaySubject, Subject} from 'rxjs';
import {AssetType} from '../../../../model/asset-type';
import {AssetLocation} from '../../../../model/location';
import {SecurityGoalValue} from '../../../../model/security-goal-value';
import {OwnerService} from '../../../../service/owner.service';
import {LocationService} from '../../../../service/location.service';
import {SecurityGoalValueService} from '../../../../service/security-goal-value.service';
import {takeUntil} from 'rxjs/internal/operators';
import {AssetNetworkService} from '../../../../service/asset-network.service';

@Component({
  selector: 'app-hardware-asset-dialog',
  templateUrl: './hardware-asset-dialog.component.html',
  styleUrls: ['./hardware-asset-dialog.component.css']
})
export class HardwareAssetDialogComponent implements OnInit, OnDestroy {

  uuid: string;
  networkId: number;
  name: string;
  entity: HardwareAsset;
  flag = false;
  softwareFlag = true;

  form = new FormGroup({});
  fields: FormlyFieldConfig[];
  selectList: Owner[];
  selected: number;
  selectFilterCtrl: FormControl = new FormControl();
  filteredSelectList: ReplaySubject<Owner[]> = new ReplaySubject<Owner[]>(1);
  onDestroyVar = new Subject<void>();
  selectListType = [];
  selectedType: number;
  selectFilterCtrlType: FormControl = new FormControl();
  filteredSelectListType: ReplaySubject<AssetType[]> = new ReplaySubject<AssetType[]>(1);
  selectListLocation: AssetLocation[];
  selectedLocation: number;
  selectFilterCtrlLocation: FormControl = new FormControl();
  filteredSelectListLocation: ReplaySubject<AssetLocation[]> = new ReplaySubject<AssetLocation[]>(1);
  // for Security goal values
  multipleCtrl: FormControl = new FormControl();
  selectListSecurityValues: SecurityGoalValue[];
  selectFilterCtrlSecurityValues: FormControl = new FormControl();
  filteredSelectListSecurityValues: ReplaySubject<SecurityGoalValue[]> = new ReplaySubject<SecurityGoalValue[]>(1);


  constructor( private softwareService: SoftwareAssetService,
               private ownerService: OwnerService,
               private securityGoalValueService: SecurityGoalValueService,
               private locationService: LocationService,
               private dialogRef: MatDialogRef<HardwareAssetDialogComponent>,
               private assetNetworkService: AssetNetworkService,
               @Inject(MAT_DIALOG_DATA) data) {
    this.uuid = data.id;
    this.name = data.name;
    this.softwareFlag = data.flag;
    this.networkId = data.networkId;
  }

  ngOnInit() {
    this.setFormFields();
    this.entity = new HardwareAsset();
    this.selectListSecurityValues = [];
    this.locationService.getAll().then(
      res => {
        this.selectListLocation = res;
        this.filteredSelectListLocation.next(this.selectListLocation.slice());
        this.selectFilterCtrlLocation.valueChanges
          .pipe(takeUntil(this.onDestroyVar))
          .subscribe(() => {
            this.filterSelectLocation();
          });
        this.ownerService.getAll().then(
          resO => {
            this.selectList = resO;
            this.filteredSelectList.next(this.selectList.slice());
            this.selectFilterCtrl.valueChanges
              .pipe(takeUntil(this.onDestroyVar))
              .subscribe(() => {
                this.filterSelect();
              });
            this.setAssetTypes();
            this.filteredSelectListType.next(this.selectListType.slice());
            this.selectFilterCtrlType.valueChanges
              .pipe(takeUntil(this.onDestroyVar))
              .subscribe(() => {
                this.filterSelectType();
              });
            this.securityGoalValueService.getAll().then(
              resSec => {
                this.selectListSecurityValues = resSec;
                this.filteredSelectListSecurityValues.next(this.selectListSecurityValues.slice());
                this.flag = true;
                this.selectFilterCtrlSecurityValues.valueChanges
                  .pipe(takeUntil(this.onDestroyVar))
                  .subscribe(() => {
                    this.filterSelectSecurityValues();
                  });
              }
            );
          }
        );
      }
    );
  }

  setAssetTypes() {
    for (const eMember in AssetType) {
      if (typeof AssetType[eMember] !== 'number') {
        this.selectListType.push(AssetType[eMember]);
      }
    }
  }

  setFormFields() {
    this.fields = [
      {
        key: 'name',
        type: 'input',
        templateOptions: {
          label: 'Name',
          placeholder: 'Enter name',
          required: true }
      },
      {
        key: 'description',
        type: 'input',
        templateOptions: {
          label: 'Description',
          placeholder: 'Enter description',
          required: true }
      },
      {
        key: 'macAddress',
        type: 'input',
        templateOptions: {
          label: 'Mac Address',
          placeholder: 'Enter Mac address',
          required: true }
      },
      {
        key: 'ipAddress',
        type: 'input',
        templateOptions: {
          label: 'IP Address',
          placeholder: 'Enter IP address',
          required: true }
      },
      {
        key: 'lastUpdated',
        type: 'datepicker',
        templateOptions: {
          label: 'Last updated',
          placeholder: 'Enter last updated',
          required: false }
      },
      {
        key: 'serialNumber',
        type: 'input',
        templateOptions: {
          label: 'Serial number',
          placeholder: 'Enter serial number',
          required: false }
      },
      {
        key: 'model',
        type: 'input',
        templateOptions: {
          label: 'Model',
          placeholder: 'Enter model',
          required: false }
      },
      {
        key: 'operatingSystem',
        type: 'input',
        templateOptions: {
          label: 'Operating system',
          placeholder: 'Enter operating system',
          required: false }
      }
    ];
  }

  protected filterSelectLocation() {
    if (!this.selectListLocation) {
      return;
    }
    // get the search keyword
    let search = this.selectFilterCtrlLocation.value;
    if (!search) {
      this.filteredSelectListLocation.next(this.selectListLocation.slice());
      return;
    } else {
      search = search.toLowerCase();
    }

    this.filteredSelectListLocation.next(
      this.selectListLocation.filter(entity => (entity.room + ' ' + entity.building).toLowerCase().indexOf(search) > -1 )
    );
  }

  protected filterSelect() {
    if (!this.selectList) {
      return;
    }
    // get the search keyword
    let search = this.selectFilterCtrl.value;
    if (!search) {
      this.filteredSelectList.next(this.selectList.slice());
      return;
    } else {
      search = search.toLowerCase();
    }

    this.filteredSelectList.next(
      this.selectList.filter(entity => (entity.firstName + ' ' + entity.lastName).toLowerCase().indexOf(search) > -1 )
    );
  }

  protected filterSelectType() {
    if (!this.selectListType) {
      return;
    }
    let search = this.selectFilterCtrlType.value;
    if (!search) {
      this.filteredSelectListType.next(this.selectListType.slice());
      return;
    } else {
      search = search.toLowerCase();
    }

    this.filteredSelectListType.next(
      this.selectListType.filter(entity => entity.toString().toLowerCase().indexOf(search) > -1)
    );
  }

  protected filterSelectSecurityValues() {
    if (!this.selectListSecurityValues) {
      return;
    }
    let search = this.selectFilterCtrlSecurityValues.value;
    if (!search) {
      this.filteredSelectListSecurityValues.next(this.selectListSecurityValues.slice());
      return;
    } else {
      search = search.toLowerCase();
    }

    this.filteredSelectListSecurityValues.next(
      this.selectListSecurityValues.filter(entity => entity.name.toString().toLowerCase().indexOf(search) > -1)
    );
  }

  compareFunc(c1: SecurityGoalValue, c2: SecurityGoalValue): boolean {
    return c1 && c2 ? c1.id === c2.id : c1 === c2;
  }

  close() {
    this.dialogRef.close();
  }

  ngOnDestroy() {
    this.onDestroyVar.next();
    this.onDestroyVar.complete();
  }

  submit() {
    this.entity.securityGoalValues = this.multipleCtrl.value;
    if (this.softwareFlag) {
      this.softwareService.addHardwaresDirectly(this.uuid, this.entity).then(
        res => {
          this.close();
        }
      );
    } else {
      this.assetNetworkService.addHAssetsDirectly(this.networkId, this.entity).then(
        res => {
          this.close();
        }
      );
    }
  }
}
