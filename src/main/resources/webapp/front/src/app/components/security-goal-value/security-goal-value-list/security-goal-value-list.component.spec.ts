import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SecurityGoalValueListComponent } from './security-goal-value-list.component';

describe('SecurityGoalValueListComponent', () => {
  let component: SecurityGoalValueListComponent;
  let fixture: ComponentFixture<SecurityGoalValueListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SecurityGoalValueListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SecurityGoalValueListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
