import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OwnerReportListComponent } from './owner-report-list.component';

describe('OwnerReportListComponent', () => {
  let component: OwnerReportListComponent;
  let fixture: ComponentFixture<OwnerReportListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OwnerReportListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OwnerReportListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
