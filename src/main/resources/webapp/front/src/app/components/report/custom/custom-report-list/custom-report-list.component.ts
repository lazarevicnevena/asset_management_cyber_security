import {Component, OnDestroy, OnInit} from '@angular/core';
import {CustomReport} from '../../../../model/custom-report';
import {CustomReportService} from '../../../../service/custom-report.service';
import {MatDialog, MatDialogConfig, MatSnackBar} from '@angular/material';
import {ResultDialogComponent} from '../../result-dialog/result-dialog.component';
import {FormlyFieldConfig} from '@ngx-formly/core';
import {FormControl, FormGroup} from '@angular/forms';
import {AssetType} from '../../../../model/asset-type';
import {ReplaySubject, Subject} from 'rxjs';
import {takeUntil} from 'rxjs/internal/operators';

@Component({
  selector: 'app-custom-report-list',
  templateUrl: './custom-report-list.component.html',
  styleUrls: ['./custom-report-list.component.css']
})
export class CustomReportListComponent implements OnInit, OnDestroy {

  entities: CustomReport[];
  form = new FormGroup({});
  selectedRows: CustomReport[];
  entity: CustomReport;
  fields: FormlyFieldConfig[];
  mode = '';
  selectListSubType = [];
  selectedSubType: number;
  selectFilterCtrlSubType: FormControl = new FormControl();
  filteredSelectListSubType: ReplaySubject<string[]> = new ReplaySubject<string[]>(1);
  onDestroyVar = new Subject<void>();
  selectListType = [];
  selectedType: number;
  selectFilterCtrlType: FormControl = new FormControl();
  filteredSelectListType: ReplaySubject<AssetType[]> = new ReplaySubject<AssetType[]>(1);

  constructor( private customReportService: CustomReportService,
               private dialog: MatDialog,
               private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.mode = 'CREATE';
    this.setFormFields();
    this.entity = new CustomReport();
    this.setAssetTypes();
    this.filteredSelectListType.next(this.selectListType.slice());
    this.selectFilterCtrlType.valueChanges
      .pipe(takeUntil(this.onDestroyVar))
      .subscribe(() => {
        this.filterSelectType();
      });
    this.setSubTypes();
    this.filteredSelectListSubType.next(this.selectListSubType.slice());
    this.selectFilterCtrlSubType.valueChanges
      .pipe(takeUntil(this.onDestroyVar))
      .subscribe(() => {
        this.filterSelectSubType();
      });
    this.getAll();
  }

  ngOnDestroy() {
    this.onDestroyVar.next();
    this.onDestroyVar.complete();
  }

  setAssetTypes() {
    for (const eMember in AssetType) {
      if (typeof AssetType[eMember] !== 'number') {
        this.selectListType.push(AssetType[eMember]);
      }
    }
  }

  setSubTypes() {
    this.selectListSubType.push(
      'ANY',
      'HARDWARE',
      'SOFTWARE',
      'VIRTUAL',
      'DATA'
    );
  }

  getAll() {
    this.entities = [];
    this.customReportService.getAll().then(
      res => {
        this.entities = res;
      }
    );
  }

  delete() {
    if (this.mode !== 'CREATE') {
      this.entity = this.selectedRows[0];
      this.customReportService.delete(this.entity.id)
        .then(res => {
          this.getAll();
        });
    }
  }

  edit() {
    this.entity = this.selectedRows[0];
    this.mode = 'EDIT';
    if (this.entity.type !== null) {
      this.setCheckedValueType(AssetType[this.entity.type]);
    }
    if (this.entity.subtype !== '') {
      this.setCheckedValueSubType(this.entity.subtype);
    }
    this.setFormFields();
  }

  create() {
    this.mode = 'CREATE';
    this.entity = new CustomReport();
    this.selectedRows = [];
    this.selectedRows.push(this.entity);
    this.setCheckedValueType(null);
    this.setCheckedValueSubType('');
    this.setFormFields();
  }

  submit() {
    if (this.mode === 'EDIT') {
      this.customReportService.update(this.entity).then(
        res => {
          this.close();
          this.getAll();
        }
      );
    } else if (this.mode === 'CREATE') {
      this.customReportService.create(this.entity).then(
        res => {
          this.close();
          this.getAll();
        }
      );
    }
  }

  close() {
    this.selectedRows = [];
    this.entity = new CustomReport();
    this.mode = 'CREATE';
  }

  filter() {
    if (this.selectedRows.length > 0) {
      this.entity = this.selectedRows[0];
      const id = this.entity.id;
      console.log('Filtering for report: ' + id);
      this.customReportService.filter(id).then(
        res => {
          const dialogConfig = new MatDialogConfig();

          dialogConfig.disableClose = true;
          dialogConfig.autoFocus = true;
          dialogConfig.data = {
            assets: res
          };

          this.dialog.open(ResultDialogComponent, dialogConfig);
        }
      ).catch(
        res => {
          this.openAlert('No asset satisfies criteria for this report!');
        }
      );
    }
  }

  openAlert(message: string) {
    this.snackBar.open(message, 'x', {
      verticalPosition: 'top',
      duration: 2000,
      panelClass: ['snackbar-class']
    });
  }

  setFormFields() {
    this.fields = [
      {
        key: 'name',
        type: 'input',
        templateOptions: {
          label: 'Name',
          placeholder: 'Enter asset name'}
      },
      {
        key: 'description',
        type: 'input',
        templateOptions: {
          label: 'Description',
          placeholder: 'Enter asset description'}
      },
      {
        key: 'building',
        type: 'input',
        templateOptions: {
          label: 'Building',
          placeholder: 'Enter building'}
      },
      {
        key: 'room',
        type: 'input',
        templateOptions: {
          label: 'Room',
          placeholder: 'Enter room'}
      },
      {
        key: 'owner',
        type: 'input',
        templateOptions: {
          label: 'Owner',
          placeholder: 'Enter owner'}
      },
      {
        key: 'securityGoalValue',
        type: 'input',
        templateOptions: {
          label: 'Security goal value',
          placeholder: 'Enter security goal value'}
      },
      {
        key: 'securityMeasure',
        type: 'input',
        templateOptions: {
          label: 'Security measure',
          placeholder: 'Enter security measure'}
      }
    ];
  }

  setCheckedValueSubType(subtype: string) {
    if (subtype !== '') {
      this.selectListSubType.forEach((item, index) => {
        if (item === subtype) {
          this.selectedSubType = index;
          this.entity.subtype = item;
          return false;
        }
      });
    } else {
      this.selectedSubType = 0;
      this.entity.subtype = this.selectListSubType[0];
    }
  }

  setCheckedValueType(type: AssetType) {
    if (type !== null) {
      this.selectListType.forEach((item, index) => {
        if (item === type) {
          this.selectedType = index;
          this.entity.type = item;
          return false;
        }
      });
    } else {
      this.selectedType = 0;
      this.entity.type = this.selectListType[0];
    }
  }

  protected filterSelectType() {
    if (!this.selectListType) {
      return;
    }
    let search = this.selectFilterCtrlType.value;
    if (!search) {
      this.filteredSelectListType.next(this.selectListType.slice());
      return;
    } else {
      search = search.toLowerCase();
    }

    this.filteredSelectListType.next(
      this.selectListType.filter(entity => entity.toString().toLowerCase().indexOf(search) > -1)
    );
  }

  protected filterSelectSubType() {
    if (!this.selectListSubType) {
      return;
    }
    let search = this.selectFilterCtrlSubType.value;
    if (!search) {
      this.filteredSelectListSubType.next(this.selectListSubType.slice());
      return;
    } else {
      search = search.toLowerCase();
    }

    this.filteredSelectListSubType.next(
      this.selectListSubType.filter(entity => entity.toString().toLowerCase().indexOf(search) > -1)
    );
  }

  rowSelected() {
    if (this.selectedRows.length === 0) {
      this.entity = new CustomReport();
      this.mode = 'CREATE';
    } else {
      this.edit();
    }
  }
}
