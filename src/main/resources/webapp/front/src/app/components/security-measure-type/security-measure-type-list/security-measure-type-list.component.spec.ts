import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SecurityMeasureTypeListComponent } from './security-measure-type-list.component';

describe('SecurityMeasureTypeListComponent', () => {
  let component: SecurityMeasureTypeListComponent;
  let fixture: ComponentFixture<SecurityMeasureTypeListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SecurityMeasureTypeListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SecurityMeasureTypeListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
