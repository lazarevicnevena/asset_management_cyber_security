import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SecurityGoalListComponent } from './security-goal-list.component';

describe('SecurityGoalListComponent', () => {
  let component: SecurityGoalListComponent;
  let fixture: ComponentFixture<SecurityGoalListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SecurityGoalListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SecurityGoalListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
