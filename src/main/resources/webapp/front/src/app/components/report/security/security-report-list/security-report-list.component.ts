import { Component, OnInit } from '@angular/core';
import {SecurityReport} from '../../../../model/security-report';
import {SecurityReportService} from '../../../../service/security-report.service';
import {MatDialog, MatDialogConfig, MatSnackBar} from '@angular/material';
import {Router} from '@angular/router';
import {ResultDialogComponent} from '../../result-dialog/result-dialog.component';
import {FormlyFieldConfig} from '@ngx-formly/core';
import {FormGroup} from '@angular/forms';

@Component({
  selector: 'app-security-report-list',
  templateUrl: './security-report-list.component.html',
  styleUrls: ['./security-report-list.component.css']
})
export class SecurityReportListComponent implements OnInit {

  entities: SecurityReport[];
  form = new FormGroup({});
  selectedRows: SecurityReport[];
  entity: SecurityReport;
  fields: FormlyFieldConfig[];
  mode = '';

  constructor( private securityReportService: SecurityReportService,
               private dialog: MatDialog,
               private snackBar: MatSnackBar,
               private router: Router) { }

  ngOnInit() {
    this.mode = 'CREATE';
    this.setFormFields();
    this.entity = new SecurityReport();
    this.getAll();
  }

  getAll() {
    this.entities = [];
    this.securityReportService.getAll().then(
      res => {
        this.entities = res;
      }
    );
  }

  delete() {
    if (this.mode !== 'CREATE') {
      this.entity = this.selectedRows[0];
      this.securityReportService.delete(this.entity.id)
        .then(res => {
          this.getAll();
        });
    }
  }

  edit() {
    this.entity = this.selectedRows[0];
    this.mode = 'EDIT';
    this.setFormFields();
  }

  create() {
    this.mode = 'CREATE';
    this.entity = new SecurityReport();
    this.selectedRows = [];
    this.selectedRows.push(this.entity);
    this.setFormFields();
  }

  submit() {
    if (this.mode === 'EDIT') {
      this.securityReportService.update(this.entity).then(
        res => {
          this.close();
          this.getAll();
        }
      );
    } else if (this.mode === 'CREATE') {
      this.securityReportService.create(this.entity).then(
        res => {
          this.close();
          this.getAll();
        }
      );
    }
  }

  close() {
    this.selectedRows = [];
    this.entity = new SecurityReport();
    this.mode = 'CREATE';
  }

  filter() {
    if (this.selectedRows.length > 0) {
      this.entity = this.selectedRows[0];
      const id = this.entity.id;
      console.log('Filtering for report: ' + id);
      this.securityReportService.filter(id).then(
        res => {
          const dialogConfig = new MatDialogConfig();

          dialogConfig.disableClose = true;
          dialogConfig.autoFocus = true;
          dialogConfig.data = {
            assets: res
          };

          this.dialog.open(ResultDialogComponent, dialogConfig);
        }
      ).catch(
        res => {
          this.openAlert('No asset satisfies criteria for this report!');
        }
      );
    }
  }

  openAlert(message: string) {
    this.snackBar.open(message, 'x', {
      verticalPosition: 'top',
      duration: 2000,
      panelClass: ['snackbar-class']
    });
  }

  setFormFields() {
    this.fields = [
      {
        key: 'name',
        type: 'input',
        templateOptions: {
          label: 'Name',
          placeholder: 'Enter asset name'}
      },
      {
        key: 'securityGoalValues',
        type: 'input',
        templateOptions: {
          label: 'Security goal values',
          placeholder: 'Enter security goal values'}
      },
      {
        key: 'securityMeasures',
        type: 'input',
        templateOptions: {
          label: 'Security measures',
          placeholder: 'Enter security measures'}
      },
    ];
  }

  rowSelected() {
    if (this.selectedRows.length === 0) {
      this.entity = new SecurityReport();
      this.mode = 'CREATE';
    } else {
      this.edit();
    }
  }
}
