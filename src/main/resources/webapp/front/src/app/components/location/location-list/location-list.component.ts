import { Component, OnInit } from '@angular/core';
import {LocationService} from '../../../service/location.service';
import {AssetLocation} from '../../../model/location';
import {Router} from '@angular/router';
import {FormlyFieldConfig} from '@ngx-formly/core';
import {FormGroup} from '@angular/forms';

@Component({
  selector: 'app-location-list',
  templateUrl: './location-list.component.html',
  styleUrls: ['./location-list.component.css']
})
export class LocationListComponent implements OnInit {

  entities: AssetLocation[];
  form = new FormGroup({});
  selectedRows: AssetLocation[];
  entity: AssetLocation;
  fields: FormlyFieldConfig[];
  mode = '';

  constructor( private locationService: LocationService,
               private router: Router) { }

  ngOnInit() {
    this.mode = 'CREATE';
    this.setFormFields();
    this.entity = new AssetLocation();
    this.getAll();
  }

  getAll() {
    this.entities = [];
    this.locationService.getAll().then(
      res => {
        this.entities = res;
      }
    );
  }

  delete() {
    if (this.mode !== 'CREATE') {
      this.entity = this.selectedRows[0];
      console.log('Deleting location with id: ' + this.entity.id);
      this.locationService.delete(this.entity.id)
        .then(res => {
          this.getAll();
        });
    }
  }

  edit() {
    this.entity = this.selectedRows[0];
    console.log('Editing location with id: ' + this.entity.id);
    this.mode = 'EDIT';
    this.setFormFields();
  }

  create() {
    this.mode = 'CREATE';
    this.entity = new AssetLocation();
    this.selectedRows = [];
    this.selectedRows.push(this.entity);
    this.setFormFields();
  }

  submit() {
    if (this.mode === 'EDIT') {
      this.locationService.update(this.entity).then(
        res => {
          this.close();
          this.getAll();
        }
      );
    } else if (this.mode === 'CREATE') {
      this.locationService.create(this.entity).then(
        res => {
          this.close();
          this.getAll();
        }
      );
    }
  }

  close() {
    this.selectedRows = [];
    this.entity = new AssetLocation();
    this.mode = 'CREATE';
  }

  setFormFields() {
    this.fields = [
      {
        key: 'building',
        type: 'input',
        templateOptions: {
          label: 'Building',
          placeholder: 'Enter building',
          required: true }
      },
      {
        key: 'room',
        type: 'input',
        templateOptions: {
          label: 'Room',
          placeholder: 'Enter room',
          required: true }
      },
      {
        key: 'rack',
        type: 'input',
        templateOptions: {
          label: 'Rack',
          placeholder: 'Enter rack',
          required: false }
      }
    ];
  }

  rowSelected() {
    if (this.selectedRows.length === 0) {
      this.entity = new AssetLocation();
      this.mode = 'CREATE';
    } else {
      this.edit();
    }
  }
}
