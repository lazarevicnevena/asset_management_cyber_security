import { Component, OnInit } from '@angular/core';
import {SecurityGoal} from '../../../model/security-goal';
import {SecurityGoalService} from '../../../service/security-goal.service';
import {Router} from '@angular/router';
import {FormlyFieldConfig} from '@ngx-formly/core';
import {FormGroup} from '@angular/forms';

@Component({
  selector: 'app-security-goal-list',
  templateUrl: './security-goal-list.component.html',
  styleUrls: ['./security-goal-list.component.css']
})
export class SecurityGoalListComponent implements OnInit {

  entities: SecurityGoal[];
  form = new FormGroup({});
  selectedRows: SecurityGoal[];
  entity: SecurityGoal;
  fields: FormlyFieldConfig[];
  mode = '';
  constructor( private securityGoalService: SecurityGoalService,
               private router: Router) { }

  ngOnInit() {
    this.mode = 'CREATE';
    this.setFormFields();
    this.entity = new SecurityGoal();
    this.getAll();
  }

  getAll() {
    this.entities = [];
    this.securityGoalService.getAll().then(
      res => {
        this.entities = res;
      }
    );
  }

  delete() {
    if (this.mode !== 'CREATE') {
      this.entity = this.selectedRows[0];
      console.log('Deleting security goal with id: ' + this.entity.id);
      this.securityGoalService.delete(this.entity.id)
        .then(res => {
          this.getAll();
        });
    }
  }

  edit() {
    this.entity = this.selectedRows[0];
    console.log('Editing security goal with id: ' + this.entity.id);
    this.mode = 'EDIT';
    this.setFormFields();
  }

  create() {
    this.mode = 'CREATE';
    this.entity = new SecurityGoal();
    this.selectedRows = [];
    this.selectedRows.push(this.entity);
    this.setFormFields();
  }

  submit() {
    if (this.mode === 'EDIT') {
      this.securityGoalService.update(this.entity).then(
        res => {
          this.close();
          this.getAll();
        }
      );
    } else if (this.mode === 'CREATE') {
      this.securityGoalService.create(this.entity).then(
        res => {
          this.close();
          this.getAll();
        }
      );
    }
  }

  close() {
    this.selectedRows = [];
    this.entity = new SecurityGoal();
    this.mode = 'CREATE';
  }

  setFormFields() {
    this.fields = [
      {
        key: 'name',
        type: 'input',
        templateOptions: {
          label: 'Name',
          placeholder: 'Enter name',
          required: true }
      }
    ];
  }

  rowSelected() {
    if (this.selectedRows.length === 0) {
      this.entity = new SecurityGoal();
      this.mode = 'CREATE';
    } else {
      this.edit();
    }
  }
}
