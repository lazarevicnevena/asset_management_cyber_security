import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {SecurityMeasure} from '../../../model/security-measure';
import {SecurityMeasureService} from '../../../service/security-measure.service';
import {Router} from '@angular/router';
import {FormlyFieldConfig} from '@ngx-formly/core';
import {FormControl, FormGroup} from '@angular/forms';
import {SecurityMeasureTypeService} from '../../../service/security-measure-type.service';
import {SecurityMeasureType} from '../../../model/security-measure-type';
import {ReplaySubject, Subject} from 'rxjs';
import {takeUntil} from 'rxjs/internal/operators';

@Component({
  selector: 'app-security-measure-list',
  templateUrl: './security-measure-list.component.html',
  styleUrls: ['./security-measure-list.component.css']
})
export class SecurityMeasureListComponent implements OnInit, OnDestroy {

  entities: SecurityMeasure[];
  form = new FormGroup({});
  selectedRows: SecurityMeasure[];
  entity: SecurityMeasure;
  fields: FormlyFieldConfig[];
  mode = '';
  selectList: SecurityMeasureType[];
  selected: number;
  selectFilterCtrl: FormControl = new FormControl();
  filteredSelectList: ReplaySubject<SecurityMeasureType[]> = new ReplaySubject<SecurityMeasureType[]>(1);
  onDestroyVar = new Subject<void>();

  constructor( private securityMeasureService: SecurityMeasureService,
               private securityMeasureTypeService: SecurityMeasureTypeService,
               private router: Router) { }

  ngOnInit() {
    this.selected = -1;
    this.mode = 'CREATE';
    this.setFormFields();
    this.entity = new SecurityMeasure();
    this.securityMeasureTypeService.getAll().then(
      res2 => {
        this.selectList = res2;
        this.filteredSelectList.next(this.selectList.slice());

        // listen for search field value changes
        this.selectFilterCtrl.valueChanges
          .pipe(takeUntil(this.onDestroyVar))
          .subscribe(() => {
            this.filterSelect();
          });
        this.getAll();
      }
    );
  }

  ngOnDestroy() {
    this.onDestroyVar.next();
    this.onDestroyVar.complete();
  }

  getAll() {
    this.entities = [];
    this.securityMeasureService.getAll().then(
      res => {
        this.entities = res;
      }
    );
  }

  delete() {
    if (this.mode !== 'CREATE') {
      this.entity = this.selectedRows[0];
      this.securityMeasureService.delete(this.entity.id)
        .then(res => {
          this.getAll();

        });
    }
  }

  edit() {
    this.entity = this.selectedRows[0];
    console.log('Editing security measure with id: ' + this.entity.id);
    this.mode = 'EDIT';
    if (this.entity.type.name !== '') {
      this.setCheckedValue(this.entity.type.name);
    }
    this.setFormFields();
  }

  create() {
    this.mode = 'CREATE';
    this.entity = new SecurityMeasure();
    this.selectedRows = [];
    this.setCheckedValue('');
    this.setFormFields();
  }

  submit() {
    if (this.mode === 'EDIT') {
      this.securityMeasureService.update(this.entity).then(
        res => {
          this.close();
          this.getAll();
        }
      );
    } else if (this.mode === 'CREATE') {
      this.securityMeasureService.create(this.entity).then(
        res => {
          this.close();
          this.getAll();
        }
      );
    }
  }

  close() {
    this.selectedRows = [];
    this.entity = new SecurityMeasure();
    this.mode = 'CREATE';
  }

  setFormFields() {
    this.fields = [
      {
        key: 'name',
        type: 'input',
        templateOptions: {
          label: 'Name',
          placeholder: 'Enter name',
          required: true }
      },
      {
        key: 'description',
        type: 'input',
        templateOptions: {
          label: 'Description',
          placeholder: 'Enter description',
          required: true }
      }
    ];
  }

  setCheckedValue(name: string) {
    if (name !== '') {
      this.selectList.forEach((item, index) => {
        if (item.name === name) {
          this.selected = index;
          this.entity.type = item;
          return false;
        }
      });
    } else {
      this.selected = 0;
      this.entity.type = this.selectList[0];
    }
  }

  protected filterSelect() {
    if (!this.selectList) {
      return;
    }
    // get the search keyword
    let search = this.selectFilterCtrl.value;
    if (!search) {
      this.filteredSelectList.next(this.selectList.slice());
      return;
    } else {
      search = search.toLowerCase();
    }

    this.filteredSelectList.next(
      this.selectList.filter(entity => entity.name.toLowerCase().indexOf(search) > -1)
    );
  }

  rowSelected() {
    if (this.selectedRows.length === 0) {
      this.entity = new SecurityMeasure();
      this.mode = 'CREATE';
    } else {
      this.edit();
    }
  }
}
