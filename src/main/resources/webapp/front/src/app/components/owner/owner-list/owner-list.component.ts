import {Component, OnInit} from '@angular/core';
import {OwnerService} from '../../../service/owner.service';
import {Owner} from '../../../model/owner';
import {Router} from '@angular/router';
import {FormlyFieldConfig} from '@ngx-formly/core';
import {FormGroup} from '@angular/forms';

@Component({
  selector: 'app-owner-list',
  templateUrl: './owner-list.component.html',
  styleUrls: ['./owner-list.component.css']
})
export class OwnerListComponent implements OnInit {

  entities: Owner[];
  form = new FormGroup({});
  selectedRows: Owner[];
  entity: Owner;
  fields: FormlyFieldConfig[];
  mode = '';

  constructor(private ownerService: OwnerService,
              private router: Router) {
  }

  ngOnInit() {
    this.mode = 'CREATE';
    this.setFormFields();
    this.entity = new Owner();
    this.getAll();
  }

  getAll() {
    this.entities = [];
    this.ownerService.getAll().then(
      res => {
        this.entities = res;
      }
    );
  }

  delete() {
    if (this.mode !== 'CREATE') {
      this.entity = this.selectedRows[0];
      console.log('Deleting owner with id: ' + this.entity.id);
      this.ownerService.delete(this.entity.employeeId)
        .then(res => {
          this.getAll();
        });
    }
  }

  edit() {
    this.entity = this.selectedRows[0];
    console.log('Editing owner with id: ' + this.entity.id);
    this.mode = 'EDIT';
    this.setFormFields();
  }

  create() {
    this.mode = 'CREATE';
    this.entity = new Owner();
    this.selectedRows = [];
    this.selectedRows.push(this.entity);
    this.setFormFields();
  }

  submit() {
    if (this.mode === 'EDIT') {
      this.ownerService.update(this.entity).then(
        res => {
          this.close();
          this.getAll();
        }
      );
    } else if (this.mode === 'CREATE') {
      this.ownerService.create(this.entity).then(
        res => {
          this.close();
          this.getAll();
        }
      );
    }
  }

  close() {
    this.selectedRows = [];
    this.entity = new Owner();
    this.mode = 'CREATE';
  }

  setFormFields() {
    this.fields = [
      {
        key: 'firstName',
        type: 'input',
        templateOptions: {
          label: 'First name',
          placeholder: 'Enter first name',
          required: true
        }
      },
      {
        key: 'middleName',
        type: 'input',
        templateOptions: {
          label: 'Middle name',
          placeholder: 'Enter middle name',
          required: false
        }
      },
      {
        key: 'lastName',
        type: 'input',
        templateOptions: {
          label: 'Last name',
          placeholder: 'Enter last name',
          required: true
        }
      },
      {
        key: 'phoneNumber',
        type: 'input',
        templateOptions: {
          label: 'Phone number',
          placeholder: 'Enter phone number',
          required: false
        }
      },
      {
        key: 'employeeId',
        type: 'input',
        templateOptions: {
          label: 'Employee Id',
          placeholder: 'Enter employee Id',
          required: true
        }
      }
    ];
  }

  rowSelected() {
    if (this.selectedRows.length === 0) {
      this.entity = new Owner();
      this.mode = 'CREATE';
    } else {
      this.edit();
    }
  }
}
