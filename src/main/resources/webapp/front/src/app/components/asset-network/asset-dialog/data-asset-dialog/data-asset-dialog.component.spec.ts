import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DataAssetDialogComponent } from './data-asset-dialog.component';

describe('DataAssetDialogComponent', () => {
  let component: DataAssetDialogComponent;
  let fixture: ComponentFixture<DataAssetDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DataAssetDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DataAssetDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
