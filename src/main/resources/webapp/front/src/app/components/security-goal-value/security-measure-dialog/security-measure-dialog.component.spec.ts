import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SecurityMeasureDialogComponent } from './security-measure-dialog.component';

describe('SecurityMeasureDialogComponent', () => {
  let component: SecurityMeasureDialogComponent;
  let fixture: ComponentFixture<SecurityMeasureDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SecurityMeasureDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SecurityMeasureDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
