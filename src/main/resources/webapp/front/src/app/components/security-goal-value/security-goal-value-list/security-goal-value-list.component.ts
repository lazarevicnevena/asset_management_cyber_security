import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {SecurityGoalValueService} from '../../../service/security-goal-value.service';
import {SecurityGoalService} from '../../../service/security-goal.service';
import {SecurityGoalValue} from '../../../model/security-goal-value';
import {SecurityMeasureDialogComponent} from '../security-measure-dialog/security-measure-dialog.component';
import {MatDialog, MatDialogConfig, MatSnackBar} from '@angular/material';
import {FormlyFieldConfig} from '@ngx-formly/core';
import {FormControl, FormGroup} from '@angular/forms';
import {SecurityGoal} from '../../../model/security-goal';
import {ReplaySubject, Subject} from 'rxjs';
import {MatSelect} from '@angular/material';
import {takeUntil} from 'rxjs/internal/operators';
import {SecurityMeasure} from '../../../model/security-measure';
import {SecurityMeasureService} from '../../../service/security-measure.service';

@Component({
  selector: 'app-security-goal-value-list',
  templateUrl: './security-goal-value-list.component.html',
  styleUrls: ['./security-goal-value-list.component.css']
})
export class SecurityGoalValueListComponent implements OnInit, OnDestroy {

  entities: SecurityGoalValue[];
  form = new FormGroup({});
  selectedRows: SecurityGoalValue[] = [];
  entity: SecurityGoalValue;
  fields: FormlyFieldConfig[];
  mode = '';
  selectList: SecurityGoal[] = [];
  selected: number;
  selectFilterCtrl: FormControl = new FormControl();
  filteredSelectList: ReplaySubject<SecurityGoal[]> = new ReplaySubject<SecurityGoal[]>(1);
  @ViewChild('singleSelect') singleSelect: MatSelect;
  onDestroyVar = new Subject<void>();
  multipleCtrl: FormControl = new FormControl();
  selectListMeasure: SecurityMeasure[];
  selectFilterCtrlMeasure: FormControl = new FormControl();
  filteredSelectListMeasure: ReplaySubject<SecurityMeasure[]> = new ReplaySubject<SecurityMeasure[]>(1);

  constructor( private securityGoalValueService: SecurityGoalValueService,
               private securityGoalService: SecurityGoalService,
               private securityMeasureService: SecurityMeasureService,
               private snackBar: MatSnackBar,
               private dialog: MatDialog) { }

  ngOnInit() {
    this.mode = 'CREATE';
    this.setFormFields();
    this.entity = new SecurityGoalValue();
    this.selectListMeasure = [];
    this.securityGoalService.getAll().then(
      res => {
        this.selectList = res;
        this.filteredSelectList.next(this.selectList.slice());

        // listen for search field value changes
        this.selectFilterCtrl.valueChanges
          .pipe(takeUntil(this.onDestroyVar))
          .subscribe(() => {
            this.filterSelect();
          });
        this.securityMeasureService.getAll().then(
          resM => {
            this.selectListMeasure = resM;
            this.filteredSelectListMeasure.next(this.selectListMeasure.slice());

            // listen for search field value changes
            this.selectFilterCtrlMeasure.valueChanges
              .pipe(takeUntil(this.onDestroyVar))
              .subscribe(() => {
                this.filterSelectMeasure();
              });
            this.getAll();
          }
        );
      }
    );
  }

  getAll() {
    this.entities = [];
    this.securityGoalValueService.getAll().then(
      res => {
        this.entities = res;
      }
    );
  }

  ngOnDestroy() {
    this.onDestroyVar.next();
    this.onDestroyVar.complete();
  }

  delete() {
    if (this.mode !== 'CREATE') {
      this.entity = this.selectedRows[0];
      this.securityGoalValueService.delete(this.entity.id)
        .then(res => {
          this.getAll();
        });
    }
  }

  edit() {
    this.entity = this.selectedRows[0];
    this.mode = 'EDIT';
    if (this.entity.securityGoal.name !== '') {
      this.setCheckedValue(this.entity.securityGoal.name);
    }
    if (this.entity.securityMeasures.length > 0) {
      this.multipleCtrl.setValue(this.entity.securityMeasures);
    }
    this.setFormFields();
  }

  create() {
    this.mode = 'CREATE';
    this.entity = new SecurityGoalValue();
    this.selectedRows = [];
    this.setCheckedValue('');
    this.selectedRows.push(this.entity);
    this.setFormFields();
  }

  submit() {
    this.entity.securityMeasures = this.multipleCtrl.value;
    if (this.mode === 'EDIT') {
      this.securityGoalValueService.update(this.entity).then(
        res => {
          this.close();
          this.getAll();
        }
      );
    } else if (this.mode === 'CREATE') {
      this.securityGoalValueService.create(this.entity).then(
        res => {
          this.close();
          this.getAll();
        }
      );
    }
  }

  close() {
    this.selectedRows = [];
    this.entity = new SecurityGoalValue();
    this.multipleCtrl.setValue([]);
    this.mode = 'CREATE';
  }

  openMeasuresDialog() {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '30vw';
    dialogConfig.maxWidth = '30vw';
    dialogConfig.data = {
      id: this.entity.id,
      name: this.entity.name
    };

    const d = this.dialog.open(SecurityMeasureDialogComponent, dialogConfig);

    d.afterClosed().subscribe(() => {
      // update selected entity!!
      this.refresh();
    });
  }

  refresh() {
    this.entities = [];
    this.securityGoalValueService.getAll().then(
      res => {
        this.entities = res;
        for (const v of this.entities) {
          if (v.id === this.entity.id) {
            this.entity.securityMeasures = v.securityMeasures;
          }
        }
      }
    );
  }

  openAlert(message: string) {
    this.snackBar.open(message, 'x', {
      verticalPosition: 'top',
      duration: 2000,
      panelClass: ['snackbar-class']
    });
  }

  setFormFields() {
    this.fields = [
      {
        key: 'name',
        type: 'input',
        templateOptions: {
          label: 'Name',
          placeholder: 'Enter name',
          required: true }
      },
      {
        key: 'description',
        type: 'input',
        templateOptions: {
          label: 'Description',
          placeholder: 'Enter description',
          required: true }
      }
    ];
  }

  setCheckedValue(name: string) {
    if (name !== '') {
      this.selectList.forEach((item, index) => {
        if (item.name === name) {
          this.selected = index;
          this.entity.securityGoal = item;
          return false;
        }
      });
    } else {
      this.selected = 0;
      this.entity.securityGoal = this.selectList[0];
    }
  }

  protected filterSelect() {
    if (!this.selectList) {
      return;
    }
    // get the search keyword
    let search = this.selectFilterCtrl.value;
    if (!search) {
      this.filteredSelectList.next(this.selectList.slice());
      return;
    } else {
      search = search.toLowerCase();
    }

    this.filteredSelectList.next(
      this.selectList.filter(entity => entity.name.toLowerCase().indexOf(search) > -1)
    );
  }

  protected filterSelectMeasure() {
    if (!this.selectListMeasure) {
      return;
    }
    // get the search keyword
    let search = this.selectFilterCtrlMeasure.value;
    if (!search) {
      this.filteredSelectListMeasure.next(this.selectListMeasure.slice());
      return;
    } else {
      search = search.toLowerCase();
    }

    this.filteredSelectListMeasure.next(
      this.selectListMeasure.filter(entity => entity.name.toLowerCase().indexOf(search) > -1)
    );
  }

  compareFunc(c1: SecurityMeasure, c2: SecurityMeasure): boolean {
    return c1 && c2 ? c1.id === c2.id : c1 === c2;
  }

  rowSelected() {
    if (this.selectedRows.length === 0) {
      this.selectedRows = [];
      this.multipleCtrl.setValue([]);
      this.entity = new SecurityGoalValue();
      this.mode = 'CREATE';
    } else {
      this.edit();
    }
  }
}
