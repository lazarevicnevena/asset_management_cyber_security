import {Component, OnInit, Inject, OnDestroy} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {SecurityMeasure} from '../../../model/security-measure';
import {SecurityGoalValueService} from '../../../service/security-goal-value.service';
import {FormControl, FormGroup} from '@angular/forms';
import {FormlyFieldConfig} from '@ngx-formly/core';
import {SecurityMeasureType} from '../../../model/security-measure-type';
import {ReplaySubject, Subject} from 'rxjs';
import {takeUntil} from 'rxjs/internal/operators';
import {SecurityMeasureTypeService} from '../../../service/security-measure-type.service';

@Component({
  selector: 'app-security-measure-dialog',
  templateUrl: './security-measure-dialog.component.html',
  styleUrls: ['./security-measure-dialog.component.css']
})
export class SecurityMeasureDialogComponent implements OnInit, OnDestroy {

  id: number; // id of selected security goal value
  name: string;
  // form fields
  form = new FormGroup({});
  entity: SecurityMeasure;
  fields: FormlyFieldConfig[];
  selectList: SecurityMeasureType[];
  selected: number;
  selectFilterCtrl: FormControl = new FormControl();
  filteredSelectList: ReplaySubject<SecurityMeasureType[]> = new ReplaySubject<SecurityMeasureType[]>(1);
  onDestroyVar = new Subject<void>();
  flag = false;

  constructor( private securityGoalValueService: SecurityGoalValueService,
               private dialogRef: MatDialogRef<SecurityMeasureDialogComponent>,
               private securityMeasureTypeService: SecurityMeasureTypeService,
               @Inject(MAT_DIALOG_DATA) data) {
    this.id = data.id;
    this.name = data.name;
  }

  ngOnInit() {
    this.selected = -1;
    this.setFormFields();
    this.entity = new SecurityMeasure();
    this.securityMeasureTypeService.getAll().then(
      res2 => {
        this.selectList = res2;
        this.filteredSelectList.next(this.selectList.slice());
        this.flag = true;
        // listen for search field value changes
        this.selectFilterCtrl.valueChanges
          .pipe(takeUntil(this.onDestroyVar))
          .subscribe(() => {
            this.filterSelect();
          });
      }
    );
  }

  protected filterSelect() {
    if (!this.selectList) {
      return;
    }
    // get the search keyword
    let search = this.selectFilterCtrl.value;
    if (!search) {
      this.filteredSelectList.next(this.selectList.slice());
      return;
    } else {
      search = search.toLowerCase();
    }

    this.filteredSelectList.next(
      this.selectList.filter(entity => entity.name.toLowerCase().indexOf(search) > -1)
    );
  }

  setFormFields() {
    this.fields = [
      {
        key: 'name',
        type: 'input',
        templateOptions: {
          label: 'Name',
          placeholder: 'Enter name',
          required: true }
      },
      {
        key: 'description',
        type: 'input',
        templateOptions: {
          label: 'Description',
          placeholder: 'Enter description',
          required: true }
      }
    ];
  }

  close() {
    this.dialogRef.close();
  }

  ngOnDestroy() {
    this.onDestroyVar.next();
    this.onDestroyVar.complete();
  }

  submit() {
    this.securityGoalValueService.addMeasureDirectly(this.id, this.entity).then(
      res => {
        this.close();
      }
    );
  }

}
