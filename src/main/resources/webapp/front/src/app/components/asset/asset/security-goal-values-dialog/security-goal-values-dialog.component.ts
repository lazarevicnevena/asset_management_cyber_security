import {Component, OnInit, Inject, OnDestroy} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {SecurityGoalValue} from '../../../../model/security-goal-value';
import {AssetService} from '../../../../service/asset.service';
import {VirtualAssetService} from '../../../../service/virtual-asset.service';
import {HardwareAssetService} from '../../../../service/hardware-asset.service';
import {SoftwareAssetService} from '../../../../service/software-asset.service';
import {DataAssetService} from '../../../../service/data-asset.service';
import {FormControl, FormGroup} from '@angular/forms';
import {FormlyFieldConfig} from '@ngx-formly/core';
import {SecurityGoal} from '../../../../model/security-goal';
import {ReplaySubject, Subject} from 'rxjs';
import {SecurityMeasure} from '../../../../model/security-measure';
import {takeUntil} from 'rxjs/internal/operators';
import {SecurityGoalService} from '../../../../service/security-goal.service';
import {SecurityMeasureService} from '../../../../service/security-measure.service';

@Component({
  selector: 'app-security-goal-values-dialog',
  templateUrl: './security-goal-values-dialog.component.html',
  styleUrls: ['./security-goal-values-dialog.component.css']
})
export class SecurityGoalValuesDialogComponent implements OnInit, OnDestroy {

  entity: SecurityGoalValue;
  uuid: string;
  name: string;
  selectedRows = [];
  flag = false;
  assetSubType: string;

  form = new FormGroup({});
  fields: FormlyFieldConfig[];
  selectList: SecurityGoal[] = [];
  selected: number;
  selectFilterCtrl: FormControl = new FormControl();
  filteredSelectList: ReplaySubject<SecurityGoal[]> = new ReplaySubject<SecurityGoal[]>(1);
  onDestroyVar = new Subject<void>();
  multipleCtrl: FormControl = new FormControl();
  selectListMeasure: SecurityMeasure[];
  selectFilterCtrlMeasure: FormControl = new FormControl();
  filteredSelectListMeasure: ReplaySubject<SecurityMeasure[]> = new ReplaySubject<SecurityMeasure[]>(1);

  constructor( private virtualAssetService: VirtualAssetService,
               private hardwareAssetService: HardwareAssetService,
               private softwareAssetService: SoftwareAssetService,
               private dataAssetService: DataAssetService,
               private securityGoalService: SecurityGoalService,
               private securityMeasureService: SecurityMeasureService,
               private dialogRef: MatDialogRef<SecurityGoalValuesDialogComponent>,
               @Inject(MAT_DIALOG_DATA) data) {
    this.uuid = data.id;
    this.name = data.name;
    this.assetSubType = data.subtype;
  }

  ngOnInit() {
    this.setFormFields();
    this.entity = new SecurityGoalValue();
    this.selectListMeasure = [];
    this.securityGoalService.getAll().then(
      res => {
        this.selectList = res;
        this.filteredSelectList.next(this.selectList.slice());

        // listen for search field value changes
        this.selectFilterCtrl.valueChanges
          .pipe(takeUntil(this.onDestroyVar))
          .subscribe(() => {
            this.filterSelect();
          });
        this.securityMeasureService.getAll().then(
          resM => {
            this.selectListMeasure = resM;
            this.filteredSelectListMeasure.next(this.selectListMeasure.slice());
            this.flag = true;
            // listen for search field value changes
            this.selectFilterCtrlMeasure.valueChanges
              .pipe(takeUntil(this.onDestroyVar))
              .subscribe(() => {
                this.filterSelectMeasure();
              });
          }
        );
      }
    );
  }



  close() {
    this.dialogRef.close();
  }

  ngOnDestroy() {
    this.onDestroyVar.next();
    this.onDestroyVar.complete();
  }

  setFormFields() {
    this.fields = [
      {
        key: 'name',
        type: 'input',
        templateOptions: {
          label: 'Name',
          placeholder: 'Enter name',
          required: true }
      },
      {
        key: 'description',
        type: 'input',
        templateOptions: {
          label: 'Description',
          placeholder: 'Enter description',
          required: true }
      }
    ];
  }

  protected filterSelect() {
    if (!this.selectList) {
      return;
    }
    // get the search keyword
    let search = this.selectFilterCtrl.value;
    if (!search) {
      this.filteredSelectList.next(this.selectList.slice());
      return;
    } else {
      search = search.toLowerCase();
    }

    this.filteredSelectList.next(
      this.selectList.filter(entity => entity.name.toLowerCase().indexOf(search) > -1)
    );
  }

  protected filterSelectMeasure() {
    if (!this.selectListMeasure) {
      return;
    }
    // get the search keyword
    let search = this.selectFilterCtrlMeasure.value;
    if (!search) {
      this.filteredSelectListMeasure.next(this.selectListMeasure.slice());
      return;
    } else {
      search = search.toLowerCase();
    }

    this.filteredSelectListMeasure.next(
      this.selectListMeasure.filter(entity => entity.name.toLowerCase().indexOf(search) > -1)
    );
  }

  compareFunc(c1: SecurityMeasure, c2: SecurityMeasure): boolean {
    return c1 && c2 ? c1.id === c2.id : c1 === c2;
  }

  submit() {
    this.entity.securityMeasures = this.multipleCtrl.value;
    if (this.assetSubType === 'virtual') {
      this.virtualAssetService.addGoalValueDirectly(this.uuid, this.entity).then(
        res => {
          this.close();
        }
      );
    } else if (this.assetSubType === 'hardware') {
      this.hardwareAssetService.addGoalValueDirectly(this.uuid, this.entity).then(
        res => {
          this.close();
        }
      );
    } else if (this.assetSubType === 'software') {
      this.softwareAssetService.addGoalValueDirectly(this.uuid, this.entity).then(
        res => {
          this.close();
        }
      );
    } else {
      this.dataAssetService.addGoalValueDirectly(this.uuid, this.entity).then(
        res => {
          this.close();
        }
      );
    }
  }

}
