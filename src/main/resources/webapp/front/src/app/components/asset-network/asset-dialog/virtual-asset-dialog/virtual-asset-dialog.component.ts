import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {AssetType} from '../../../../model/asset-type';
import {HardwareAssetService} from '../../../../service/hardware-asset.service';
import {OwnerService} from '../../../../service/owner.service';
import {Owner} from '../../../../model/owner';
import {ReplaySubject, Subject} from 'rxjs';
import {takeUntil} from 'rxjs/internal/operators';
import {HardwareAsset} from '../../../../model/hardware-asset';
import {SecurityGoalValue} from '../../../../model/security-goal-value';
import {SecurityGoalValueService} from '../../../../service/security-goal-value.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {FormlyFieldConfig} from '@ngx-formly/core';
import {FormControl, FormGroup} from '@angular/forms';
import {VirtualAsset} from '../../../../model/virtual-asset';
import {AssetNetworkService} from '../../../../service/asset-network.service';


@Component({
  selector: 'app-virtual-asset-dialog',
  templateUrl: './virtual-asset-dialog.component.html',
  styleUrls: ['./virtual-asset-dialog.component.css']
})
export class VirtualAssetDialogComponent implements OnInit, OnDestroy {

  networkId: number;
  name: string;
  flag = false;

  form = new FormGroup({});
  entity: VirtualAsset;
  fields: FormlyFieldConfig[];
  selectList: Owner[];
  selected: number;
  selectFilterCtrl: FormControl = new FormControl();
  filteredSelectList: ReplaySubject<Owner[]> = new ReplaySubject<Owner[]>(1);
  onDestroyVar = new Subject<void>();
  selectListType = [];
  selectedType: number;
  selectFilterCtrlType: FormControl = new FormControl();
  filteredSelectListType: ReplaySubject<AssetType[]> = new ReplaySubject<AssetType[]>(1);
  selectListHardwares: HardwareAsset[];
  selectedHardwares: number;
  selectFilterCtrlHardwares: FormControl = new FormControl();
  filteredSelectListHardwares: ReplaySubject<HardwareAsset[]> = new ReplaySubject<HardwareAsset[]>(1);
  // for Security goal values
  multipleCtrl: FormControl = new FormControl();
  selectListSecurityValues: SecurityGoalValue[];
  selectFilterCtrlSecurityValues: FormControl = new FormControl();
  filteredSelectListSecurityValues: ReplaySubject<SecurityGoalValue[]> = new ReplaySubject<SecurityGoalValue[]>(1);

  constructor( private hardwareAssetService: HardwareAssetService,
               private securityGoalValueService: SecurityGoalValueService,
               private ownerService: OwnerService,
               private dialogRef: MatDialogRef<VirtualAssetDialogComponent>,
               private assetNetworkService: AssetNetworkService,
               @Inject(MAT_DIALOG_DATA) data) {
    this.name = data.name;
    this.networkId = data.networkId;
  }

  ngOnInit() {
    this.setFormFields();
    this.entity = new VirtualAsset();
    this.selectListSecurityValues = [];
    this.hardwareAssetService.getAll().then(
      res => {
        this.selectListHardwares = res;
        this.filteredSelectListHardwares.next(this.selectListHardwares.slice());
        this.selectFilterCtrlHardwares.valueChanges
          .pipe(takeUntil(this.onDestroyVar))
          .subscribe(() => {
            this.filterSelectHardwares();
          });
        this.ownerService.getAll().then(
          resO => {
            this.selectList = resO;
            this.filteredSelectList.next(this.selectList.slice());
            this.selectFilterCtrl.valueChanges
              .pipe(takeUntil(this.onDestroyVar))
              .subscribe(() => {
                this.filterSelect();
              });
            this.setAssetTypes();
            this.filteredSelectListType.next(this.selectListType.slice());
            this.selectFilterCtrlType.valueChanges
              .pipe(takeUntil(this.onDestroyVar))
              .subscribe(() => {
                this.filterSelectType();
              });
            this.securityGoalValueService.getAll().then(
              resSec => {
                this.selectListSecurityValues = resSec;
                this.filteredSelectListSecurityValues.next(this.selectListSecurityValues.slice());
                this.flag = true;
                this.selectFilterCtrlSecurityValues.valueChanges
                  .pipe(takeUntil(this.onDestroyVar))
                  .subscribe(() => {
                    this.filterSelectSecurityValues();
                  });
              }
            );
          }
        );
      }
    );
  }

  ngOnDestroy() {
    this.onDestroyVar.next();
    this.onDestroyVar.complete();
  }

  setAssetTypes() {
    for (const eMember in AssetType) {
      if (typeof AssetType[eMember] !== 'number') {
        this.selectListType.push(AssetType[eMember]);
      }
    }
  }

  setFormFields() {
    this.fields = [
      {
        key: 'name',
        type: 'input',
        templateOptions: {
          label: 'Name',
          placeholder: 'Enter name',
          required: true }
      },
      {
        key: 'description',
        type: 'input',
        templateOptions: {
          label: 'Description',
          placeholder: 'Enter description',
          required: true }
      },
      {
        key: 'macAddress',
        type: 'input',
        templateOptions: {
          label: 'Mac Address',
          placeholder: 'Enter Mac address',
          required: true }
      },
      {
        key: 'ipAddress',
        type: 'input',
        templateOptions: {
          label: 'IP Address',
          placeholder: 'Enter IP address',
          required: true }
      },
      {
        key: 'lastUpdated',
        type: 'datepicker',
        templateOptions: {
          label: 'Last updated',
          placeholder: 'Enter last updated',
          required: false }
      },
      {
        key: 'operatingSystem',
        type: 'input',
        templateOptions: {
          label: 'Operating System',
          placeholder: 'Enter operating system',
          required: false }
      }
    ];
  }

  protected filterSelect() {
    if (!this.selectList) {
      return;
    }
    // get the search keyword
    let search = this.selectFilterCtrl.value;
    if (!search) {
      this.filteredSelectList.next(this.selectList.slice());
      return;
    } else {
      search = search.toLowerCase();
    }

    this.filteredSelectList.next(
      this.selectList.filter(entity => (entity.firstName + ' ' + entity.lastName).toLowerCase().indexOf(search) > -1 )
    );
  }

  protected filterSelectHardwares() {
    if (!this.selectListHardwares) {
      return;
    }
    // get the search keyword
    let search = this.selectFilterCtrlHardwares.value;
    if (!search) {
      this.filteredSelectListHardwares.next(this.selectListHardwares.slice());
      return;
    } else {
      search = search.toLowerCase();
    }

    this.filteredSelectListHardwares.next(
      this.selectListHardwares.filter(entity => entity.name.toLowerCase().indexOf(search) > -1 )
    );
  }

  protected filterSelectType() {
    if (!this.selectListType) {
      return;
    }
    let search = this.selectFilterCtrlType.value;
    if (!search) {
      this.filteredSelectListType.next(this.selectListType.slice());
      return;
    } else {
      search = search.toLowerCase();
    }

    this.filteredSelectListType.next(
      this.selectListType.filter(entity => entity.toString().toLowerCase().indexOf(search) > -1)
    );
  }

  protected filterSelectSecurityValues() {
    if (!this.selectListSecurityValues) {
      return;
    }
    let search = this.selectFilterCtrlSecurityValues.value;
    if (!search) {
      this.filteredSelectListSecurityValues.next(this.selectListSecurityValues.slice());
      return;
    } else {
      search = search.toLowerCase();
    }

    this.filteredSelectListSecurityValues.next(
      this.selectListSecurityValues.filter(entity => entity.name.toString().toLowerCase().indexOf(search) > -1)
    );
  }

  compareFunc(c1: SecurityGoalValue, c2: SecurityGoalValue): boolean {
    return c1 && c2 ? c1.id === c2.id : c1 === c2;
  }

  close() {
    this.dialogRef.close();
  }

  submit() {
    this.entity.securityGoalValues = this.multipleCtrl.value;
    this.assetNetworkService.addVAssetsDirectly(this.networkId, this.entity).then(
      res => {
        this.close();
      }
    );
  }
}
