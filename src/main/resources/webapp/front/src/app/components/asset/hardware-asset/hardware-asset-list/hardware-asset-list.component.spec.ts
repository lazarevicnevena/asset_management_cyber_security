import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HardwareAssetListComponent } from './hardware-asset-list.component';

describe('HardwareAssetListComponent', () => {
  let component: HardwareAssetListComponent;
  let fixture: ComponentFixture<HardwareAssetListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HardwareAssetListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HardwareAssetListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
