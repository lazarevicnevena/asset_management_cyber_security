import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DataAssetListComponent } from './data-asset-list.component';

describe('DataAssetListComponent', () => {
  let component: DataAssetListComponent;
  let fixture: ComponentFixture<DataAssetListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DataAssetListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DataAssetListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
