import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NetworkReportListComponent } from './network-report-list.component';

describe('NetworkReportListComponent', () => {
  let component: NetworkReportListComponent;
  let fixture: ComponentFixture<NetworkReportListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NetworkReportListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NetworkReportListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
