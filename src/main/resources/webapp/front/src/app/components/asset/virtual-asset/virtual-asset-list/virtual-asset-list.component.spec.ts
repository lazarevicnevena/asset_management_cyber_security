import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VirtualAssetListComponent } from './virtual-asset-list.component';

describe('VirtualAssetListComponent', () => {
  let component: VirtualAssetListComponent;
  let fixture: ComponentFixture<VirtualAssetListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VirtualAssetListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VirtualAssetListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
