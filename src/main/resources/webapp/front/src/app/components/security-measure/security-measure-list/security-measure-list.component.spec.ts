import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SecurityMeasureListComponent } from './security-measure-list.component';

describe('SecurityMeasureListComponent', () => {
  let component: SecurityMeasureListComponent;
  let fixture: ComponentFixture<SecurityMeasureListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SecurityMeasureListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SecurityMeasureListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
