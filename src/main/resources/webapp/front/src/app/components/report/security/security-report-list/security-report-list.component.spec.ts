import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SecurityReportListComponent } from './security-report-list.component';

describe('SecurityReportListComponent', () => {
  let component: SecurityReportListComponent;
  let fixture: ComponentFixture<SecurityReportListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SecurityReportListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SecurityReportListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
