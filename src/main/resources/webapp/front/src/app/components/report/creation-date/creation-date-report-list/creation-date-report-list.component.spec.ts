import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreationDateReportListComponent } from './creation-date-report-list.component';

describe('CreationDateReportListComponent', () => {
  let component: CreationDateReportListComponent;
  let fixture: ComponentFixture<CreationDateReportListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreationDateReportListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreationDateReportListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
