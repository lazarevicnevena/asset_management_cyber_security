import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SoftwareAssetListComponent } from './software-asset-list.component';

describe('SoftwareAssetListComponent', () => {
  let component: SoftwareAssetListComponent;
  let fixture: ComponentFixture<SoftwareAssetListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SoftwareAssetListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SoftwareAssetListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
