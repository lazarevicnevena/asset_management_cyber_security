import { Component, OnInit } from '@angular/core';
import {AssetService} from '../../../../service/asset.service';
import {Asset} from '../../../../model/asset';
import {Router} from '@angular/router';
import {HardwareAssetService} from '../../../../service/hardware-asset.service';
import {SoftwareAssetService} from '../../../../service/software-asset.service';
import {VirtualAssetService} from '../../../../service/virtual-asset.service';
import {DataAssetService} from '../../../../service/data-asset.service';
import {MatDialog, MatDialogConfig, MatSnackBar} from '@angular/material';
import {SecurityGoalValuesDialogComponent} from '../security-goal-values-dialog/security-goal-values-dialog.component';
import {AssetType} from '../../../../model/asset-type';
import {Owner} from '../../../../model/owner';

@Component({
  selector: 'app-asset-list',
  templateUrl: './asset-list.component.html',
  styleUrls: ['./asset-list.component.css']
})
export class AssetListComponent implements OnInit {

  entities: Asset[];
  mainFlag: boolean;
  entity: Asset;
  selectedRows: Asset[] = [];

  constructor( private assetService: AssetService,
               private snackBar: MatSnackBar,
               private dialog: MatDialog,
               private hardwareAssetService: HardwareAssetService,
               private softwareAssetService: SoftwareAssetService,
               private virtualAssetService: VirtualAssetService,
               private dataAssetService: DataAssetService,
               private router: Router) { }

  ngOnInit() {
    this.entity = {
      id : 0,
      name : '',
      description : '',
      state : '',
      uuid : '',
      created : new Date(),
      type : AssetType.WORK_STATION,
      owner : new Owner(),
      dtype : '',
      securityGoalValues : []
    };
    this.getAll();
  }

  getAll() {
    this.mainFlag = false;
    this.entities = [];
    this.assetService.getAll().then(
      res => {
        this.entities = res;
        this.mainFlag = true;
      }
    );
  }

  openDialog() {
    let subtype = '';
    if (this.entity.dtype === 'Hardware asset') {
      subtype = 'hardware';
    } else if (this.entity.dtype === 'Software asset') {
      subtype = 'software';
    }  else if (this.entity.dtype === 'Virtual asset') {
      subtype = 'virtual';
    }  else if (this.entity.dtype === 'Data asset') {
      subtype = 'data';
    }
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '30vw';
    dialogConfig.maxWidth = '30vw';
    dialogConfig.data = {
      id: this.entity.uuid,
      name: this.entity.name,
      subtype: subtype
    };

    const d = this.dialog.open(SecurityGoalValuesDialogComponent, dialogConfig);

    d.afterClosed().subscribe(() => {
      // update selected entity!!
      this.refresh();
    });
  }

  refresh() {
    this.entities = [];
    this.assetService.getAll().then(
      res => {
        this.entities = res;
        for (const v of this.entities) {
          if (v.uuid === this.entity.uuid) {
            this.entity.securityGoalValues = v.securityGoalValues;
          }
        }
      }
    );
  }

  openAlert(message: string) {
    this.snackBar.open(message, 'x', {
      verticalPosition: 'top',
      duration: 2000,
      panelClass: ['snackbar-class']
    });
  }

  rowSelected() {
    if (this.selectedRows.length === 0) {
      this.entity.securityGoalValues = [];
    } else {
      this.entity = this.selectedRows[0];
    }
  }

}
