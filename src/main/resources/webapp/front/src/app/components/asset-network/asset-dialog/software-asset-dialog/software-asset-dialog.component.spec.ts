import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SoftwareAssetDialogComponent } from './software-asset-dialog.component';

describe('SoftwareAssetDialogComponent', () => {
  let component: SoftwareAssetDialogComponent;
  let fixture: ComponentFixture<SoftwareAssetDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SoftwareAssetDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SoftwareAssetDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
