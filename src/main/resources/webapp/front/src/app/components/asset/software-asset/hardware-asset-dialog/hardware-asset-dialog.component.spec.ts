import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HardwareAssetDialogComponent } from './hardware-asset-dialog.component';

describe('HardwareAssetDialogComponent', () => {
  let component: HardwareAssetDialogComponent;
  let fixture: ComponentFixture<HardwareAssetDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HardwareAssetDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HardwareAssetDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
