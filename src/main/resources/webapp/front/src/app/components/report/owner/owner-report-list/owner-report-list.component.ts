import { Component, OnInit } from '@angular/core';
import {FormGroup} from '@angular/forms';
import {FormlyFieldConfig} from '@ngx-formly/core';
import {MatDialog, MatDialogConfig, MatSnackBar} from '@angular/material';
import {Router} from '@angular/router';
import {ResultDialogComponent} from '../../result-dialog/result-dialog.component';
import {OwnerReport} from '../../../../model/owner-report';
import {OwnerReportService} from '../../../../service/owner-report.service';

@Component({
  selector: 'app-owner-report-list',
  templateUrl: './owner-report-list.component.html',
  styleUrls: ['./owner-report-list.component.css']
})
export class OwnerReportListComponent implements OnInit {

  entities: OwnerReport[];
  form = new FormGroup({});
  selectedRows: OwnerReport[];
  entity: OwnerReport;
  fields: FormlyFieldConfig[];
  mode = '';

  constructor( private ownerReportService: OwnerReportService,
               private dialog: MatDialog,
               private snackBar: MatSnackBar,
               private router: Router) { }

  ngOnInit() {
    this.mode = 'CREATE';
    this.setFormFields();
    this.entity = new OwnerReport();
    this.getAll();
  }

  getAll() {
    this.entities = [];
    this.ownerReportService.getAll().then(
      res => {
        this.entities = res;
      }
    );
  }

  delete() {
    if (this.mode !== 'CREATE') {
      this.entity = this.selectedRows[0];
      this.ownerReportService.delete(this.entity.id)
        .then(res => {
          this.getAll();
        });
    }
  }

  edit() {
    this.entity = this.selectedRows[0];
    this.mode = 'EDIT';
    this.setFormFields();
  }

  create() {
    this.mode = 'CREATE';
    this.entity = new OwnerReport();
    this.selectedRows = [];
    this.selectedRows.push(this.entity);
    this.setFormFields();
  }

  submit() {
    if (this.mode === 'EDIT') {
      this.ownerReportService.update(this.entity).then(
        res => {
          this.close();
          this.getAll();
        }
      );
    } else if (this.mode === 'CREATE') {
      this.ownerReportService.create(this.entity).then(
        res => {
          this.close();
          this.getAll();
        }
      );
    }
  }

  close() {
    this.selectedRows = [];
    this.entity = new OwnerReport();
    this.mode = 'CREATE';
  }

  filter() {
    if (this.selectedRows.length > 0) {
      this.entity = this.selectedRows[0];
      const id = this.entity.id;
      console.log('Filtering for report: ' + id);
      this.ownerReportService.filter(id).then(
        res => {
          const dialogConfig = new MatDialogConfig();

          dialogConfig.disableClose = true;
          dialogConfig.autoFocus = true;
          dialogConfig.data = {
            assets: res
          };

          this.dialog.open(ResultDialogComponent, dialogConfig);
        }
      ).catch(
        res => {
          this.openAlert('No asset satisfies criteria for this report!');
        }
      );
    }
  }

  openAlert(message: string) {
    this.snackBar.open(message, 'x', {
      verticalPosition: 'top',
      duration: 2000,
      panelClass: ['snackbar-class']
    });
  }

  setFormFields() {
    this.fields = [
      {
        key: 'firstName',
        type: 'input',
        templateOptions: {
          label: 'First name',
          placeholder: 'Enter first name'}
      },
      {
        key: 'lastName',
        type: 'input',
        templateOptions: {
          label: 'Last name',
          placeholder: 'Enter last name'}
      },
      {
        key: 'middleName',
        type: 'input',
        templateOptions: {
          label: 'Middle mame',
          placeholder: 'Enter middle name'}
      },
      {
        key: 'employeeNr',
        type: 'input',
        templateOptions: {
          label: 'Employee ID',
          placeholder: 'Enter employee ID'}
      },
      {
        key: 'phoneNumber',
        type: 'input',
        templateOptions: {
          label: 'Phone number',
          placeholder: 'Enter phone number'}
      }
    ];
  }

  rowSelected() {
    if (this.selectedRows.length === 0) {
      this.entity = new OwnerReport();
      this.mode = 'CREATE';
    } else {
      this.edit();
    }
  }

}
