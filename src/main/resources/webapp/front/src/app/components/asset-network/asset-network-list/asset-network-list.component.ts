import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {AssetNetwork} from '../../../model/asset-network';
import {AssetNetworkService} from '../../../service/asset-network.service';
import {MatDialog, MatDialogConfig, MatSnackBar} from '@angular/material';
import {FormlyFieldConfig} from '@ngx-formly/core';
import {FormControl, FormGroup} from '@angular/forms';
import {Asset} from '../../../model/asset';
import {ReplaySubject, Subject} from 'rxjs';
import {takeUntil} from 'rxjs/internal/operators';
import {AssetService} from '../../../service/asset.service';
import {HardwareAssetDialogComponent} from '../../asset/software-asset/hardware-asset-dialog/hardware-asset-dialog.component';
import {SoftwareAssetDialogComponent} from '../asset-dialog/software-asset-dialog/software-asset-dialog.component';
import {VirtualAssetDialogComponent} from '../asset-dialog/virtual-asset-dialog/virtual-asset-dialog.component';
import {DataAssetDialogComponent} from '../asset-dialog/data-asset-dialog/data-asset-dialog.component';

@Component({
  selector: 'app-asset-network-list',
  templateUrl: './asset-network-list.component.html',
  styleUrls: ['./asset-network-list.component.css']
})
export class AssetNetworkListComponent implements OnInit, OnDestroy {

  entities: AssetNetwork[];
  form = new FormGroup({});
  selectedRows: AssetNetwork[] = [];
  entity: AssetNetwork;
  fields: FormlyFieldConfig[];
  mode = '';
  mainFlag = false;
  multipleCtrl: FormControl = new FormControl();
  selectList: Asset[];
  selectFilterCtrl: FormControl = new FormControl();
  filteredSelectList: ReplaySubject<Asset[]> = new ReplaySubject<Asset[]>(1);
  onDestroyVar = new Subject<void>();

  constructor( private assetNetworkService: AssetNetworkService,
               private assetService: AssetService,
               private snackBar: MatSnackBar,
               private dialog: MatDialog) { }

  ngOnInit() {
    this.mode = 'CREATE';
    this.setFormFields();
    this.entity = new AssetNetwork();
    this.selectList = [];
    this.assetService.getAll().then(
      res2 => {
        this.selectList = res2;
        this.filteredSelectList.next(this.selectList.slice());

        // listen for search field value changes
        this.selectFilterCtrl.valueChanges
          .pipe(takeUntil(this.onDestroyVar))
          .subscribe(() => {
            this.filterSelect();
          });
        this.getAll();
      }
    );
  }


  ngOnDestroy() {
    this.onDestroyVar.next();
    this.onDestroyVar.complete();
  }

  getAll() {
    this.mainFlag = false;
    this.entities = [];
    this.assetNetworkService.getAll().then(
      res => {
        this.entities = res;
        this.mainFlag = true;
      }
    );
  }

  delete() {
    this.entity = this.selectedRows[0];
    console.log('Deleting network with id: ' + this.entity.id);
    this.assetNetworkService.delete(this.entity.id)
      .then(res => {
        this.getAll();
      });
  }

  edit() {
    this.entity = this.selectedRows[0];
    console.log('Editing network with id: ' + this.entity.id);
    this.mode = 'EDIT';
    if (this.entity.assets.length > 0) {
      this.multipleCtrl.setValue(this.entity.assets);
    }
    this.setFormFields();
  }

  create() {
    this.mode = 'CREATE';
    this.multipleCtrl.setValue([]);
    this.entity = new AssetNetwork();
    this.selectedRows = [];
    this.setFormFields();
  }

  submit() {
    this.entity.assets = this.multipleCtrl.value;
    if (this.mode === 'EDIT') {
      this.assetNetworkService.update(this.entity).then(
        res => {
          this.close();
          this.getAll();
        }
      );
    } else if (this.mode === 'CREATE') {
      this.assetNetworkService.create(this.entity).then(
        res => {
          this.close();
          this.getAll();
        }
      );
    }
  }

  close() {
    this.selectedRows = [];
    this.entity = new AssetNetwork();
    this.multipleCtrl.setValue([]);
    this.mode = 'CREATE';
  }

  openAlert(message: string) {
    this.snackBar.open(message, 'x', {
      verticalPosition: 'top',
      duration: 2000,
      panelClass: ['snackbar-class']
    });
  }

  openHardwareADialog() {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '30vw';
    dialogConfig.maxWidth = '30vw';
    dialogConfig.data = {
      id: 0,
      name: this.entity.name,
      flag: false,
      networkId: this.entity.id
    };

    const d = this.dialog.open(HardwareAssetDialogComponent, dialogConfig);

    d.afterClosed().subscribe(() => {
      // update selected entity!!
      this.refresh();
    });
  }

  openSoftwareADialog() {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '30vw';
    dialogConfig.maxWidth = '30vw';
    dialogConfig.data = {
      name: this.entity.name,
      networkId: this.entity.id
    };

    const d = this.dialog.open(SoftwareAssetDialogComponent, dialogConfig);

    d.afterClosed().subscribe(() => {
      // update selected entity!!
      this.refresh();
    });
  }

  openVirtualADialog() {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '30vw';
    dialogConfig.maxWidth = '30vw';
    dialogConfig.data = {
      name: this.entity.name,
      networkId: this.entity.id
    };

    const d = this.dialog.open(VirtualAssetDialogComponent, dialogConfig);

    d.afterClosed().subscribe(() => {
      // update selected entity!!
      this.refresh();
    });
  }

  openDataADialog() {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '30vw';
    dialogConfig.maxWidth = '30vw';
    dialogConfig.data = {
      name: this.entity.name,
      networkId: this.entity.id
    };

    const d = this.dialog.open(DataAssetDialogComponent, dialogConfig);

    d.afterClosed().subscribe(() => {
      // update selected entity!!
      this.refresh();
    });
  }

  refresh() {
    this.entities = [];
    this.assetNetworkService.getAll().then(
      res => {
        this.entities = res;
        for (const v of this.entities) {
          if (v.id === this.entity.id) {
            this.entity.assets = v.assets;
          }
        }
      }
    );
  }

  setFormFields() {
    this.fields = [
      {
        key: 'name',
        type: 'input',
        templateOptions: {
          label: 'Name',
          placeholder: 'Enter name',
          required: true }
      },
      {
        key: 'description',
        type: 'input',
        templateOptions: {
          label: 'Description',
          placeholder: 'Enter description',
          required: true }
      }
    ];
  }

  compareFunc(c1: Asset, c2: Asset): boolean {
    return c1 && c2 ? c1.uuid === c2.uuid : c1 === c2;
  }

  protected filterSelect() {
    if (!this.selectList) {
      return;
    }
    // get the search keyword
    let search = this.selectFilterCtrl.value;
    if (!search) {
      this.filteredSelectList.next(this.selectList.slice());
      return;
    } else {
      search = search.toLowerCase();
    }

    this.filteredSelectList.next(
      this.selectList.filter(entity => entity.name.toLowerCase().indexOf(search) > -1)
    );
  }

  rowSelected() {
    if (this.selectedRows.length === 0) {
      this.selectedRows = [];
      this.multipleCtrl.setValue([]);
      this.entity = new AssetNetwork();
      this.mode = 'CREATE';
    } else {
      this.edit();
    }
  }
}
