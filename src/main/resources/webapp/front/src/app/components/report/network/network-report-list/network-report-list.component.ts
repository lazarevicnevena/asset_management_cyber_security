import { Component, OnInit } from '@angular/core';
import {FormGroup} from '@angular/forms';
import {FormlyFieldConfig} from '@ngx-formly/core';
import {MatDialog, MatDialogConfig, MatSnackBar} from '@angular/material';
import {Router} from '@angular/router';
import {ResultDialogComponent} from '../../result-dialog/result-dialog.component';
import {NetworkReport} from '../../../../model/network-report';
import {NetworkReportService} from '../../../../service/network-report.service';

@Component({
  selector: 'app-network-report-list',
  templateUrl: './network-report-list.component.html',
  styleUrls: ['./network-report-list.component.css']
})
export class NetworkReportListComponent implements OnInit {

  entities: NetworkReport[];
  form = new FormGroup({});
  selectedRows: NetworkReport[];
  entity: NetworkReport;
  fields: FormlyFieldConfig[];
  mode = '';

  constructor( private networkReportService: NetworkReportService,
               private dialog: MatDialog,
               private snackBar: MatSnackBar,
               private router: Router) { }

  ngOnInit() {
    this.mode = 'CREATE';
    this.setFormFields();
    this.entity = new NetworkReport();
    this.getAll();
  }

  getAll() {
    this.entities = [];
    this.networkReportService.getAll().then(
      res => {
        this.entities = res;
      }
    );
  }

  delete() {
    if (this.mode !== 'CREATE') {
      this.entity = this.selectedRows[0];
      this.networkReportService.delete(this.entity.id)
        .then(res => {
          this.getAll();
        });
    }
  }

  edit() {
    this.entity = this.selectedRows[0];
    this.mode = 'EDIT';
    this.setFormFields();
  }

  create() {
    this.mode = 'CREATE';
    this.entity = new NetworkReport();
    this.selectedRows = [];
    this.selectedRows.push(this.entity);
    this.setFormFields();
  }

  submit() {
    if (this.mode === 'EDIT') {
      this.networkReportService.update(this.entity).then(
        res => {
          this.close();
          this.getAll();
        }
      );
    } else if (this.mode === 'CREATE') {
      this.networkReportService.create(this.entity).then(
        res => {
          this.close();
          this.getAll();
        }
      );
    }
  }

  close() {
    this.selectedRows = [];
    this.entity = new NetworkReport();
    this.mode = 'CREATE';
  }

  filter() {
    if (this.selectedRows.length > 0) {
      this.entity = this.selectedRows[0];
      const id = this.entity.id;
      console.log('Filtering for report: ' + id);
      this.networkReportService.filter(id).then(
        res => {
          const dialogConfig = new MatDialogConfig();

          dialogConfig.disableClose = true;
          dialogConfig.autoFocus = true;
          dialogConfig.data = {
            assets: res
          };

          this.dialog.open(ResultDialogComponent, dialogConfig);
        }
      ).catch(
        res => {
          this.openAlert('No asset satisfies criteria for this report!');
        }
      );
    }
  }

  openAlert(message: string) {
    this.snackBar.open(message, 'x', {
      verticalPosition: 'top',
      duration: 2000,
      panelClass: ['snackbar-class']
    });
  }

  setFormFields() {
    this.fields = [
      {
        key: 'name',
        type: 'input',
        templateOptions: {
          label: 'Name',
          placeholder: 'Enter network name'}
      },
      {
        key: 'description',
        type: 'input',
        templateOptions: {
          label: 'Description',
          placeholder: 'Enter network description'}
      },
      {
        key: 'minAssetNr',
        type: 'input',
        templateOptions: {
          label: 'Minimum asset included',
          placeholder: 'Enter minimum number of assets in network'}
      },
      {
        key: 'maxAssetNr',
        type: 'input',
        templateOptions: {
          label: 'Maximum asset included',
          placeholder: 'Enter maximum number of assets in network'}
      }
    ];
  }

  rowSelected() {
    if (this.selectedRows.length === 0) {
      this.entity = new NetworkReport();
      this.mode = 'CREATE';
    } else {
      this.edit();
    }
  }

}
