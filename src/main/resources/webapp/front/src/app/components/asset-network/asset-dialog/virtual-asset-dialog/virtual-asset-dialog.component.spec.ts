import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VirtualAssetDialogComponent } from './virtual-asset-dialog.component';

describe('VirtualAssetDialogComponent', () => {
  let component: VirtualAssetDialogComponent;
  let fixture: ComponentFixture<VirtualAssetDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VirtualAssetDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VirtualAssetDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
